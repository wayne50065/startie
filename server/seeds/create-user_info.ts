import { Knex } from "knex";
import { hashPassword } from "../utils/bcrypt";
import { Chance } from "chance";
const chance = new Chance();

export async function seed(knex: Knex): Promise<void> {
  // delete table
  await knex("api_log").del();
  await knex("complaint").del();
  await knex("angel_fund_participant").del();
  await knex("angel_fund").del();
  await knex("project_competition").del();
  await knex("competition").del();
  await knex("user_talent").del();
  await knex("project_talent").del();
  await knex("project_discussion").del();
  await knex("project_participant").del();
  await knex("bookmark_list").del();
  await knex("project_image").del();
  await knex("project").del();
  await knex("talent").del();
  await knex("project_category").del();
  await knex("user_info").del();
  await knex("school").del();

  //

  // variable list
  const school_list = [
    {
      school_type: "UGC-funded universities",
      code: "CU",
      domain: "cuhk.edu.hk",
      english_name: "The Chinese University of Hong Kong",
      chinese_name: "香港中文大學",
      email_domains: ["cuhk.edu.hk", "link.cuhk.edu.hk"],
    },
    {
      school_type: "UGC-funded universities",
      code: "CITYU",
      domain: "cityu.edu.hk",
      english_name: "City University of Hong Kong",
      chinese_name: "香港城市大學",
      email_domains: ["my.cityu.edu.hk"],
    },
    {
      school_type: "UGC-funded universities",
      code: "EDU",
      domain: "eduhk.hk",
      english_name: "The Education University of Hong Kong",
      chinese_name: "香港教育大學",
      email_domains: ["s.eduhk.hk"],
    },
    {
      school_type: "UGC-funded universities",
      code: "BU",
      domain: "hkbu.edu.hk",
      english_name: "Hong Kong Baptist University",
      chinese_name: "香港浸會大學",
      email_domains: ["hkbu.edu.hk", "ife.hkbu.edu.hk"],
    },
    {
      school_type: "UGC-funded universities",
      code: "POLYU",
      domain: "polyu.edu.hk",
      english_name: "The Hong Kong Polytechnic University",
      chinese_name: "香港理工大學",
      email_domains: ["polyu.edu.hk", "connect.polyu.hk"],
    },
    {
      school_type: "UGC-funded universities",
      code: "UST",
      domain: "ust.hk",
      english_name: "The Hong Kong University of Science and Technology",
      chinese_name: "香港科技大學",
      email_domains: ["connect.ust.hk", "ust.hk", "stu.ust.hk"],
    },
    {
      school_type: "UGC-funded universities",
      code: "LU",
      domain: "ln.edu.hk",
      english_name: "Lingnan University",
      chinese_name: "嶺南大學",
      email_domains: ["LN.edu.hk".toLowerCase(), "LN.hk".toLowerCase()],
    },
    {
      school_type: "UGC-funded universities",
      code: "HKU",
      domain: "hku.hk",
      english_name: "The University of Hong Kong",
      chinese_name: "香港大學",
      email_domains: ["connect.hku.hk"],
    },
    {
      school_type: "Self-funded institutions",
      code: "OU",
      domain: "ouhk.edu.hk",
      english_name: "The Open University of Hong Kong",
      chinese_name: "香港公開大學",
      email_domains: ["ouhk.edu.hk"],
    },
    {
      school_type: "Self-funded institutions",
      code: "SYU",
      domain: "hksyu.edu",
      english_name: "Hong Kong Shue Yan University",
      chinese_name: "香港樹仁大學",
      email_domains: ["hksyu.edu", "alumni.hksyu.edu.hk"],
    },
    {
      school_type: "Self-funded institutions",
      code: "HSU",
      domain: "hsu.edu.hk",
      english_name: "Hang Seng University of Hong Kong",
      chinese_name: "香港恒生大學",
      email_domains: ["hsu.edu.hk"],
    },
    {
      school_type: "Member Institutions of the Vocational Training Council",
      code: "IVE",
      domain: "ive.edu.hk",
      english_name: "Hong Kong Institute of Vocational Education",
      chinese_name: "香港專業教育學院",
      email_domains: ["ive.edu.hk"],
    },
    {
      school_type: "Member Institutions of the Vocational Training Council",
      code: "THEI",
      domain: "thei.edu.hk",
      english_name: "Technological and Higher Education Institute of Hong Kong",
      chinese_name: "香港高等教育科技學院",
      email_domains: ["thei.edu.hk"],
    },
    {
      school_type: "Member Institutions of the Vocational Training Council",
      code: "VTC",
      domain: "vtc.edu.hk",
      english_name: "Vocational Training Council",
      chinese_name: "職業訓練局",
      email_domains: ["vtc.edu.hk", "stu.vtc.edu.hk"],
    },
    {
      school_type: "admin",
      code: "admin",
      domain: "NA",
      english_name: "admin",
      chinese_name: "管理員",
      email_domains: [],
    },
  ];

  const user_list_admin = [
    {
      email_address: "ring50065@gmail.com",
      name: "wayne Wayne",
      title: "Mr",
      self_introduction: "self_introduction",
      education_level: "Bachelor",
      year: "graduated",
      mode_of_study: "Full-Time",
      school_id: 15,
      major: "NA",
      faculty: "NA",
      is_admin: true,
      personal_image_url:"Personal_image3.jpg",
    },
    {
      email_address: "mark.lam.work@gmail.com",
      name: "Mark Lam",
      title: "Mr",
      self_introduction: "self_introduction",
      education_level: "Bachelor",
      year: "graduated",
      mode_of_study: "Full-Time",
      school_id: 15,
      major: "NA",
      faculty: "NA",
      is_admin: true,
      personal_image_url:"Personal_image1.jpg",
    },
    {
      email_address: "ckpkate@gmail.com",
      name: "Kate Cheng",
      title: "Miss",
      self_introduction: "self_introduction",
      education_level: "Bachelor",
      year: "graduated",
      mode_of_study: "Full-Time",
      school_id: 15,
      major: "NA",
      faculty: "NA",
      is_admin: true,
      personal_image_url:"Personal_image2.jpg",
    },

    {
      email_address: "mark@tecky.io",
      name: "Mark Lam",
      title: "Mr",
      self_introduction: "self_introduction",
      education_level: "Bachelor",
      year: "graduated",
      mode_of_study: "Full-Time",
      school_id: 15,
      major: "NA",
      faculty: "NA",
      is_admin: false,
      personal_image_url:"Personal_image1.jpg",
    },
    {
      email_address: "kate@tecky.io",
      name: "Kate Cheng",
      title: "Miss",
      self_introduction: "self_introduction",
      education_level: "Bachelor",
      year: "graduated",
      mode_of_study: "Full-Time",
      school_id: 15,
      major: "NA",
      faculty: "NA",
      is_admin: false,
      personal_image_url:"Personal_image2.jpg",
    },
    {
      email_address: "wayne@tecky.io",
      name: "Wayne Sze",
      title: "Mr",
      self_introduction: "self_introduction",
      education_level: "Bachelor",
      year: "graduated",
      mode_of_study: "Full-Time",
      school_id: 15,
      major: "NA",
      faculty: "NA",
      is_admin: false,
      personal_image_url:"Personal_image3.jpg",
    },
  ];

  let user_list = [];
  for (let i = 1; i <= 1500; i++) {
    user_list[i - 1] = {
      email_address: chance.email({ domain: "tecky.io" }),
      name: chance.name(),
      title:
        Math.random() > 0.66 ? "Mr" : Math.random() > 0.33 ? "Miss" : "Mrs",
      self_introduction: chance.paragraph({ sentences: 1 }),
      education_level: Math.random() > 0.2 ? "Bachelor" : "Master",
      year:
        (Math.random() > 0.2 ? "Bachelor" : "Master") == "Bachelor"
          ? "Year" + Math.ceil(Math.random() * 4)
          : "Year1",
      mode_of_study: Math.random() > 0.1 ? "Full-time" : "Part-time",
      school_id: Math.round(Math.random() * 13 + 1),
      major:
        Math.random() > 0.75
          ? "business"
          : Math.random() > 0.5
          ? "IT"
          : Math.random() > 0.25
          ? "IT"
          : "Language",
      is_admin: false,
      personal_image_url:
        "student_image"+Math.round(Math.random() * 8 + 1)+".jpg",
      created_at:
        "202" +
        Math.round(Math.random() * 1) +
        "-" +
        Math.round(Math.random() * 11 + 1) +
        "-" +
        Math.round(Math.random() * 27 + 1) +
        " 19:27:58.980091+08",
    };
  }

  const project_category_list = [

    { category_name: "Accounting"},
    { category_name: "Advertising"},
    { category_name: "App development"},
    { category_name: "Baking"},
    { category_name: "Business"},
    { category_name: "Business consulting"},
    { category_name: "Event planning"},
    { category_name: "Fin-Tech"},
    { category_name: "Graphic design"},
    { category_name: "House service"},
    { category_name: "Law"},
    { category_name: "Marketing firm"},
    { category_name: "Music"},
    { category_name: "New energy"},
    { category_name: "Person consulting"},
    { category_name: "Pets"},
    { category_name: "Photographers"},
    { category_name: "Platform"},
    { category_name: "Product invent"},
    { category_name: "Production house"},
    { category_name: "Retail"},
    { category_name: "Sport"},
    { category_name: "Translate"},
    { category_name: "Web designing"},
    { category_name: "YouTube"},
    
  ];

  const talent_list = [
    { skill: "Time management" },
    { skill: "Effective communication" },
    { skill: "Emotional intelligence" },
    { skill: "Conflict management" },
    { skill: "Teamwork skills" },
    { skill: "Stress management" },
    { skill: "Problem-solving" },
    { skill: "Productivity & organization" },
    { skill: "Critical thinking" },
    { skill: "Attention to detail" },
    { skill: "Adaptability" },
    { skill: "Data analysis" },
    { skill: "Web analytics " },
    { skill: "SEO/SEM" },
    { skill: "HTML & CSS" },
    { skill: "Wordpress" },
    { skill: "Email marketing" },
    { skill: "Web scraping" },
    { skill: "CRO and A/B Testing" },
    { skill: "Data visualization " },
    { skill: "pattern-finding through critical thinking" },
    { skill: "Search Engine and Keyword Optimization" },
    { skill: "Project/campaign management" },
    { skill: "Social media and mobile marketing " },
    { skill: "Paid social media advertisements" },
    { skill: "B2B Marketing" },
    { skill: "The 4 P-s of Marketing" },
    { skill: "Consumer Behavior Drivers" },
    { skill: "Brand management" },
    { skill: "Creativity" },
    { skill: "Copywriting" },
    { skill: "Storytelling" },
    { skill: "Sales" },
    { skill: "CMS Tools" },
    { skill: "Six Sigma techniques" },
    { skill: "The McKinsey 7s Framework" },
    { skill: "Porter’s Five Forces" },
    { skill: "PESTEL " },
    { skill: "Dealing with work-related stress" },
    { skill: "Motivation" },
    { skill: "Task delegation" },
    { skill: "Technological savviness" },
    { skill: "People management" },
    { skill: "Business Development" },
    { skill: "Strategic Management" },
    { skill: "Negotiation " },
    { skill: "Planning" },
    { skill: "Proposal writing" },
    { skill: "Innovation" },
    { skill: "Charisma" },
    { skill: "Customer Relationship Management (CRM)" },
    { skill: "Cold-calling" },
    { skill: "Negotiation" },
    { skill: "Public speaking" },
    { skill: "Closing" },
    { skill: "Lead generation" },
    { skill: "Buyer-Responsive selling" },
    { skill: "Buyer engagement" },
    { skill: "Product knowledge" },
    { skill: "Persuasion" },
    { skill: "Effective communication and sociability" },
    { skill: "Empathy" },
    { skill: "Social media/digital communication" },
    { skill: "Teamwork" },
    { skill: "Illustrator" },
    { skill: "InDesign" },
    { skill: "Photoshop" },
    { skill: "Dreamweaver" },
    { skill: "Infographics" },
    { skill: "Photo editing" },
    { skill: "Typography" },
    { skill: "Storyboarding" },
    { skill: "Logo creation" },
    { skill: "Digital printing" },
    { skill: "Attention to detail & aesthetics" },
    { skill: "Interactive media design" },
    { skill: "Color sense & theory" },
    { skill: "Ad design" },
    { skill: "Active listening" },
    { skill: "Microsoft Word" },
    { skill: "Microsoft Excel" },
    { skill: "Microsoft Access" },
    { skill: "Microsoft Publisher" },
    { skill: "Microsoft Outlook " },
    { skill: "Microsoft Powerpoint" },
    { skill: "Microsoft PowerBI" },
    { skill: "Microsoft PowerAPP" },
    { skill: "Microsoft PowerAutomate" },
    { skill: "Filing and paper management" },
    { skill: "Data entry" },
    { skill: "Bookkeeping through Excel or TurboTax" },
    { skill: "Research and data analysis" },
    { skill: "Basic knowledge of user interface communication" },
    { skill: "Technical writing" },
    { skill: "Cloud networking and file sharing " },
    { skill: "Enterprise Resource Planning " },
    { skill: "Big Data Analysis & SQL" },
    { skill: "Know Your Customers (KYC)" },
    { skill: "Cognos Analytics (IBM)" },
    { skill: "Visual Basic" },
    { skill: "Accounting Software" },
    { skill: "Revenue recognition" },
    { skill: "Anti Money Laundering" },
    { skill: "Clear communication" },
    { skill: "General business knowledge" },
    { skill: "Numerical competence" },
    { skill: "Accuracy" },
    { skill: "Updated curriculum knowledge" },
    { skill: "Research & Data analysis" },
    { skill: "Communication" },
    { skill: "Educational platforms (software like Elearn)" },
    { skill: "Technological & digital literacy" },
    { skill: "Patience" },
    { skill: "Enthusiasm" },
    { skill: "HTML/CSS" },
    { skill: "CSS preprocessors" },
    { skill: "Javascript" },
    { skill: "Graphic User Interfaces (GUI)" },
    { skill: "Git/Version control (Github, gitlab)" },
    { skill: "Search Engine Optimization (SEO)" },
    { skill: "Application Programming Interface (API)" },
    { skill: "Adobe Photoshop, InDesign" },
    { skill: "Content Management Systems (CMS)" },
    { skill: "Testing/Debugging" },
    { skill: "Responsive design principles" },
    { skill: "SQL" },
    { skill: "R" },
    { skill: "spark" },
    { skill: "knex" },
    { skill: "Python" },
    { skill: "Scala" },
    { skill: "Matlab" },
    { skill: "STATA" },
    { skill: "SPSS" },
    { skill: "SAS" },
    { skill: "SAP" },
    { skill: "Data Mapping" },
    { skill: "Entity Relationship Diagrams" },
    { skill: "Wireframes" },
    { skill: "Big Data tools " },
    { skill: "Microsoft Visio" },
    { skill: "Agile Business Analysis" },
    { skill: "Machine learning " },
    { skill: "System Context Diagrams" },
    { skill: "Business Process Modeling" },
    { skill: "Technical communication" },
    { skill: "non-technical communication" },
    { skill: "Mathematics" },
    { skill: "CPR" },
    { skill: "Patient care and assistance" },
    { skill: "Paperwork/record-keeping abilities" },
    { skill: "Compassion" },
    { skill: "Advanced Cardiac Life Support (ACLS)" },
    { skill: "Telemetry" },
    { skill: "Physical endurance " },
    { skill: "Acute care" },
    { skill: "Infection control" },
    { skill: "Surgery preparation" },
    { skill: "Mandarin Chinese" },
    { skill: "Spanish" },
    { skill: "English" },
    { skill: "Cantonese" },
    { skill: "Portuguese" },
    { skill: "Russian" },
    { skill: "Japanese" },
    { skill: "Korean" },
    { skill: "French" },
    { skill: "German" },
    { skill: "Vietnamese" },
    { skill: "Italian" },
    { skill: "Polish" },
    { skill: "Art" },
    { skill: "Design" },
    { skill: "Science" },
    { skill: "Coding" },
    { skill: "Health" },
  ];


  let project_list = [];

  
  for (let i = 1; i <= 4000; i++) {
    let year = Math.round(Math.random() * 1);
    let month = (year==0)?Math.round(Math.random() * 11 + 1):Math.round(Math.random() * 10 + 1);
    let application_deadline =
      "2023" + year + "-" + month + "-25 19:27:58.980091+08";
    let project_deadline =
      "2023"+ "-" + month + "-28 19:27:58.980091+08";
    let created_at = "202" + year + "-" + month + "-02 19:27:58.980091+08";

    project_list[i - 1] = {
      title: "Project " + i + " -  generate automatically",
      detail: "Project detail -  generate automatically",
      user_id: Math.round(Math.random() * 499 + 1),
      category_id: Math.round(Math.random() * 11 + 1),
      chat_room_url: chance.url(),
      youtube_url: chance.url(),
      criteria: Math.random()>0.66?"Undergraduate/master student required":Math.random()>0.33?"Final year student required":"Graduated student required",
      application_deadline,
      project_deadline,
      created_at,
    };
  }



  const project_list_present = [
    {
      title: "Project 1 xxxxxxxxxxxxxxxxxxxxxxx",
      detail: "this is for input project detail",
      user_email: "mark@tecky.io",
      category_id: "Sport",
      chat_room_url: "",
      youtube_url: "",
      criteria: "",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
    {
      title: "Project 2 xxxxxxxxxxxxxxxxxxxxxxx",
      detail: "Please input details",
      user_email: "kate@tecky.io",
      category_id: "Sport",
      chat_room_url: "",
      youtube_url: "",
      criteria: "",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
    {
      title: "Project 3 xxxxxxxxxxxxxxxxxxxxxxx",
      detail: "Please input details",
      user_email: "wayne@tecky.io",
      category_id: "Sport",
      chat_room_url: "",
      youtube_url: "",
      criteria: "",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
    {
      title: "Project 4 xxxxxxxxxxxxxxxxxxxxxxx",
      detail: "Please input details",
      user_email: "mark@tecky.io",
      category_id: "Sport",
      chat_room_url: "",
      youtube_url: "",
      criteria: "",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
    {
      title: "Project 5 xxxxxxxxxxxxxxxxxxxxxxx",
      detail: "Please input details",
      user_email: "kate@tecky.io",
      category_id: "Sport",
      chat_room_url: "",
      youtube_url: "",
      criteria: "",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
    {
      title: "Project 6 xxxxxxxxxxxxxxxxxxxxxxx",
      detail: "Please input details",
      user_email: "wayne@tecky.io",
      category_id: "Sport",
      chat_room_url: "",
      youtube_url: "",
      criteria: "",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
  ];


  let project_image = [];
  for (let i = 1; i <= 4000; i++) {
    project_image[i - 1] = {
      project_id: i,
      project_image_url: "Project"+Math.round(Math.random() * 8 + 1)+".jpg",
  
    };
  }


  let bookmark_list = [];
  for (let i = 1; i <= 5000; i++) {
    let year = Math.round(Math.random() * 1);
    let month = (year==0)?Math.round(Math.random() * 11 + 1):Math.round(Math.random() * 10 + 1);
    let liked_at =
      "202" + year + "-" + month + "-02 19:27:58.980091+08";
    let unliked_at =
      "202" + year + "-" + month + "-03 19:27:58.980091+08";
    let created_at = liked_at;
      bookmark_list[i - 1] = {
      user_id: Math.round(Math.random() * 499 + 1),
      project_id: Math.round(Math.random() * 2999 + 1),
      liked_at,
      unliked_at,
      created_at
    };
  }


  



  let project_talent = [];
  for (let i = 1; i <= 9000; i++) {
    let year = Math.round(Math.random() * 1);
    let month = (year==0)?Math.round(Math.random() * 11 + 1):Math.round(Math.random() * 10 + 1);
    let user_id= Math.round(Math.random() * 499 + 1);
    let project_id= Math.round(Math.random() * 3999 + 1);
    let talent_id= Math.round(Math.random() * 100 + 1);

    project_talent[i - 1] = {
      talent_id,
      project_id,
    };
  }


  let user_talent = [];
  for (let i = 1; i <= 4000; i++) {
    let year = Math.round(Math.random() * 1);
    let month = (year==0)?Math.round(Math.random() * 11 + 1):Math.round(Math.random() * 10 + 1);
    let user_id= Math.round(Math.random() * 1000 + 1);
    let project_id= Math.round(Math.random() * 2999 + 1);
    let talent_id= Math.round(Math.random() * 150+ 1);

    user_talent[i - 1] = {
      talent_id,
      user_id,
    };
  }


 


  
  // insert data into table
  await knex.insert(school_list).into("school").returning("*");
  await knex.insert(user_list_admin).into("user_info").returning("*");
  await knex.insert(user_list).into("user_info").returning("*");

  await knex
    .insert(project_category_list)
    .into("project_category")
    .returning("*");

  await knex.insert(talent_list).into("talent").returning("*");

  await knex.insert(project_list).into("project").returning("*");

  await knex.insert(project_image).into("project_image").returning("*");

  await knex.insert(bookmark_list).into("bookmark_list").returning("*");





  let project_discussion = [];
  for (let i = 1; i <= 6000; i++) {
    let year = Math.round(Math.random() * 1);
    let month = (year==0)?Math.round(Math.random() * 11 + 1):Math.round(Math.random() * 10 + 1);
    let discussion =chance.sentence({ words: Math.ceil(Math.random() * 5) });
    let project_id= Math.round(Math.random() * 2999 + 1);
    let created = await knex.select("created_at").from("project").where("id", project_id);
    let created_at = created[0]["created_at"]
      project_discussion[i - 1] = {
      user_id: Math.round(Math.random() * 499 + 1),
      project_id: Math.round(Math.random() * 2999 + 1),
      discussion,
    };
  }


  await knex.insert(project_discussion).into("project_discussion").returning("*");





  let project_participant = [];
  for (let i = 1; i <= 5000; i++) {
    let year = Math.round(Math.random() * 1);
    let month = (year==0)?Math.round(Math.random() * 11 + 1):Math.round(Math.random() * 10 + 1);
    let user_id= Math.round(Math.random() * 499 + 1);
    let project_id= Math.round(Math.random() * 2999 + 1);
    let approver= await knex.select("user_id").from("project").where("id", project_id);
    let approver_id = approver[0]["id"]
    let detail= chance.sentence();
    let created = await knex.select("created_at").from("project").where("id", project_id);
    let created_at = created[0]["created_at"]
    project_participant[i - 1] = {
      user_id,
      project_id,
      approver_id,
      detail,
      created_at,
    };
  }

  await knex.insert(project_participant).into("project_participant").returning("*");

  await knex.insert(project_talent).into("project_talent").returning("*");

  await knex.insert(user_talent).into("user_talent").returning("*");


  let competition_list = [];
  for (let i = 1; i <= 2300; i++) {
    let year = Math.round(Math.random() * 1);
    let month = (year==0)?Math.round(Math.random() * 11 + 1):Math.round(Math.random() * 10 + 1);
    let created_at = "202" + year + "-" + month + "-02 19:27:58.980091+08";
    let date = Math.round(Math.random() * 27 + 1);
    let start_date =
      "2022" + year + "-" + month + "-25 19:27:58.980091+08";
    

    competition_list[i - 1] = {
      title: "Competition name " +month+"/202"+year+" - "+i,
      short_desc: "competition detail -  generate automatically",
      image_url: "competition"+Math.round(Math.random() * 8 + 1)+".jpg",
      detail: chance.paragraph({ sentences: 1 }),
      url: chance.url(),
      start_date,
      created_at,
    };
  }

  await knex.insert(competition_list).into("competition").returning("*");


  let angel_fund = [];
  for (let i = 1; i <= 5000; i++) {
    let year = Math.round(Math.random() * 1);
    let month = (year==0)?Math.round(Math.random() * 11 + 1):Math.round(Math.random() * 10 + 1);
    let randomYear = year+1;
    let application_deadline =
      "2023-" + month + "-26 19:27:58.980091+08";
    let project_deadline =
      "2023-" + month + "-27 19:27:58.980091+08";
    let created_at = "202" + year + "-" + month + "-02 19:27:58.980091+08";
    let file_url = "angel_fund"+Math.round(Math.random() * 8 + 1)+".jpg";
    let detail= chance.sentence() + " "+ i;
    angel_fund[i - 1] = {
      title: "Angel fund " +" "+month+"/202"+randomYear +"- "+i ,
      short_desc: "Angel fund short_desc",
      file_url,
      detail,
      application_deadline,
      created_at,
    };
  }

  await knex.insert(angel_fund).into("angel_fund").returning("*");



  let angel_fund_participant = [];
  for (let i = 1; i <= 2000; i++) {
    let year = Math.round(Math.random() * 1);
    let month = (year==0)?Math.round(Math.random() * 11 + 1):Math.round(Math.random() * 9 + 1);
    let applied_at =
      "202" + year + "-" + month + "-04 19:27:58.980091+08";
    let created_at = "202" + year + "-" + month + "-01 19:27:58.980091+08";
    let user_id= Math.round(Math.random() * 499 + 1);
    let angel_fund_id= Math.round(Math.random() * 300 + 1);
    let detail= chance.sentence();
    let file_url = Math.random() > 0.66? "proposal1.pdf": Math.random() > 0.33? "proposal2.pdf": "proposal3.pdf";
    angel_fund_participant[i - 1] = {
      user_id,
      angel_fund_id,
      detail,
      file_url,
      applied_at,
      created_at
          
    };
  }

  await knex.insert(angel_fund_participant).into("angel_fund_participant").returning("*");


  let complaint_list = [];
  for (let i = 1; i <= 10; i++) {
    let year = Math.round(Math.random() * 1);
    let month = (year==0)?Math.round(Math.random() * 11 + 1):Math.round(Math.random() * 10 + 1);
    let applied_at =
      "202" + year + "-" + month + "-03 19:27:58.980091+08";
    let created_at = "202" + year + "-" + month + "-01 19:27:58.980091+08";
    let user_id= Math.round(Math.random() * 499 + 1);
    let project_id= Math.round(Math.random() * 2999 + 1);
    let discussion_id= Math.round(Math.random() * 800 + 1);
    let respondent_id_at_knex = await knex.select("user_id").from("project_discussion").where("id", discussion_id);
    let respondent_id = respondent_id_at_knex[0]["user_id"]
    let discussion_created_at_knex = await knex.select("created_at").from("project_discussion").where("id", discussion_id);
    let discussion_created_at = discussion_created_at_knex[0]["created_at"]
    let discussion_detail_knex =await knex.select("discussion").from("project_discussion").where("id", discussion_id);
    let discussion_detail = discussion_detail_knex[0]["discussion"]
    let detail = chance.sentence();

    let file_url = Math.random() > 0.66? "proposal1.pdf": Math.random() > 0.33? "proposal2.pdf": "proposal3.pdf";
    complaint_list[i - 1] = {
      user_id,
      project_id,
      respondent_id,
      discussion_detail,
      discussion_created_at,
      detail
          
    };
  }

  await knex.insert(complaint_list).into("complaint").returning("*");

}
