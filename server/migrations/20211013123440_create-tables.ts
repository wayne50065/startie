import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {



    await knex.schema.createTable('school',table=>{
        table.increments();
        table.string('school_type').notNullable();
        table.string('domain').notNullable();
        table.string('code').notNullable();
        table.string('english_name').notNullable();
        table.string('chinese_name').notNullable();
        table.string('email_domains').notNullable();
        table.string('school_icon');
        table.timestamps(false,true);
    })

    await knex.schema.createTable('user_info',table=>{
        table.increments();
        table.string('email_address').notNullable();
        table.string('name').notNullable();
        table.string('title');
        table.string('self_introduction');
        table.string('education_level');
        table.string('year');
        table.string('mode_of_study');
        table.integer('school_id').unsigned();
        table.foreign('school_id').references("school.id");
        table.string('major');
        table.string('faculty');
        table.boolean('is_admin')//.notNullable();
        table.string('password_hash');
        table.string('personal_image_url');
        table.string('student_card_url');
        table.timestamp('approved_at');
        table.timestamp('blocked_at');
        table.timestamps(false,true);
    })


    await knex.schema.createTable('project_category',table=>{
        table.increments();
        table.string('category_name').unique();
        table.timestamps(false,true);
    })

    await knex.schema.createTable('project',table=>{
        table.increments();
        table.string('title').notNullable();
        table.string('detail').notNullable();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references("user_info.id");
        table.integer('category_id').unsigned();
        table.foreign('category_id').references("project_category.id");
        table.string('chat_room_url');
        table.string('youtube_url');
        table.string('criteria');
        table.timestamp('application_deadline');
        table.timestamp('project_deadline');
        table.timestamp('canceled_at');
        table.timestamp('blocked_at');
        table.timestamps(false,true);
    })

    await knex.schema.createTable('project_image',table=>{
        table.increments();
        table.integer('project_id').unsigned();
        table.foreign('project_id').references("project.id");
        table.string('project_image_url');
        table.timestamps(false,true);
    })


    await knex.schema.createTable('project_participant',table=>{
        table.increments();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references("user_info.id");
        table.integer('project_id').unsigned();
        table.foreign('project_id').references("project.id");
        table.integer('approver_id');
        table.foreign('approver_id').references("user_info.id");
        table.string('detail');
        table.timestamp('applied_at'); 
        table.timestamp('quitted_at'); 
        table.timestamp('approved_at'); 
        table.timestamp('rejected_at');
        table.timestamps(false,true);
    })

    await knex.schema.createTable('project_discussion',table=>{
        table.increments();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references("user_info.id");
        table.integer('project_id').unsigned();
        table.foreign('project_id').references("project.id");
        table.string('discussion').notNullable();       
        table.timestamps(false,true);
    })

    await knex.schema.createTable('bookmark_list',table=>{
        table.increments();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references("user_info.id");
        table.integer('project_id').unsigned();
        table.foreign('project_id').references("project.id");
        table.timestamp('liked_at');
        table.timestamp('unliked_at');
        table.timestamps(false,true);
    })

    await knex.schema.createTable('talent',table=>{
        table.increments();
        table.string('skill').unique().notNullable();
        table.timestamps(false,true);
    })

    await knex.schema.createTable('project_talent',table=>{
        table.increments();
        table.integer('project_id').unsigned();
        table.foreign('project_id').references("project.id");
        table.integer('talent_id').unsigned();
        table.foreign('talent_id').references("talent.id");
        table.timestamp('removed_at');
        table.timestamps(false,true);
    })


    await knex.schema.createTable('user_talent',table=>{
        table.increments();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references("user_info.id");
        table.integer('talent_id').unsigned();
        table.foreign('talent_id').references("talent.id");
        table.timestamp('removed_at');
        table.timestamps(false,true);
    })

    await knex.schema.createTable('competition',table=>{
        table.increments();
        table.string('title').notNullable();
        table.string('short_desc')
        table.string('image_url');
        table.string('detail');
        table.string('url');
        table.timestamp('start_date');
        table.timestamp('cancel_date');
        table.timestamps(false,true);
    })

    await knex.schema.createTable('project_competition',table=>{
        table.increments();
        table.integer('project_id').unsigned();
        table.foreign('project_id').references("project.id");
        table.integer('competition_id').unsigned();
        table.foreign('competition_id').references("competition.id");
        table.timestamps(false,true);
    })


    await knex.schema.createTable('angel_fund',table=>{
        table.increments();
        table.string('title').notNullable().unique();
        table.string('short_desc');
        table.string('detail');
        table.string('file_url');
        table.timestamp('application_deadline');
        table.timestamp('canceled_at');
        table.timestamp('blocked_at');
        table.timestamps(false,true);
    })


    await knex.schema.createTable('angel_fund_participant',table=>{
        table.increments();
        table.integer('user_id').unsigned();
        table.foreign('user_id').references("user_info.id");
        table.integer('angel_fund_id').unsigned();
        table.foreign('angel_fund_id').references("angel_fund.id");
        table.string('detail');
        table.string('file_url');
        table.integer('approver_id');
        table.foreign('approver_id').references("user_info.id");
        table.timestamp('applied_at'); 
        table.timestamp('quitted_at'); 
        table.timestamp('approved_at'); 
        table.timestamp('rejected_at');
        table.timestamps(false,true);
    })


await knex.schema.createTable('complaint',table=>{
    table.increments();
    table.integer('user_id').unsigned();
    table.foreign('user_id').references("user_info.id");
    table.integer('project_id').unsigned();
    table.foreign('project_id').references("project.id");
    table.integer('respondent_id').unsigned();
    table.foreign('respondent_id').references("user_info.id");
    table.integer('discussion_id').unsigned();
    table.foreign('discussion_id').references("project_discussion.id");
    table.string('discussion_detail'); 
    table.timestamp('discussion_created_at');
    table.timestamp('reviewed_at'); 
    table.string('detail');                 
    table.timestamps(false,true);
})


await knex.schema.createTable('api_log',table=>{
    table.increments();
    table.string('table_name').notNullable();
    table.string('system_action').notNullable();
    table.string('api_user_email_address');
    table.string('project_title');
    table.string('project_participant_email_address');
    table.string('approver_email_address');   
    table.string('discussion');
    table.string('competition_title');
    table.string('angel_fund_title');  
    table.string('complaint_text');
    table.string('respondent_email_address');
    table.string('api_url');
    table.string('api_method');
    table.string('api_name');
    table.string('remark');    
    table.timestamp('applied_at');         
    table.timestamp('approved_at');
    table.timestamp('rejected_at');
    table.timestamp('canceled_at');
    table.timestamp('blocked_at');
    table.timestamp('liked_at');
    table.timestamp('unliked_at');
    table.timestamp('reviewed_at');      
    table.timestamps(false,true);
})

    // action means search/select(see)/update


}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('api_log');
    await knex.schema.dropTable('complaint');
    await knex.schema.dropTable('angel_fund_participant');
    await knex.schema.dropTable('angel_fund');
    await knex.schema.dropTable('project_competition');
    await knex.schema.dropTable('competition');
    await knex.schema.dropTable('user_talent');
    await knex.schema.dropTable('project_talent');
    await knex.schema.dropTable('talent');
    await knex.schema.dropTable('bookmark_list');
    await knex.schema.dropTable('project_discussion');
    await knex.schema.dropTable('project_participant');
    await knex.schema.dropTable('project_image');
    await knex.schema.dropTable('project');
    await knex.schema.dropTable('project_category');
    await knex.schema.dropTable('user_info');
    await knex.schema.dropTable('school');

}

