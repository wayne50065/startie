export type EmailType = {
  Eng: string
  Chin: string
  EmailDomains: string[]
}

export type Institution = {
  Type:
    | 'UGC-funded universities'
    | 'Self-funded institutions'
    | 'Public institutions'
    | 'Sub-degree institutions'
    | 'Member Institutions of the Vocational Training Council'
  Code: string
  EnglishName: string
  ChineseName: string
  Domain: string
  EmailDomains: string[]
}

/**
 * reference: https://en.wikipedia.org/wiki/List_of_higher_education_institutions_in_Hong_Kong
 * reference: https://cdcf.ugc.edu.hk/cdcf/searchUniv.action?lang=TC
 * TODO add asso and HD, and diploma
 * */
export let Institutions: Institution[] = [
  {
    Type: 'UGC-funded universities',
    Code: 'cu',
    Domain: 'cuhk.edu.hk',
    EnglishName: 'The Chinese University of Hong Kong',
    ChineseName: '香港中文大學',
    EmailDomains: ['cuhk.edu.hk', 'link.cuhk.edu.hk'],
  },
  {
    Type: 'UGC-funded universities',
    Code: 'cityu',
    Domain: 'cityu.edu.hk',
    EnglishName: 'City University of Hong Kong',
    ChineseName: '香港城市大學',
    EmailDomains: ['my.cityu.edu.hk'],
  },
  {
    Type: 'UGC-funded universities',
    Code: 'edu',
    Domain: 'eduhk.hk',
    EnglishName: 'The Education University of Hong Kong',
    ChineseName: '香港教育大學',
    EmailDomains: ['s.eduhk.hk'],
  },
  {
    Type: 'UGC-funded universities',
    Code: 'bu',
    Domain: 'hkbu.edu.hk',
    EnglishName: 'Hong Kong Baptist University',
    ChineseName: '香港浸會大學',
    EmailDomains: ['hkbu.edu.hk', 'ife.hkbu.edu.hk'],
  },
  {
    Type: 'UGC-funded universities',
    Code: 'polyu',
    Domain: 'polyu.edu.hk',
    EnglishName: 'The Hong Kong Polytechnic University',
    ChineseName: '香港理工大學',
    EmailDomains: ['polyu.edu.hk', 'connect.polyu.hk'],
  },
  {
    Type: 'UGC-funded universities',
    Code: 'ust',
    Domain: 'ust.hk',
    EnglishName: 'The Hong Kong University of Science and Technology',
    ChineseName: '香港科技大學',
    EmailDomains: ['connect.ust.hk', 'ust.hk', 'stu.ust.hk'],
  },
  {
    Type: 'UGC-funded universities',
    Code: 'lu',
    Domain: 'ln.edu.hk',
    EnglishName: 'Lingnan University',
    ChineseName: '嶺南大學',
    EmailDomains: ['LN.edu.hk'.toLowerCase(), 'LN.hk'.toLowerCase()],
  },
  {
    Type: 'UGC-funded universities',
    Code: 'hku',
    Domain: 'hku.hk',
    EnglishName: 'The University of Hong Kong',
    ChineseName: '香港大學',
    EmailDomains: ['connect.hku.hk'],
  },
  {
    Type: 'Self-funded institutions',
    Code: 'ou',
    Domain: 'ouhk.edu.hk',
    EnglishName: 'The Open University of Hong Kong',
    ChineseName: '香港公開大學',
    EmailDomains: ['ouhk.edu.hk'],
  },
  {
    Type: 'Self-funded institutions',
    Code: 'syu',
    Domain: 'hksyu.edu',
    EnglishName: 'Hong Kong Shue Yan University',
    ChineseName: '香港樹仁大學',
    EmailDomains: ['hksyu.edu', 'alumni.hksyu.edu.hk'],
  },
  {
    Type: 'Self-funded institutions',
    Code: 'hsu',
    Domain: 'hsu.edu.hk',
    EnglishName: 'Hang Seng University of Hong Kong',
    ChineseName: '香港恒生大學',
    EmailDomains: ['hsu.edu.hk'],
  },
  {
    Type: 'Member Institutions of the Vocational Training Council',
    Code: 'ive',
    Domain: 'ive.edu.hk',
    EnglishName: 'Hong Kong Institute of Vocational Education',
    ChineseName: '香港專業教育學院',
    EmailDomains: ['ive.edu.hk'],
  },
  {
    Type: 'Member Institutions of the Vocational Training Council',
    Code: 'thei',
    Domain: 'thei.edu.hk',
    EnglishName: 'Technological and Higher Education Institute of Hong Kong',
    ChineseName: '香港高等教育科技學院',
    EmailDomains: ['thei.edu.hk'],
  },
  {
    Type: 'Member Institutions of the Vocational Training Council',
    Code: 'vtc',
    Domain: 'vtc.edu.hk',
    EnglishName: 'Vocational Training Council',
    ChineseName: '職業訓練局',
    EmailDomains: ['vtc.edu.hk', 'stu.vtc.edu.hk'],
  },
]
for (const institution of Institutions) {
  institution.Domain = institution.Domain.toLowerCase()
  institution.EmailDomains = institution.EmailDomains.map(s => s.toLowerCase())
}

export let email_list: EmailType[] = []
Institutions.forEach(x => {
  email_list.push({
    Chin: x.ChineseName,
    Eng: x.EnglishName,
    EmailDomains: x.EmailDomains,
  })
})

