import express, { Request, Response } from "express";
import { AutoCompleteService } from "../service/AutoCompleteService";
import { ExpressController } from "./ExpressController";

export class AutoCompleteController extends ExpressController {


    constructor(private autoCompleteService: AutoCompleteService) {
        super()
        this.router.get('/talent/search', this.searchTalent)
        this.router.get('/category/search', this.searchCategory)
    }

    searchTalent = (req: Request, res: Response) => {
        const q = req.query.q
        if (typeof q !== 'string') {
            res.json({ error: 'missing string q in req.query' })
            return
        }
        this.callAPI(req, res, () => this.autoCompleteService.searchTalentList(q))
    }

    searchCategory = (req: Request, res: Response) => {
        const q = req.query.q
        if (typeof q !== 'string') {
            res.json({ error: 'missing string q in req.query' })
            return
        }
        this.callAPI(req, res, () => this.autoCompleteService.searchCategoryList(q))
    }

}