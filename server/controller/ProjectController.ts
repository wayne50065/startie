import express, { Request, Response } from "express";
import { ProjectService } from "../service/ProjectService";
import { NewProject } from "../utils/types";
import { get, getBody } from "../utils/validate";
import "../utils/models"
import { isLoggedIn } from "../utils/guard";
import { todoMiddleware } from "../utils/middleware";
import multer from "multer";
import { storage, upload } from "../utils/storage";
export class ProjectController {
  constructor(private projectService: ProjectService) { }

  toRouter() {
    const router = express.Router();
    router.get('/idea/all', this.getIdeaList)
    router.get('/idea/:id', this.getIdeaDetail)
    router.post('/idea', isLoggedIn, upload.array('images', 5), this.createProject)
    router.patch('/idea/:id', todoMiddleware)
    router.patch('/idea/cancel/:id', this.cancelProject)
    router.post('/idea/:id/apply', isLoggedIn, this.addProjectParticipant)
    router.get('/idea/:id/apply', this.getProjectParticipant)
    router.patch('/idea/:id/apply', this.quitProjectParticipant)
    router.patch('/idea/approveApply/:id', isLoggedIn, this.approveProjectParticipant)
    router.patch('/idea/rejectApply/:id', isLoggedIn, this.rejectProjectParticipant)
    router.post('/bookmark/idea/:id', this.addBookmark)
    router.get('/bookmark', isLoggedIn, this.getBookmarkList)
    router.patch('/bookmark/idea/:id', isLoggedIn, this.updateIdeaBookmark)
    router.patch('/bookmark/:id', isLoggedIn, this.updateBookmarkPage)
    router.post('/idea/:id/message', isLoggedIn, this.createMessage)
    router.get('/idea/:id/message', this.getMessage)


    router.get('/profile/created/:id', this.getCreatedProjectDetails)
    router.get('/profile/joined/:id', this.getJoinedProjectDetails)
    router.get('/profile/going/:id', this.getGoingProjectDetails)

    return router
  }


  getCreatedProjectDetails = async (req: Request, res: Response) => {
    const user_id = parseInt(req.params.id);
    console.log("get inside getCreatedProjectDetails controller, id :", user_id)
    try {
      const result = await this.projectService.getCreatedProjectDetails(user_id)
      console.log("GET data successfully @getCreatedProject")
      res.status(200).json(result)
    } catch (error) {
      res.status(500).json(`Fail to run getCreatedProjectDetails @projectService ${error}`)
    }
  }
  getJoinedProjectDetails = async (req: Request, res: Response) => {
    const user_id = parseInt(req.params.id);
    console.log("get inside getJoinedProjectDetails controller, id :", user_id)
    try {
      const result = await this.projectService.getJoinedProjectDetails(user_id)
      console.log("GET data successfully @getJoinedProject")
      res.status(200).json(result)
    } catch (error) {
      res.status(500).json(`Fail to run getJoinedProjectDetails @projectService ${error}`)
    }
  }
  getGoingProjectDetails = async (req: Request, res: Response) => {
    const user_id = parseInt(req.params.id);
    console.log("get inside getGoingProjectDetails controller, id :", user_id)
    try {
      const result = await this.projectService.getGoingProjectDetails(user_id)
      console.log("GET data successfully @getGoingProject")
      res.status(200).json(result)
    } catch (error) {
      res.status(500).json(`Fail to run getGoingProjectDetails @projectService ${error}`)
    }
  }
  getIdeaList = async (req: Request, res: Response) => {
    // this API is get all approved project detail to be displayed on idea page
    try {
      let ideaList = await this.projectService.getIdeaList();
      res.json({ ideaList });
    } catch (error) {
      console.error(error);
      res.json({ error: (error as Error).toString() });
    }
  };

  getIdeaDetail = async (req: Request, res: Response) => {
    try {
      let id = parseInt(req.params.id);
      if (!id) {
        res.json({ error: "Missing id in params" });
        return;
      }
      let ideaDetail = await this.projectService.getIdeaDetail({ project_id: id, user_id: req.jwtpayload?.id });
      res.json({ ideaDetail });
      console.log()
    } catch (error) {
      console.error(error);
      res.json({ error: (error as Error).toString() });
    }
  };

  createProject = async (req: Request, res: Response) => {
    try {
      let new_talent_needed_list = req.body.new_talent_needed_list || []
      let talent_needed_id_list = req.body.talent_needed_id_list || []
      if (typeof talent_needed_id_list === 'string') {
        talent_needed_id_list = [talent_needed_id_list]
      }
      if (new_talent_needed_list.length == 0 && talent_needed_id_list.length == 0) {
        res.status(400).json({ error: 'missing new_talent_needed nor talent_needed_id_list in req.body' })
        return
      }

      // this API is get all approved project detail to be displayed on idea page
      const new_project: NewProject = {
        user_id: req.jwtpayload.id,
        title: getBody(req, "title"),
        // new_category: category,
        // category_id,
        category: getBody(req, "category"),
        detail: getBody(req, "detail"),
        project_deadline: getBody(req, "project_deadline"),
        new_talent_needed_list,
        talent_needed_id_list,
        criteria: req.body.criteria,
        images: ((req.files as Express.Multer.File[]) || []).map(
          (f) => f.filename
        ),
        youtube_url: req.body.youtube_url,
        chat_room_url: req.body.chat_room_url,
        competition_name: req.body.competition_name,
        competition_url: req.body.competition_url,
        competition_date: req.body.competition_date,
        competition_id_list: req.body.competition_id_list || [],
        new_competition_list: req.body.new_competition_list || []
      };
      let json = await this.projectService.createProject(new_project);
      res.json(json);
    } catch (error) {
      res.status(500).json({ error: (error as Error).toString() });
      console.log(error)
    }
  };

  updateProject = async (req: Request, res: Response) => {
    try {
      // if ("dev") {
      //   console.log(req.files);
      //   res.json("TODO");
      //   return;
      // }

      // this API is get all approved project detail to be displayed on idea page
      // const new_project: NewProject = {
      //   user_id: req.jwtpayload.id,
      //   title: getBody(req, "title"),
      //   new_category: getBody(req, "new_category"),
      //   category_id: getBody(req, "category_id"),
      //   detail: getBody(req, "detail"),
      //   project_deadline: getBody(req, "project_deadline"),
      //   new_talent_needed_list: getBody(req, "new_talent_needed_list"),
      //   talent_needed_id_list: getBody(req, "talent_needed_id_list"),
      //   criteria: getBody(req, "criteria"),
      //   files: ((req.files as Express.Multer.File[]) || []).map(
      //     (f) => f.filename
      //   ),
      //   chat_room_url: getBody(req, "chat_room_url"),
      //   competition_name: getBody(req, "competition_name"),
      //   competition_url: getBody(req, "competition_url"),
      //   competition_date: getBody(req, "competition_date"),
      //   application_deadline: getBody(req, "application_deadline"),
      //   competition_id_list: getBody(req, "competition_id_list"),
      //   new_competition_list: getBody(req, "new_competition_list"),
      // };
      // console.log(new_project)
      // let json = await this.projectService.createProject(new_project);
      // res.json(json);
    } catch (error) {
      res.status(500).json({ error: (error as Error).toString() });
      console.log(error)
    }
  };

  cancelProject = async (req: Request, res: Response) => {
    try {
      // let user_id = req.jwtpayload.id;
      let id = parseInt(req.params.id);
      let email_address = req.jwtpayload.emailAddress;
      let json = await this.projectService.cancelProject(req, id, email_address)
      res.json(json)
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }

  addProjectParticipant = async (req: Request, res: Response) => {
    let email_address = req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;
    try {
      let user_id = req.jwtpayload.id
      let project_id = parseInt(req.params.id)

      let detail = req.body.about

      let id = await this.projectService.addProjectParticipant(req, user_id, project_id, email_address, detail)
      res.json({ id })
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }

  getProjectParticipant = async (req: Request, res: Response) => {
    try {
      // let user_id = req.jwtpayload.id
      let project_id = parseInt(req.params.id)
      let email_address = req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;
      let ideaParticipant = await this.projectService.getProjectParticipant(req, project_id, email_address)
      res.json({ ideaParticipant })
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }

  quitProjectParticipant = async (req: Request, res: Response) => {
    try {
      let user_id = req.jwtpayload.id;
      let project_id = parseInt(req.params.id);
      let email_address = req.jwtpayload.emailAddress;
      let json = await this.projectService.quitProjectParticipant(req, project_id, user_id, email_address)
      res.json(json)
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }


  approveProjectParticipant = async (req: Request, res: Response) => {
    try {
      let id = parseInt(req.params.id);
      let email_address = req.jwtpayload.emailAddress;
      let json = await this.projectService.approveProjectParticipant(req, id, email_address)
      res.json(json)
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }

  rejectProjectParticipant = async (req: Request, res: Response) => {
    try {
      let id = parseInt(req.params.id);
      let email_address = req.jwtpayload.emailAddress;
      let json = await this.projectService.rejectProjectParticipant(req, id, email_address)
      res.json(json)
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }


  createMessage = async (req: Request, res: Response) => {
    try {
      let user_id = req.jwtpayload.id
      let project_id = parseInt(req.params.id)
      let discussion = req.body.message
      let email_address = req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;
      let id = await this.projectService.createMessage(req, user_id, project_id, email_address, discussion)
      res.json({ id })
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }


  getMessage = async (req: Request, res: Response) => {
    try {
      // let user_id = req.jwtpayload.id
      let project_id = parseInt(req.params.id)
      let email_address = req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;
      let ideaMessage = await this.projectService.getMessage(req, project_id, email_address)
      res.json({ ideaMessage })
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }

  getBookmarkList = async (req: Request, res: Response) => {
    try {
      let user_id = req.jwtpayload.id;
      if (!user_id) {
        res.json({ error: "Missing user id" });
        return;
      }
      let email_address = req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;
      let bookmarkList = await this.projectService.getBookmarkList(req, user_id, email_address);
      res.json({ bookmarkList });
    } catch (error) {
      res.json({ error: (error as Error).toString() });
    }
  };

  addBookmark = async (req: Request, res: Response) => {
    try {
      let user_id = req.jwtpayload.id;
      let project_id = parseInt(req.params.id);
      let email_address = req.jwtpayload.emailAddress;
      let json = await this.projectService.addBookmark(req, project_id, user_id, email_address)
      res.json(json)
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }

  updateIdeaBookmark = async (req: Request, res: Response) => {
    try {
      let user_id = req.jwtpayload.id;
      let project_id = parseInt(req.params.id);
      let email_address = req.jwtpayload.emailAddress;
      let json = await this.projectService.updateIdeaBookmark(req, project_id, user_id, email_address)
      res.json(json)
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }

  updateBookmarkPage = async (req: Request, res: Response) => {
    try {
      // let user_id = req.jwtpayload.id;
      let id = parseInt(req.params.id);
      let email_address = req.jwtpayload.emailAddress;
      let json = await this.projectService.updateBookmarkPage(req, id, email_address)
      res.json(json)
    } catch (error) {
      res.json({
        error: (error as Error).toString(),
      })
    }
  }






























}
