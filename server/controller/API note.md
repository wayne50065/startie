| Index | Title                                           | priority | owner | flow | status                  | remark                     |
|-------|-------------------------------------------------|----------|-------|------|-------------------------|----------------------------|
| 01    | Register                                        |          | W     | done | check by wayne          |                            |
| 02    | Login                                           |          | W     | done | check by wayne          |                            |
| 03    | login with facebook                             | Cancel   | -     |      |                         |                            |
| 04    | Logout                                          |          | W     | done | check by wayne          |                            |
| 05    | update profile detail                           |          | -     |      |                         |                            |
| 06    | search ppl in people list                       |          | W     |      |                         |                            |
| 07    | get bookmark list                               |          | M     |      |                         |                            |
| 08    | get ppl detail to profile page                  |          |       |      |                         |                            |
| 09    | Create Project                                  |          | M     | done | check by Kate           |                            |
| 10    | get project list to idea page                   |          | K     | done | in progress             | need to set more criteria  |
| 11    | get project detail to each project page         |          | K     |      |                         |                            |
| 12    | Update project                                  | postpone | -     |      |                         |                            |
| 13    | Cancel project   (close project -> cancel date) |          |       |      |                         |                            |
| 14    | Apply project - join in participant             |          | K     | done | check by Kate           |                            |
| 15    | quit project                                    |          | M     |      |                         |                            |
| 16    | bookmark project                                |          | K     | done | check by Kate           |                            |
| 17    | (un)bookmark project                            |          |       |      |                         |                            |
| 18    | send discussion                                 |          | K     | done | check by Kate           |                            |
| 19    | get discussion                                  |          |       |      |                         |                            |
| 20    | edit discussion                                 | postpone | -     |      |                         |                            |
| 21    | get/ search competition list                    |          | M     | done | check by Kate           |                            |
| 22    | get/ search competition detail                  |          | M     | done | check by Kate           |                            |
| 23    | ge fund list                                    |          | K     | done | done                    |                            |
| 24    | get fund detail                                 |          | K     | done | done                    |                            |
| 25    | apply fund application                          |          | K     | done | check by Kate           |                            |
| 26    |                                                 |          |       |      |                         |                            |
| 27    |                                                 |          |       |      |                         |                            |
| 28    |                                                 |          |       |      |                         |                            |
| 29    |                                                 |          |       |      |                         |                            |
| 30    |                                                 |          |       |      |                         |                            |
| 31    |                                                 |          |       |      |                         |                            |
| 32    |                                                 |          |       |      |                         |                            |
| 33    |                                                 |          |       |      |                         |                            |
| 34    |                                                 |          |       |      |                         |                            |
| 35    |                                                 |          |       |      |                         |                            |
| 36    |                                                 |          |       |      |                         |                            |
| 37    |                                                 |          |       |      |                         |                            |
| 38    |                                                 |          |       |      |                         |                            |
| 39    |                                                 |          |       |      |                         |                            |
| 40    |                                                 |          |       |      |                         |                            |
