import express, { Request, Response } from "express";


export class ExpressController {
    router = express.Router()
    
    todo = (req: Request, res: Response) => {
        res.status(501).json({ error: `Not implemented. Method: ${req.method}, Url: ${req.url}` })
    }

    async callAPI(req: Request, res: Response, fn: () => any) {
        try {
            let json = await fn()
            res.json(json)
        } catch (error: any) {
            res.status(500).json({ error: error.toString() })
        }
    }
}