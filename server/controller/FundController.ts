import express, { Request, Response } from "express";
import { FundService } from "../service/FundService";
import { upload } from "../utils/storage";
import "../utils/models";

export class FundController {
  constructor(private fundService: FundService) {}

  toRouter() {
    const router = express.Router();
    router.get("/fund/list/past", this.getPastFundList);
    router.get("/fund/list/current", this.getCurrentFundList);
    router.get("/fund/:id", this.getFundDetail);
    router.post(
      "/fund/:fund_id/apply",
      upload.single("file"),
      this.addAngelFundParticipant
    );
    router.get("/fundApplied/:fund_id",this.getApplyInfo);
    return router;
  }

  // getFundList = async (req: Request, res: Response) => {
  //     // console.log('getFundList')
  //     try {
  //         let fundList = await this.fundService.getFundList()
  //         res.json({ fundList })
  //     } catch (error) {
  //         res.json({error: (error as Error).toString()})
  //     }
  // }
  getApplyInfo= async (req: Request, res: Response) => {
    try {
      const fund_id=parseInt(req.params.fund_id)
      const user_id=req.jwtpayload.id
      const result=await this.fundService.getApplyInfo(user_id,fund_id)
      res.status(200).json(result)
    } catch (error) {
      res.status(500).json({error:"failed to run SQL"})
    }
  }
  getFundDetail = async (req: Request, res: Response) => {
    let email_address =
      req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;

    try {
      let id = parseInt(req.params.id);
      if (!id) {
        res.json({ error: "Missing id in params" });
        return;
      }
      let fundDetail = await this.fundService.getFundDetail(
        req,
        id,
        email_address
      );
      res.json({ fundDetail });
    } catch (error) {
      res.json({ error: (error as Error).toString() });
    }
  };

  getCurrentFundList = async (req: Request, res: Response) => {
    // let user_id = req.jwtpayload===undefined?null:req.jwtpayload.id;
    let email_address =
      req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;

    try {
      let currentFundList = await this.fundService.getFundCurrentList(
        req,
        email_address
      );
      res.json({ currentFundList });
    } catch (error) {
      res.json({ error: (error as Error).toString() });
    }
  };

  getPastFundList = async (req: Request, res: Response) => {
    // let user_id = req.jwtpayload===undefined?null:req.jwtpayload.id;
    let email_address =
      req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;

    try {
      let pastFundList = await this.fundService.getFundPastList(
        req,
        email_address
      );
      res.json({ pastFundList });
    } catch (error) {
      res.json({ error: (error as Error).toString() });
    }
  };

  addAngelFundParticipant = async (req: Request, res: Response) => {
    try {
      const user_id=req.jwtpayload.id
    const email_address=req.jwtpayload.emailAddress
    let fund_id = parseInt(req.params.fund_id);
    console.log({ fund_id });

    const uploads = { 
      email_address,
      fund_id,
      detail: req.body.info, 
      file_url: req.file?.filename,
      user_id,
    };
    console.log("ready to insert into SQL")
    const id = await this.fundService.addAngelFundParticipant(req,uploads)
    res.status(200).json(id)
    } catch (error) {
    res.status(500).json({error:"fail to insert into SQL"})
    }
    
    

    
    // await this.fundService.addAngelFundParticipant(uploads)

    // try {
    //   if (!fund_id) {
    //     res.json({ error: "Missing fund_id in params" });
    //     return;
    //   }

    //   let user_id = req.jwtpayload.id
    //   let email_address = req.jwtpayload===undefined?null:req.jwtpayload.emailAddress;
    //   let detail = req.body.about
    //   let file_url = req.body.url

    //   let angelFundParticipant = await this.fundService.addAngelFundParticipant(req,user_id,fund_id,email_address,detail,file_url)
    //   res.json({ angelFundParticipant })
    // } catch (error) {
    //   res.json({
    //     error: (error as Error).toString(),
    //   })
    // }
  };
}
