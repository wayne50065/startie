import express, { Request, Response } from "express";
import { CompetitionService } from "../service/CompetitionService";
import "../utils/models";

export class CompetitionController {
  constructor(private CompetitionService: CompetitionService) {}

  toRouter() {
    const router = express.Router();
    router.get("/competition/past", this.getCompetitionPastList);
    router.get("/competition/current", this.getCompetitionCurrentList);
    router.get("/competition/:id", this.getCompetitionDetail);
    return router;
  }

  getCompetitionCurrentList = async (req: Request, res: Response) => {
    let email_address =
      req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;
    // let user_id = req.jwtpayload===undefined?null:req.jwtpayload.id;

    try {
      let currentCompetitionList =
        await this.CompetitionService.getCompetitionCurrentList(
          req,
          email_address
        );
      res.json({ currentCompetitionList });
    } catch (error) {
      res.json({ error: (error as Error).toString() });
    }
  };

  getCompetitionPastList = async (req: Request, res: Response) => {
    // let user_id = req.jwtpayload===undefined?null:req.jwtpayload.id;
    let email_address =
      req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;

    try {
      let pastCompetitionList =
        await this.CompetitionService.getCompetitionPastList(
          req,
          email_address
        );
      res.json({ pastCompetitionList });
    } catch (error) {
      res.json({ error: (error as Error).toString() });
    }
  };

  getCompetitionDetail = async (req: Request, res: Response) => {
    // let user_id = req.jwtpayload===undefined?null:req.jwtpayload.id;
    let email_address =
      req.jwtpayload === undefined ? null : req.jwtpayload.emailAddress;

    try {
      let id = parseInt(req.params.id);
      if (!id) {
        res.json({ error: "Missing id in params" });
        return;
      }
      let competitionDetail = await this.CompetitionService.getCompetitionDetail(req,id,email_address);
      res.json({ competitionDetail });
    } catch (error) {
      res.json({ error: (error as Error).toString() });
    }
  };
}
