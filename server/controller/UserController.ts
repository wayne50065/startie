import express, { Request, Response } from "express";

import { UserService } from "../service/UserService";
import { isLoggedIn } from "../utils/guard";
import { logger } from "../utils/loggers";

export class UserController {
  constructor(private userService: UserService) {}

  toRouter() {
    const router = express.Router();
    router.get("/register/:OTP", this.ActivateAccount);
    router.get("/login/:OTP", this.Authentication);
    router.get("/getUser/:id", isLoggedIn, this.GetUserById);
    router.get("/getCurrentUser", isLoggedIn, this.GetCurrentUser);
    router.get("/getAllUserInfo", this.GetAllUserInfo);

    router.post("/login/getOTP", this.LoginGetOTP);
    router.post("/register/getOTP", this.RegisterGetOTP);
    return router;
  }

  ActivateAccount = async (req: Request, res: Response) => {
    const emailOTP = req.params.OTP;
    logger.log({ level: "info", message: "Received activate account request" });
    logger.log({
      level: "info",
      message: `Activating account... OTP => ${emailOTP}`,
    });
    try {
      // check OTP is valid
      const checkResult = await this.userService.CheckOTP(emailOTP);
      if (!checkResult) {
        logger.log({ level: "error", message: "OTP is invalid or expired!" });
        res.status(400).json("OTP is invalid or expired!");
        return;
      }
      //create user in DB
      const userId = await this.userService.CreateUser(emailOTP);
      if (!userId) {
        logger.log({ level: "error", message: "fail to create user!" });
        res.status(400).json("OTP is invalid or expired!");
        return;
      }
      //create token for user
      const token = await this.userService.CreateToken(emailOTP);
      logger.log({ level: "info", message: "Activation Success!" });
      res.status(200).json({ "user created": "True", token });
      return;
    } catch (e) {
      logger.log({
        level: "error",
        message: `Account activation error, ${(e as Error).toString()}`,
      });
      res.status(500).json("Fail to activate Account");
      return;
    }
  };
  Authentication = async (req: Request, res: Response) => {
    let emailOTP = req.params.OTP;
    logger.log({
      level: "info",
      message: "Received login authentication request",
    });
    logger.log({
      level: "info",
      message: `Authorizing account... OTP => ${emailOTP}`,
    });
    // check if OTP is valid
    try {
      const checkResult = await this.userService.CheckOTP(emailOTP);
      if (!checkResult) {
        logger.log({ level: "error", message: "OTP is invalid or expired!" });
        return res.status(404).json("OTP is invalid or expired!");
      }
      const token = await this.userService.CreateToken(emailOTP);
      logger.log({ level: "info", message: "Login Success!" });
      return res.status(200).json({ login: "Success!", token });
    } catch (e) {
      logger.log({
        level: "error",
        message: `Authentication error, ${(e as Error).toString()}`,
      });
      return res.status(500).json("Authentication error");
    }
  };
  RegisterGetOTP = async (req: Request, res: Response) => {
    logger.log({
      level: "info",
      message: "Received get registerOTP request...",
    });
    let { registerEmail, userName } = req.body;
    if (!registerEmail || !userName) {
      logger.log({ level: "error", message: "Email or Username is missing!" });
      return res.status(400).json({ error: "Email or Username is missing!" });
    }
    try {
      //check if user is registered
      let userIsExist = await this.userService.CheckUserInfo(registerEmail);
      if (userIsExist) {
        logger.log({ level: "error", message: "Email has been registered!" });
        return res.status(400).json({ error: "Email has been registered!" });
      } else {
        logger.log({ level: "info", message: "Result: email is valid..." });
      }
    } catch (e) {
      logger.log({
        level: "error",
        message: "Fail to check email if is registered in UserController",
      });
      return res
        .status(500)
        .json({ error: "Fail to check email if is registered" });
    }

    try {
      let emailOTP = await this.userService.GenerateOTP(
        6,
        registerEmail,
        userName
      );
      const registerOTPlink = `${process.env.ORIGIN}/register/${emailOTP}`;
      // const registerOTPlink = `https://startiehk.com/register/${emailOTP}`
      let emailMessage = /*html */ `
                <h4>Hi dear,<br><br>below is your Startie register activation link:</h4>
                <h1>
                <a href=${registerOTPlink}>
                Active my account!
                </a>
                </h1>
                <br>
                <br>
                <br>
                <h4>Please click the above link to verify you email and continue your journey.</h4>
                <p>- This is a automatically generated email. Please do not reply.</p>
                `;
      //sd OTP by email to register
      const result = await this.userService.SendOTP(
        registerEmail,
        emailMessage
      );
      if (!result) {
        return res.status(200).json({ success: "True", OTP: emailOTP });
      } else {
        return res
          .status(500)
          .json({ success: "False", OTP: emailOTP, error: result });
      }
    } catch (e) {
      return res
        .status(500)
        .json({ error: `Fail to send OTP ,${e.toString()}` });
    }
  };

  LoginGetOTP = async (req: Request, res: Response) => {
    logger.log({ level: "info", message: "Received get loginOTP request" });    
    let { loginEmail } = req.body;
    let checkUserResult;
    if (!loginEmail) {
    logger.log({ level: "error", message: "loginEmail is undefined!" });    
      
      return res.status(400).json({ error: "loginEmail is undefined!" });
    }
    try {
      checkUserResult = await this.userService.CheckUserInfo(loginEmail);
      if (!checkUserResult) {
    logger.log({ level: "error", message: "user/ email is invalid!" });    

        return res.status(400).json({ error: "user/ email is invalid!" });
      }
    } catch (e) {
    logger.log({ level: "error", message: "Fail to check user email" });    
      
      return res.status(400).json({ error: "Fail to check user email!" });
    }

    try {
      let emailOTP = await this.userService.GenerateOTP(
        6,
        loginEmail,
        checkUserResult.name
      );
      console.log(emailOTP);

      const loginOTPlink = `${process.env.ORIGIN}/login/${emailOTP}`;
      //   const loginOTPlink = `https://startiehk.com/login/${emailOTP}`;
      let emailMessage = /*html */ `
                <h4>Hi dear,<br><br>below is your Startie login link:</h4>
                <h1>
                <a href=${loginOTPlink}>
                Login to my Startie account!
                </a>
                </h1>
                <br>
                <br>
                <br>
                <h4>Please click the above link to continue your journey.</h4>
                <p>- This is a automatically generated email. Please do not reply.</p>
                `;
      //send OTP by email to login
      await this.userService.SendOTP(loginEmail, emailMessage);
      return res.status(200).json({ emailOTP });
    } catch (e) {
      console.log(e);
      return res
        .status(500)
        .json({ error: `Fail to send OTP ,${e.toString()}` });
    }
  };

  GetUserById = async (req: Request, res: Response) => {
    const user_id = parseInt(req.params.id);
    console.log("getting user ID : ", user_id);
    try {
      const userInfo = await this.userService.GetUser(user_id.toString());

      if (userInfo) {
        res.status(200).json(userInfo);
        return;
      } else {
        res.status(400).json("result not found");
        return;
      }
    } catch (e) {
      res.status(500).json((e as Error).toString());
      return;
    }
  };

  GetCurrentUser = async (req: Request, res: Response) => {
    const user_id = req.jwtpayload.id;
    try {
      const userInfo = await this.userService.GetUser(user_id.toString());

      if (userInfo) {
        res.status(200).json(userInfo);
        return;
      } else {
        res.status(400).json("result not found");
        return;
      }
    } catch (e) {
      res.status(500).json((e as Error).toString());
      return;
    }
  };

  GetAllUserInfo = async (req: Request, res: Response) => {
    try {
      const userInfos = await this.userService.GetAllUserInfo();
      if (userInfos) {
        res.status(200).json(userInfos);
        return;
      } else {
        res.status(400).json("result not found");
        return;
      }
    } catch (e) {
      res.status(500).json((e as Error).toString());
      return;
    }
  };

  //==========================================DEV_for testing============================================

  insertSQL = async (req: Request, res: Response) => {
    const result = await this.userService.Insert_into_userInfo(
      "hihihi@gmail.com"
    );
    console.log(result);
    res.json(result);
  };

  selectSQL = async (req: Request, res: Response) => {
    const emailOTP = req.params.OTP;
    const result = await this.userService.Select_from_userInfo(emailOTP);
    console.log(result);
    res.json(result);
  };
  DEV_checkOTP_Valid = async (req: Request, res: Response) => {
    const emailOTP = req.params.OTP;
    // const result = await this.userService.emailOTP_map.has(emailOTP)
    const result = await this.userService.CheckOTP(emailOTP);
    res.json({ emailOTP, valid: result });
  };
}
