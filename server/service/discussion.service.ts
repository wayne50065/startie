import { DBService } from './db.service'
import { Knex } from 'knex'
import { APILogService } from './api-log.service'
import express from 'express'

export class DiscussionService extends DBService {
  constructor(public knex: Knex, public apiLog: APILogService) {
    super(knex)
  }
  table() {
    return this.knex('project_discussion')
  }
  async submitProjectDiscussion(
    req: express.Request,
    row: { user_id: number; project_id: number },
  ) {
    let [discussion_id] = await this.table().insert(row).returning('id')
    await this.apiLog.insert(req, this.submitProjectDiscussion, {
      discussion_id,
    })
    return { discussion_id }
  }
}
