import { APILogService } from "./api-log.service";

export class AutoCompleteService extends APILogService {

    async searchTalentList(text: string) {
        let talent_list = await this.knex.select('talent.id', 'talent.skill')
            .from('talent')
            .where('skill', 'ilike', `%${text}%`)
            .leftJoin('project_talent','project_talent.talent_id','talent.id')
            .groupBy('talent.id', 'talent.skill')
            .orderByRaw('count(project_talent.id) desc')
            .limit(20)
        return { talent_list }
    }

    async createTalentList(textList: string[]) {
        let rows = await this.knex.insert(textList.map(skill => ({ skill }))).into('talent').returning('id')
        let new_talent_id_list = rows.map(row => row as number)
        return { new_talent_id_list }
    }

    async searchCategoryList(text: string) {
        let category_list = await this.knex.select('project_category.id', 'project_category.category_name')
            .from('project_category')
            .where('category_name', 'ilike', `%${text}%`)
            .groupBy('project_category.id', 'project_category.category_name')
            .orderByRaw('count(project_category.id) desc')
            .limit(20)
        return { category_list }
    }

    async createCategoryList(textList: string[]) {
        let rows = await this.knex.insert(textList.map(category_name => ({ category_name }))).into('project_category').returning('id')
        let new_category_id_list = rows.map(row => row as number)
        return { new_category_id_list }
    }

}