import { Knex } from 'knex'

export class DBService {
  constructor(public knex: Knex) {}
}
