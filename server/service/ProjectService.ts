import { NewProject } from "../utils/types";

import { APIService } from "./api.service";
import { Request } from "express";

export class ProjectService extends APIService {
  async getCreatedProjectDetails(user_id: number) {
    console.log("entered getCreated", user_id);
    try {
      const result = await this.knex
        .select(
          "project.id",
          "project.title",
          "project.criteria",
          this.knex.raw("ARRAY_AGG(project_image.project_image_url) as images"),
          this.knex.raw("ARRAY_AGG(talent.skill) as talents")
        )
        .from("project")
        .leftJoin("project_image", "project.id", "project_image.project_id")
        .leftJoin("project_talent", "project.id", "project_talent.project_id")
        .leftJoin("talent", "talent.id", "project_talent.talent_id")
        .where("project.user_id", user_id)
        .groupBy("project.id", "project.title", "project.criteria")
        .orderBy([{column:"project.id",order:"desc"},{column:"project.created_at",order:"desc"}]);
      console.log(result);

      return result;
    } catch (error) {
      throw new Error("Failed to run SQL");
    }
  }

  async getJoinedProjectDetails(user_id: number) {
    console.log("entered getJoined", user_id);
    try {
      const result = await this.knex
        .select(
          "project.id",
          "project.title",
          "project.criteria",
          "project_participant.approved_at",
          this.knex.raw("ARRAY_AGG(project_image.project_image_url) as images"),
          this.knex.raw("ARRAY_AGG(talent.skill) as talents")
        )
        .from("project")
        .leftJoin(
          "project_participant",
          "project_participant.project_id",
          "project.id"
        )
        .leftJoin("project_image", "project.id", "project_image.project_id")
        .leftJoin("project_talent", "project.id", "project_talent.project_id")
        .leftJoin("talent", "talent.id", "project_talent.talent_id")
        .where("project.project_deadline", "<", this.knex.fn.now())
        .andWhere("project_participant.user_id", user_id)
        .groupBy(
          "project.id",
          "project.title",
          "project.criteria",
          "project_participant.approved_at"
        )
        .orderBy("project_participant.approved_at", "DESC");
      console.log(
        "==============================================================================="
      );
      console.log(result);
      return result;
    } catch (error) {
      throw new Error("Failed to run SQL");
    }
  }

  async getGoingProjectDetails(user_id: number) {
    console.log("entered getJoined", user_id);
    try {
      const result = await this.knex
        .select(
          "project.id",
          "project.title",
          "project.criteria",
          "project_participant.applied_at",
          this.knex.raw("ARRAY_AGG(project_image.project_image_url) as images"),
          this.knex.raw("ARRAY_AGG(talent.skill) as talents")
        )
        .from("project")
        .leftJoin(
          "project_participant",
          "project_participant.project_id",
          "project.id"
        )
        .leftJoin("project_image", "project.id", "project_image.project_id")
        .leftJoin("project_talent", "project.id", "project_talent.project_id")
        .leftJoin("talent", "talent.id", "project_talent.talent_id")
        .where("project.project_deadline", ">", this.knex.fn.now())
        .andWhere("project_participant.user_id", user_id)
        .andWhere("project_participant.quitted_at", null)
        .groupBy(
          "project.id",
          "project.title",
          "project.criteria",
          "project_participant.applied_at"
        )
        .orderBy("project_participant.applied_at", "DESC");
      console.log(
        "==============================================================================="
      );
      console.log(result);
      return result;
    } catch (error) {
      throw new Error("Failed to run SQL");
    }
  }

  async getIdeaList() {
    // let current_time = new Date(Date.now())
    let projects = await this.knex
      .select(
        "project.id",
        "project.title",
        "project.criteria",
        "project.created_at",
        this.knex.raw("ARRAY_AGG(project_image.project_image_url) as images"),
        this.knex.raw("ARRAY_AGG(talent.skill) as talents")
      )
      .from("project")
      .leftJoin(
        "project_participant",
        "project_participant.project_id",
        "project.id"
      )
      .leftJoin("project_image", "project.id", "project_image.project_id")
      .leftJoin("project_talent", "project.id", "project_talent.project_id")
      .leftJoin("talent", "talent.id", "project_talent.talent_id")
      .where("canceled_at", null)
      .andWhere("project_deadline", ">", this.knex.fn.now())
      .groupBy(
        "project.id",
        "project.title",
        "project.criteria",
        "project.created_at"
      )
      .orderBy("project.id","desc");
    console.log(projects);
    return projects;
  }

  async getIdeaDetail(options: { project_id: number; user_id: number }) {
    console.log("getIdeaDetail", options);
    const { project_id, user_id } = options;
    let project = await this.knex
      .select(
        "project.id",
        "project.title",
        "user_info.name as projectOwner",
        "user_id",
        "project_category.category_name as category",
        "project_category.id as category_id",
        "detail",
        "criteria",
        "youtube_url",
        "project_deadline",
        "chat_room_url",
        "project.created_at"
      )
      .from("project")
      .innerJoin("user_info", "user_info.id", "project.user_id")
      .innerJoin(
        "project_category",
        "project_category.id",
        "project.category_id"
      )
      .where("project.id", project_id)
      .first();

    let images = await this.knex
      .select("project_image_url")
      .from("project_image")
      .where({ project_id });
    project.images = images.map((img) => img.project_image_url);

    let rows = await this.knex
      .select("skill")
      .from("talent")
      .innerJoin("project_talent", "project_talent.talent_id", "talent.id")
      .where({ project_id });
    project.talentNeeded = rows.map((row) => row.skill);

    let compete = await this.knex
      .select("title", "url", "start_date")
      .from("competition")
      .innerJoin(
        "project_competition",
        "project_competition.competition_id",
        "competition.id"
      )
      .where({ project_id });
    project.competition_name = compete.map((name) => name.title);
    project.competition_url = compete.map((url) => url.url);
    project.competition_date = compete.map((date) => date.start_date);

    let apply = await this.knex
      .count("* as count")
      .from("project_participant")
      .where({ user_id: user_id || 0, project_id })
      .andWhere("quitted_at", null)
      .first();
    project.has_applied = +apply!.count > 0;

    let quit = await this.knex
      .count("* as count")
      .from("project_participant")
      .where({ user_id: user_id || 0, project_id })
      .andWhereNot("quitted_at", null)
      .first();
    project.has_quitted = +quit!.count > 0;

    let bookmark = await this.knex
      .select("liked_at", "unliked_at")
      .from("bookmark_list")
      .where({ user_id: user_id || 0, project_id })
      .orderBy("id", "desc")
      .first();

    project.has_liked = !!bookmark;
    project.has_unliked = !!bookmark && !!bookmark.unliked_at;

    // let like = await this.knex.count('* as count').from('bookmark_list')
    //   .where({ user_id: user_id || 0, project_id })
    //   .orderBy('id','desc')
    //   .andWhere('unliked_at', null).first()
    // project.has_liked = +like!.count > 0

    // let unlike = await this.knex.count('* as count').from('bookmark_list')
    //   .where({ user_id: user_id || 0, project_id })
    //   .andWhereNot('unliked_at', null).first()
    // project.has_unliked = +unlike!.count > 0

    // let approve = await this.knex.select('* as count').from('project_participant')
    //   .where({ user_id: user_id || 0, project_id })
    //   .andWhereNot('approved_at', null)
    //   .andWhere('rejected_at', null).first()
    // project.has_approved = +approve!.count > 0

    console.log(project);

    return project;
  }

  async createProject(new_project: NewProject) {
    return await this.knex.transaction(async (knex) => {

      let category_id: number;
      let row = await this.knex.select('id').from('project_category').where('category_name', new_project.category).first()
      if (row) {
        category_id = row.id
      } else {
        let rows = await knex
          .insert({ category_name: new_project.category })
          .into("project_category")
          .returning("id");
        category_id = rows[0] as number
      }

      // let category_id: number;
      // if (new_project.category_id) {
      //   category_id = new_project.category_id;
      // } else if (new_project.new_category) {
      //   let rows = await knex
      //     .insert({ category_name: new_project.new_category })
      //     .into("project_category")
      //     .returning("id");
      //   category_id = rows[0] as number;
      // } else {
      //   throw new Error("missing project category");
      // }

      const [project_id] = await knex
        .insert({
          title: new_project.title,
          category_id,
          detail: new_project.detail,
          criteria: new_project.criteria,
          user_id: new_project.user_id,
          youtube_url: new_project.youtube_url,
          chat_room_url: new_project.chat_room_url,
          project_deadline: new_project.project_deadline,
        })
        .into("project")
        .returning("id");
      //then insert images into project_image
      for (let image of new_project.images) {
        await knex
          .insert({ project_id, project_image_url: image })
          .into("project_image");
      }

      for (let new_talent_needed_list of new_project.new_talent_needed_list) {
        let row = await this.knex.select('id').from('talent').where('skill', new_talent_needed_list).first()
        if (row) {
          new_project.talent_needed_id_list.push(row.id);
          continue;
        }
        let [talent_id] = await knex
          .insert({ skill: new_talent_needed_list })
          .into("talent")
          .returning("id");
        new_project.talent_needed_id_list.push(talent_id as number);
      }
      // insert project & talent into project_talent
      for (let talent_id of new_project.talent_needed_id_list) {
        await knex
          .insert({ project_id, talent_id })
          .into("project_talent");
      }

      //then insert competition into competition
      // for (let new_competition of new_project.new_competition_list) {
        let [competition_id] = await knex
          .insert({
            title: new_project.competition_name,
            url: new_project.competition_url,
            start_date: new_project.competition_date
          })
          .into("competition")
          .returning("id");
        // new_project.competition_id_list.push(competition_id as number);
      // }
      //then insert project & competition into project_competition
      // for (let competition_id of new_project.competition_id_list) {
        await knex
          .insert({ project_id, competition_id })
          .into("project_competition");
      // }

      return { project_id };
    });
  }

  //updateProject

  async cancelProject(req: Request, id: number, email_address: string) {
    let current_time = new Date(Date.now());
    let result = await this.knex("project")
      .update({ canceled_at: current_time })
      .where({ id });
    // .andWhere({ user_id });
    return result;
  }

  async addProjectParticipant(
    req: Request,
    user_id: number,
    project_id: number,
    email_address: null | string,
    detail: string
  ) {
    let applied_at = new Date(Date.now());
    console.log({ user_id, project_id, detail, applied_at })
    let [id] = await this.knex
      .insert({ user_id, project_id, detail, applied_at })
      .into("project_participant")
      .returning("id");

    // let row = await this.knex
    // .select("id", "title", "file_url", "detail", "application_deadline")
    // .from("project")
    // .where({ id:project_id })
    // .first();

    // this.api.log(req, this.addProjectParticipant, {

    //   table_name: "project_particiapte",
    //   project_title: row.title,
    //   api_user_email_address:email_address?email_address:null,
    //   applied_at,
    // });

    return id;
  }

  async getProjectParticipant(req: Request, project_id: number, email_address: null | string) {
    let participants = await this.knex
      .select(
        "project_participant.id",
        "user_id as userId",
        "user_info.name as userName",
        "user_info.personal_image_url as userPic",
        "detail",
        "applied_at"
      )
      .from("project_participant")
      .innerJoin("user_info", "user_info.id", "project_participant.user_id")
      .where({ project_id });

    for (let participant of participants) {
      let apply = await this.knex.select('approved_at', 'rejected_at')
        .from("project_participant")
        .where({ project_id })
        .first()

      participant.has_approved = !!apply.approved_at && !apply.rejected_at
      participant.has_rejected = !!apply.rejected_at
    }
    return participants;
  }

  async quitProjectParticipant(
    req: Request,
    project_id: number,
    user_id: number,
    email_address: string
  ) {
    let current_time = new Date(Date.now());
    let result = await this.knex("project_participant")
      .update({ quitted_at: current_time })
      .where({ project_id })
      .andWhere({ user_id });
    return result;
  }

  async approveProjectParticipant(
    req: Request,
    id: number,
    email_address: string
  ) {
    let current_time = new Date(Date.now());
    let result = await this.knex("project_participant")
      .update({ approved_at: current_time })
      .where({ id });
    return result;
  }

  async rejectProjectParticipant(
    req: Request,
    id: number,
    email_address: string
  ) {
    let current_time = new Date(Date.now());
    let result = await this.knex("project_participant")
      .update({ rejected_at: current_time })
      .where({ id });
    // .andWhere({user_id});
    return result;
  }
  async createMessage(
    req: Request,
    user_id: number,
    project_id: number,
    email_address: null | string,
    discussion: string
  ) {
    let [result] = await this.knex
      .insert({ project_id, user_id, discussion })
      .into("project_discussion")
      .returning("id");

    // let row = await this.knex
    // .select("id", "title", "file_url", "detail", "application_deadline")
    // .from("project")
    // .where({ id:project_id })
    // .first();

    // this.api.log(req, this.createMessage, {

    //   table_name: "project_discussion",
    //   project_title: row.title,
    //   api_user_email_address:email_address?email_address:null,

    // });

    return result;
  }

  async getMessage(
    req: Request,
    project_id: number,
    email_address: null | string
  ) {
    let result = await this.knex
      .select(
        "project_discussion.id",
        "user_id",
        "user_info.name as userName",
        "user_info.personal_image_url as userPic",
        "discussion",
        "project_discussion.created_at"
      )
      .from("project_discussion")
      .innerJoin("user_info", "user_info.id", "project_discussion.user_id")
      .where({ project_id });

    // let row = await this.knex
    // .select("id", "title", "file_url", "detail", "application_deadline")
    // .from("project")
    // .where({ id:project_id })
    // .first();

    // this.api.log(req, this.getMessage, {

    //   table_name: "project_discussion",
    //   project_title: row.title,
    //   api_user_email_address:email_address?email_address:null,

    // });

    return result;
  }

  async getBookmarkList(
    req: Request,
    user_id: number,
    email_address: null | string
  ) {
    let result = await this.knex
      .select("bookmark_list.id", "project_id", "project.title as ideaName")
      .from("bookmark_list")
      .innerJoin("project", "project.id", "bookmark_list.project_id")
      .where({ "bookmark_list.user_id": user_id })
      .andWhere("unliked_at", null);

    // this.api.log(req, this.getBookmarkList, {

    //   table_name: "bookmark_list",
    //   api_user_email_address:email_address?email_address:null,
    // });

    return result;
  }

  async addBookmark(
    req: Request,
    project_id: number,
    user_id: number,
    email_address: string
  ) {
    let result = await this.knex
      .insert({
        project_id,
        user_id,
        liked_at: this.knex.fn.now(),
        unliked_at: null,
      })
      .into("bookmark_list");

    // let row = await this.knex
    // .select("id", "title", "file_url", "detail", "application_deadline")
    // .from("project")
    // .where({ id:project_id })
    // .first();

    // this.api.log(req, this.addBookmark, {

    //   table_name: "bookmark_list",
    //   project_title: row.title,
    //   api_user_email_address:email_address?email_address:null,
    //   liked_at:current_time,

    // });
    return result;
  }

  async updateIdeaBookmark(
    req: Request,
    project_id: number,
    user_id: number,
    email_address: string
  ) {
    let result = await this.knex("bookmark_list")
      .update({ unliked_at: this.knex.fn.now() })
      .where({ project_id, user_id, unliked_at: null });
    return result;
  }

  async updateBookmarkPage(req: Request, id: number, email_address: string) {
    let current_time = new Date(Date.now());
    let result = await this.knex("bookmark_list")
      .update({ unliked_at: current_time })
      .where({ id });
    // .andWhere({ user_id });
    return result;
  }
}
