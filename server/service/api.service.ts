import { Knex } from "knex";
import { APILogService } from "./api-log.service";
import { DBService } from "./db.service";

export class APIService extends DBService {
  constructor(knex: Knex, public api: APILogService) {
    super(knex);
  }
}
