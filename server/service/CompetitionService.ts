import { Knex } from "knex";
import { APIService } from "./api.service";
import { Request } from "express";

export class CompetitionService extends APIService {
    

    async getCompetitionCurrentList(req: Request,email_address:null|string){
        let row =  await this.knex
            .select(
                'id',
                'title ',
                'image_url',
                'short_desc',
                'start_date',
                'cancel_date',
                'url'
            )
            .from('competition')
            .where("start_date", ">", this.knex.fn.now())
            .orderBy("id","desc");
        this.api.log(req, this.getCompetitionCurrentList, {
        table_name: "competition",
        api_user_email_address:email_address?email_address:null,



    });
    return row;
  }

    async getCompetitionPastList(req: Request,email_address:null|string){
        let row =  await await this.knex
            .select(
                'id',
                'title ',
                'image_url',
                'short_desc',
                'start_date',
                'cancel_date',
                'url'
            )
            .from('competition')
            .where("start_date", "<", this.knex.fn.now())
            .orderBy("id","desc");;
        this.api.log(req, this.getCompetitionPastList, {
        table_name: "competition",
        api_user_email_address:email_address?email_address:null,
    

        });
        return row;
    }


    async getCompetitionDetail(req: Request,id:number,email_address:null|string){
        let row =await this.knex
            .select(
            'id',
            'title ',
            'image_url',
            'detail',
            'start_date as date',
            'cancel_date',
            'url'
            )
            .from('competition')
            .where({id})
            .first()
            this.api.log(req, this.getCompetitionDetail, {
                table_name: "competition",
                competition_title: row.title,
                api_user_email_address:email_address?email_address:null,
          
              });
              return row;
            }
}