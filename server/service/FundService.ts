import { Knex } from "knex";
import { APIService } from "./api.service";
import { Request } from "express";

export class FundService extends APIService {
  async getApplyInfo(user_id:number,fund_id:number){
    console.log("in service")
    let result=await this.knex.select('*').from('angel_fund_participant')
    .where('user_id',user_id)
    .andWhere('angel_fund_id',fund_id)
    .first()
    
    if(result){
      
      return true
    }
    return
  }
  async getFundDetail(req: Request, id: number,email_address:null|string) {

    let row = await this.knex
      .select("id", "title", "file_url", "detail", "application_deadline")
      .from("angel_fund")
      .where({ id })
      .first();

    let current = await this.knex.select('*')
    .from("angel_fund")
    .where('application_deadline', ">", this.knex.fn.now())
    .andWhere({ id })
    .first()

    row.isValid = !!current

    this.api.log(req, this.getFundDetail, {
      table_name: "angel_fund",
      angel_fund_title: row.title,
      api_user_email_address:email_address?email_address:null,


    });
    return row;
  }

  async getFundCurrentList(req: Request, email_address:null|string) {
    let current_time = new Date(Date.now())
    
    let row  = await this.knex
      .select("id", "title", "file_url", "short_desc")
      .from("angel_fund")
      .where("application_deadline", ">", current_time);

    this.api.log(req, this.getFundCurrentList, {
      table_name: "angel_fund",
      api_user_email_address:email_address?email_address:null,

    });

    return row;
  }

  async getFundPastList(req: Request, email_address:null|string) {
    let current_time = new Date(Date.now())
    
    let row  = await this.knex
      .select("id", "title", "file_url", "short_desc")
      .from("angel_fund")
      .where("application_deadline", "<", current_time);

    this.api.log(req, this.getFundPastList, {
      table_name: "angel_fund",
      api_user_email_address:email_address?email_address:null,
    });


    return row;
  }



  async addAngelFundParticipant(req: Request, uploads:{email_address:string,
    fund_id:number,
    detail:string, 
    file_url:string|undefined,
    user_id:number,}){

    let applied_at = new Date(Date.now())
    console.log("inserting into SQL")
    let angelFundParticipant = await this.knex
      .insert({user_id:uploads.user_id, angel_fund_id:uploads.fund_id, applied_at,detail:uploads.detail,file_url:uploads.file_url})
      .into('angel_fund_participant')
      .returning('id')
      console.log(angelFundParticipant)
      return angelFundParticipant;
  }

}
