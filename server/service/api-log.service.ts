import { DBService } from "./db.service";
import express from "express";
import "../utils/types";

let sampleLog = {
  api_user_email_address: "abc@gmail.com",
  api_method: "GET",
  api_url: "/idea/:id",
  api_name: "getFundCurrentList",
  table_name: "angel_fund",
  project_title: "",
  project_participant_email_address: "",
  approver_email_address: "",
  discussion: "",
  competition_title: "",
  angel_fund_title: "",
  angel_fund_participant_email_address: "",
  complaint_text: "",
  respondent_email_address: "",
  remark: "",
  approved_at: "2021-11-04 03:14:07",
  rejected_at: "2021-11-04 03:14:07",
  canceled_at: "2021-11-04 03:14:07",
  blocked_at: "2021-11-04 03:14:07",
  liked_at: "2021-11-04 03:14:07",
  unliked_at: "2021-11-04 03:14:07",
};
// type Log = typeof sampleLog;

export type Log = {
  api_user_email_address: null|string,
  api_method: string,
  api_url: string,
  api_name: string,
  table_name: string,
  project_title: null|string,
  project_participant_email_address: null|string,
  approver_email_address: null|string,
  discussion: null|string,
  competition_title: null|string,
  angel_fund_title: null|string,
  angel_fund_participant_email_address: null|string,
  complaint_text: null|string,
  respondent_email_address: null|string,
  remark: null|string,
  applied_at: null|Date,
  approved_at: null|Date,
  rejected_at: null|Date,
  canceled_at: null|Date,
  blocked_at: null|Date,
  liked_at: null|Date,
  unliked_at: null|Date,
}

export class APILogService extends DBService {
  log(
    req: express.Request,
    method: (...args: any[]) => any,
    log: Partial<Log> & {
      table_name: string;
    }
  ) {
    log.api_url = req.url; // e.g. /idea/:id
    log.api_method = req.method; // e.g. GET
    log.api_name = method.name; // e.g. getFundCurrentList
    log.api_user_email_address = req.jwtpayload?req.jwtpayload.emailAddress:null // JWT user
    return this.knex("api_log").insert(log);
  }
}
