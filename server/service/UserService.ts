import { Knex } from "knex";
import { EmailDetails, SendVerifyEmail } from "../utils/nodemailer";
import dotenv from "dotenv";
import jwtSimple from "jwt-simple";
import { json } from "express";
import { JWTpayload } from "../utils/models";
import {logger} from '../utils/loggers'


dotenv.config();

export class UserService {
  constructor(public knex: Knex) {}
  emailOTP_map = new Map<
    string,
    { userEmail: string; userName: string; timer: any }
  >();
  emailTrial_map = new Map<string, number>();
  emailSent_map = new Map<string, boolean>();
  isSentOTP: boolean;

  async CheckUserInfo(userEmail: string) {
    console.log("-Checking user email @CheckUserStatus in UserService...");
    let result = await this.knex
      .select("id", "name", "email_address", "is_admin")
      .from("user_info")
      .where("email_address", userEmail)
      .first();
    return result;
  }

  async GenerateOTP(digits: number, userEmail: string, userName: string) {
    let timer_OTPexpired = setTimeout(() => {
      this.emailOTP_map.delete(emailOTP);
    }, 1000 * 60 * 5);
    if (this.emailSent_map.get(userEmail)) {
      throw new Error("OTP has already sent.");
    }
    console.log("-Generating OTP /GenerateOTP @userService...");
    const verifyDigits =
      "1234567890";
    let emailOTP = "";
    let i = 0;
    do {
      for (let i = 0; i < digits; i++) {
        emailOTP += verifyDigits[Math.floor(Math.random() * 10)];
      }
      if (i > 1) {
        console.log("OTP duplicated, regenerating");
      }
      i++;
    } while (this.emailOTP_map.has(emailOTP));

    

    this.emailOTP_map.set(emailOTP, {
      userEmail,
      userName,
      timer: timer_OTPexpired,
    });
    const result = this.emailOTP_map.has(emailOTP);
    const map_detail = this.emailOTP_map.get(emailOTP);
    console.log(`map set result :${result}, ${map_detail?.userEmail}`);
    console.log(`-OTP generated => ${emailOTP}!`);
    return emailOTP.toString();
  }
  async CheckOTP(emailOTP: string) {
    const isValid = await this.emailOTP_map.has(emailOTP);
    if (isValid === false) {
      return
    }
    logger.log({level:"info", message:"OTP matched!"})
    return isValid;
  }



  async SendOTP(receiverEmail: string, emailMessage: string) {
    this.emailTrial_map.set(receiverEmail, 0);

    let emailDetails: EmailDetails = { receiverEmail, emailMessage };
    console.log("-Sending email to user /sendOTP @userService...");
    try {
      // console.log(this.emailSent_map.get(receiverEmail));
      if (this.emailSent_map.get(receiverEmail)) {
        throw new Error("OTP has already sent.");
      }
      console.log("get receiver email success!");
      await SendVerifyEmail(emailDetails);
      this.emailSent_map.set(receiverEmail, true);
      setTimeout(() => {
        this.emailSent_map.set(receiverEmail, false);
      }, 1000*10);
      console.log(`-Email has sent!`);
    } catch (e) {
      console.log((e as Error).toString());
      return (e as Error).toString()
    }
    return;
  }

  async CreateToken(emailOTP: string) {
    const userInfo = this.emailOTP_map.get(emailOTP);
    if (!userInfo) {
      return;
    }
      const result = await this.knex
      .select("email_address", "name", "id", "is_admin")
      .from("user_info")
      .where("email_address", userInfo.userEmail)
      .first();    
    if (!result) {
      console.log("login email not found! /createToken @userService");
      return;
    }
    let payload: JWTpayload = {
      id: result.id,
      userName: result.name,
      emailAddress: result.email_address,
      admin: result.is_admin,
    };
    if (!process.env.JWT_SECRET) {
      throw new Error("JWT_SECRET is missing");
    }
    let token = jwtSimple.encode(payload, process.env.JWT_SECRET);
    //delete OTP from OTP_map & delete timer
    const pair = this.emailOTP_map.get(emailOTP);
    if (!pair) {
      console.log("cannot find emailOTP in OTP map");
      return;
    }
    clearTimeout(pair.timer);
    this.emailOTP_map.delete(emailOTP);
    console.log(token);
    return token;
  }
  async CreateUser(emailOTP: string){
    const registerInfo = this.emailOTP_map.get(emailOTP);
    //return if no result in emailOTP map
    if (!registerInfo) {
      logger.log({level:"error", message:"cannot find emailOTP in OTP map"})
      return;
    }
    //insert user info into users_info table returning ID
    const userId= (await this.knex
      .insert({
        email_address: registerInfo.userEmail,
        name: registerInfo.userName,
        is_admin: false,
      })
      .into("user_info")
      .returning("id"))[0]
    return userId;
  }







  async GetUser(user_id: string) {
    console.log("getting user info from DB");
    try {
      const userInfos = await this.knex
      .select(
        "user_info.id",
        "user_info.email_address",
        "user_info.name",
        "user_info.is_admin",
        "user_info.title",
        "user_info.personal_image_url",
        "user_info.student_card_url",
        "user_info.self_introduction",
        "user_info.education_level",
        "user_info.year",
        "user_info.mode_of_study",
        "school.english_name AS school_name",
        "user_info.major",
        "user_info.faculty",
        "user_info.approved_at",
        "user_info.blocked_at",
        this.knex.raw('ARRAY_AGG(talent.skill) as skill')
      )
      .from("user_info")
      .leftJoin("school","user_info.school_id","school.id")
      .leftJoin('user_talent','user_info.id','user_talent.user_id')
      .leftJoin('talent','talent.id','user_talent.talent_id')
      .groupBy(
        "user_info.id",
        "user_info.email_address",
        "user_info.name",
        "user_info.is_admin",
        "user_info.title",
        "user_info.personal_image_url",
        "user_info.student_card_url",
        "user_info.self_introduction",
        "user_info.education_level",
        "user_info.year",
        "user_info.mode_of_study",
        "user_info.school_id",
        "user_info.major",
        "user_info.faculty",
        "user_info.approved_at",
        "user_info.blocked_at",
        "school.english_name",
      )
      .where("user_info.id", user_id)
      .first();
      console.log("userInfos :", userInfos);
    return { userInfos };
    } catch (error) {
      throw new Error("failed to get userInfos from DB @controller")
    }
    

    
  }

  async GetAllUserInfo() {
    try {
      const result = await this.knex
        .select([
          "user_info.id",
          "user_info.name",
          "user_info.email_address",
          "user_info.personal_image_url",
          "user_info.self_introduction",
          "user_info.created_at",
          this.knex.raw('ARRAY_AGG(talent.skill) as skill'),
        ])
        .from("user_info")
        .leftJoin("user_talent", "user_info.id", "user_talent.user_id")
        .leftJoin("talent", "talent.id", "user_talent.talent_id")
        .groupBy(
          "user_info.id",
          "user_info.email_address",
          "user_info.name",
          "user_info.personal_image_url",
          "user_info.self_introduction",
          "user_info.created_at"
        )
        .where("user_info.is_admin",false)
        .orderBy("user_info.created_at","desc");

        //to do order-by
        //where create at a>z

      console.log(result);
      return result;
    } catch (error) {
      throw new Error("cannot get CVs");
    }
  }

  //==========================================DEV_for testing============================================
  async Insert_into_userInfo(userEmail: string) {
    console.log(userEmail);
    const result = (
      await this.knex
        .insert({
          email_address: userEmail,
          name: "testing",
          is_admin: true,
        })
        .into("user_info")
        .returning("id")
    )[0];
    return result;
  }

  async Select_from_userInfo(emailOTP: string) {
    console.log(emailOTP);
    const result = await this.knex
      .select("email_address", "name", "id", "is_admin")
      .from("user_info")
      .where("email_address", emailOTP)
      .first();
    return result;
  }
}
