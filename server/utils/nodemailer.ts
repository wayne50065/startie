import { createTransport } from "nodemailer";
import dotenv from "dotenv";
dotenv.config();

export interface EmailDetails {
  //receiver email to be selected from DB,
  //separated by comma if more than 1.
  receiverEmail: string;
  emailMessage: string;
}

let transporter = createTransport({
  host: process.env.MAILER_HOST,
  port: 587,
  secure: false, // true for 465, false for other ports
  auth: {
    user: process.env.MAILER_USERNAME, // generated ethereal user
    pass: process.env.MAILER_PASSWORD, // generated ethereal password
  },
  tls: {
    ciphers:'SSLv3'
}
});

export const SendVerifyEmail = async (props: EmailDetails) => {
  const emailRegex =
    /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;

  if (props.receiverEmail.search(emailRegex) == -1) {
    throw new Error("receiver email is not valid");
  }

  let emailInfo = {
    from: `"Startie" <${process.env.MAILER_ADDRESS}>`, // sender address
    to: props.receiverEmail, // list of receivers
    subject: "Startie - One Time Password ", // Subject line
    html: props.emailMessage, // html body
  };

  let emailResult = await transporter.sendMail(emailInfo);
  console.log("sent email result:", emailResult);
};
