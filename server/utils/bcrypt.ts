import { hash, compare } from "bcrypt";

export async function hashPassword(password: string): Promise<string> {
  let rounds = 10;
  return hash(password, rounds);
}

export async function ComparePassword(
  password: string,
  hashPassword: string
): Promise<boolean> {
  return compare(password, hashPassword);
}
