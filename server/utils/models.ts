export interface JWTpayload {
  emailAddress: string;
  userName: string;
  id: number;
  admin: boolean;
}

export interface User {
  id: number;
  email_address: string;
  name: string;
  title: string;
  personal_image_url: string;
  student_card_url: string;
  self_introduction: string;
  education_level: string;
  year: string;
  mode_of_study: string;
  school_id: string;
  major: string;
  faculty: string;
  approved_at: string;
  blocked_at: string;
}
declare global {
  namespace Express {
    interface Request {
      jwtpayload: JWTpayload;
    }
  }
}
