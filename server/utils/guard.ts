import { Request, Response, NextFunction } from "express";
import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import jwt from "./jwt";
import { JWTpayload} from "./models";

const permit = new Bearer({ query: "access_token" });

export  function isLoggedIn(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (!jwt.jwtSecret) {
    res.status(500).json({ error: "Token is missing in req" });
    return;
  }
  const token = permit.check(req);
  if (!token) {
    res.status(401).json({ error: "Token is missing in req" });
    return;
  }
  let payload: JWTpayload;
  try {
    payload = jwtSimple.decode(token, jwt.jwtSecret);
  } catch (e) {
    res.status(401).json({ error: "token decode failed in req" });
    return;
  }
  req.jwtpayload = payload;
  next();
}
