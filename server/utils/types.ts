export type NewProject = {
  competition_id_list: number[];
  new_competition_list: Array<{
    name: string;
    start_date?: string;
    url?: string;
    year?: number;
  }>
  user_id?:number;
  title: string;
  category: string;
  detail: string;
  project_deadline: Date;
  new_talent_needed_list: string[];
  talent_needed_id_list: number[];
  criteria: string;
  images: string[];
  youtube_url:string;
  chat_room_url: String;
  competition_name: string;
  competition_url: string;
  competition_date: Date;
};
