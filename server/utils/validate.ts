import express from "express";

export function get(
  req: express.Request,
  field: keyof express.Request & string,
  key: string
) {
  let object = req[field];

  if (key in object) {
    return object[key];
  }

  throw new Error(`missing ${key} in req.${field}`);
}

export function getBody(req: express.Request, key: string) {
  return get(req, "body", key);
}
