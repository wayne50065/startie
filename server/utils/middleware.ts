import { Request, Response, NextFunction } from "express";
import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import jwt from "./jwt";

const permit = new Bearer({ query: "access_token" });

export async function jwtMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const token = permit.check(req);
    let payload = jwtSimple.decode(token, jwt.jwtSecret!);
    req.jwtpayload = payload;
  } catch (error) {}
  next();
}

export async function todoMiddleware(
  req: Request,
  res: Response,
) {
  res
    .status(501)
    .json({ error: `TODO, method: ${req.method}, url: ${req.url}` });
}
