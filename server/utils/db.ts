import Knex from 'knex'
import dotenv from 'dotenv'
dotenv.config()

let configs = require('../knexfile')

let mode = process.env.NODE_ENV
if (!mode) {
  throw new Error('Missing NODE_ENV in process.env')
}
let config = configs[mode]

export let knex = Knex(config)