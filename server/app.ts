import express from "express";
import dotenv from "dotenv";
import cors from "cors";
import { knex } from "./utils/db";
import { UserController } from "./controller/UserController";
import { UserService } from "./service/UserService";
import { ProjectService } from "./service/ProjectService";
import { ProjectController } from "./controller/ProjectController";
import { FundController } from "./controller/FundController";
import { FundService } from "./service/FundService";
import multer from "multer";
import { storage, storageProtected } from "./utils/storage";
import { print } from "listening-on";
import { jwtMiddleware } from "./utils/middleware";
import { APILogService } from "./service/api-log.service";
import { CompetitionController } from "./controller/CompetitionController";
import { CompetitionService } from "./service/CompetitionService";
import { AutoCompleteController } from "./controller/AutoCompleteController";
import { AutoCompleteService } from "./service/AutoCompleteService";
dotenv.config();

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(jwtMiddleware)


app.use(express.static("public"));


export let apiLogService = new APILogService(knex);
export const upload = multer({ storage: storage });
export const uploadProtected = multer({ storage: storageProtected });

export let userService = new UserService(knex);
export let userController = new UserController(userService);
app.use(userController.toRouter());

export let projectService = new ProjectService(knex,apiLogService);
export let projectController = new ProjectController(projectService);
app.use(projectController.toRouter());

export let competitionService = new CompetitionService(knex,apiLogService);
export let competitionController = new CompetitionController(competitionService);
app.use(competitionController.toRouter());


export let fundService = new FundService(knex,apiLogService);
export let fundController = new FundController(fundService);
app.use(fundController.toRouter());

let autoCompleteService = new AutoCompleteService(knex)
let autoCompleteController = new AutoCompleteController(autoCompleteService)
app.use(autoCompleteController.router)



let PORT = process.env.PORT;
if (!PORT) {
  throw new Error();
}
let port = +PORT;
app.listen(port, () => {
  print(port);
});
