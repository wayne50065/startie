from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os

def read_dataframes_dim_angel_fund(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(SELECT * from angel_fund) angel_fund")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()


def insert_dim_angel_fund(row):
 

    insert_dim_angel_fund_sql = "insert into dim_angel_fund (title,short_desc,detail,file_url,application_deadline,canceled_at,blocked_at,created_at,updated_at) values(%(title)s,%(short_desc)s,%(detail)s,%(file_url)s,%(application_deadline)s,%(canceled_at)s,%(blocked_at)s,%(imported_at)s,%(imported_at)s) on conflict(title) do update SET short_desc = %(short_desc)s, detail = %(detail)s, application_deadline = %(application_deadline)s, canceled_at = %(canceled_at)s, blocked_at = %(blocked_at)s, updated_at= %(imported_at)s  "

    cur.execute(insert_dim_angel_fund_sql,row)
    return (True)



def main_dim_angel_fund(spark):
    df_dim_angel_fund = read_dataframes_dim_angel_fund(spark)
    df_dim_angel_fund.show()
    print("before transform - dim angel_fund")
    df_dim_angel_fund = df_dim_angel_fund.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_dim_angel_fund = df_dim_angel_fund.withColumn("imported_at", current_timestamp())
    # df_dim_school = df_dim_school.withColumn("created_at", date_trunc("second", col("created_at")))
    df_dim_angel_fund.show()
    print("RDD count: ",df_dim_angel_fund.rdd.count())
    print("after transform - dim angel_fund")
    df_dim_angel_fund.rdd.foreach(insert_dim_angel_fund)
