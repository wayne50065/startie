from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os


def read_dataframes_sub_dim_project_talent(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(SELECT project_talent.*,talent.skill, project.title from project_talent left join talent on project_talent.talent_id = talent.id left join project on project.id = project_talent.project_id) project_talent")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()


def insert_sub_dim_project_talent(row):

    insert_dim_talent_sql = "insert into sub_dim_project_talent (project_id,skill,removed_at,created_at, updated_at) values((select id from dim_project where title = %(title)s limit 1),%(skill)s,%(removed_at)s,%(imported_at)s,%(imported_at)s) on conflict(project_id,skill) do update SET removed_at = %(removed_at)s, updated_at = %(imported_at)s"
    cur.execute(insert_dim_talent_sql,row)
    return (True)


def main_sub_dim_project_talent(spark):
    df_sub_dim_project_talent= read_dataframes_sub_dim_project_talent(spark)
    df_sub_dim_project_talent.show()
    print("before transform - dim_project_talent")
    df_sub_dim_project_talent = df_sub_dim_project_talent.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_sub_dim_project_talent = df_sub_dim_project_talent.withColumn("imported_at", current_timestamp())
    df_sub_dim_project_talent.show()
    print("after transform - dim_project_talent")
    df_sub_dim_project_talent.rdd.foreach(insert_sub_dim_project_talent)



