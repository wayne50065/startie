from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os



def read_dataframes_fact_angel_fund_participant(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(SELECT a.email_address, angel_fund.title,angel_fund_participant.detail, angel_fund_participant.applied_at,angel_fund_participant.quitted_at, angel_fund_participant.approved_at, angel_fund_participant.rejected_at ,angel_fund_participant.created_at from angel_fund_participant inner join user_info a on a.id = angel_fund_participant.id inner join angel_fund on angel_fund.id = angel_fund_participant.angel_fund_id ) angel_fund_participant")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()

def insert_fact_angel_fund_participant(row):

    insert_fact_angel_fund_participant_sql = "insert into fact_angel_fund_participant (user_id,angel_fund_id,detail,applied_at,quitted_at,approved_at,rejected_at,date_id,created_at,updated_at) values((select id from dim_user_info where email_address = %(email_address)s limit 1),(select id from dim_angel_fund where title = %(title)s limit 1),%(detail)s,%(applied_at)s,%(quitted_at)s,%(approved_at)s,%(rejected_at)s,(select id from dim_date where date = %(created_at)s limit 1),%(imported_at)s,%(imported_at)s)"
    cur.execute(insert_fact_angel_fund_participant_sql,row)
    return (True)



def main_fact_angel_fund_participant(spark):
    df_fact_angel_fund_participant = read_dataframes_fact_angel_fund_participant(spark)
    df_fact_angel_fund_participant.show()
    print("before transform - fact_angel_fund_participant")
    df_fact_angel_fund_participant = df_fact_angel_fund_participant.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_fact_angel_fund_participant = df_fact_angel_fund_participant.withColumn("imported_at", current_timestamp())
    df_fact_angel_fund_participant.show()
    print("RDD count: ",df_fact_angel_fund_participant.rdd.count())
    print("after transform - fact_angel_fund_participant")
    df_fact_angel_fund_participant.rdd.foreach(insert_fact_angel_fund_participant)