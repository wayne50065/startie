from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os




def read_dataframes_dim_project_category(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(SELECT * from project_category) project_category")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()


def insert_dim_project_category(row):


    insert_dim_project_category_sql = "insert into dim_project_category (category_name,created_at,updated_at) values(%(category_name)s,%(imported_at)s,%(imported_at)s) on conflict(category_name) do nothing"
    cur.execute(insert_dim_project_category_sql,row)
    return (True)


def main_dim_project_category(spark):
    df_dim_project_category = read_dataframes_dim_project_category(spark)
    df_dim_project_category.show()
    print("before transform - dim_project_category")
    df_dim_project_category = df_dim_project_category.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_dim_project_category = df_dim_project_category.withColumn("imported_at", current_timestamp())
    df_dim_project_category.show()
    print("RDD count: ",df_dim_project_category.rdd.count())
    print("after transform - dim_project_category")
    df_dim_project_category.rdd.foreach(insert_dim_project_category)