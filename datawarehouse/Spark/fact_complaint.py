from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os


def read_dataframes_fact_complaint(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(select a.email_address, project.title,b.email_address as repsondent_email_address,complaint.detail, complaint.discussion_detail, complaint.discussion_created_at, complaint.reviewed_at, complaint.created_at from complaint inner join user_info as a on a.id = complaint.user_id inner join project on project.id = complaint.project_id inner join user_info as b on b.id = complaint.respondent_id) complaint")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()

def insert_fact_complaint(row):


    insert_fact_complaint_sql = "insert into fact_complaint (project_id,user_id, respondent_id,discussion_detail,discussion_created_at,detail,reviewed_at,date_id,created_at) values((select id from dim_project where title = %(title)s limit 1),(select id from dim_user_info where email_address = %(email_address)s limit 1),(select id from dim_user_info where email_address = %(repsondent_email_address)s limit 1),%(discussion_detail)s,%(discussion_created_at)s,%(detail)s,%(reviewed_at)s,(select id from dim_date where date = %(created_at)s limit 1),%(imported_at)s)"
    cur.execute(insert_fact_complaint_sql,row)
    return (True)

def main_fact_complaint(spark):
    df_fact_complaint = read_dataframes_fact_complaint(spark)
    df_fact_complaint.show()
    print("before transform - fact_complaint")
    df_fact_complaint = df_fact_complaint.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_fact_complaint = df_fact_complaint.withColumn("imported_at", current_timestamp())
    print("RDD count: ",df_fact_complaint.rdd.count())
    df_fact_complaint.show()
    print("after transform - fact_complaint")
    df_fact_complaint.rdd.foreach(insert_fact_complaint)
