from dotenv import load_dotenv
import os 
load_dotenv()
DW_NAME=os.getenv('DW_NAME')
DW_USERNAME=os.getenv('DW_USERNAME')
DW_PASSWORD=os.getenv('DW_PASSWORD')
DW_HOST = os.getenv('DW_HOST') or 'localhost'# 如果無DB_HOST，就load localhost

import psycopg2 as psycopg
conn = psycopg.connect(dbname=DW_NAME,user=DW_USERNAME,password=DW_PASSWORD,host=DW_HOST)
conn.autocommit = True

start_year = 2020
end_year =2040

from datetime import datetime
from datetime import timedelta
import math

print(datetime.now() + timedelta(minutes=1))

# def number_of_date_eachyear():
#     if (new_year.year%4) !=0:
#         i=365 
#     else:
#         i=366

def main():
    sql = "select count(*) as count from dim_date"
    cur = conn.cursor()
    cur.execute(sql)
    row = cur.fetchone()
    count = row[0]
    if count > 0:
        print("skip")
        return
    for year in range(start_year,end_year):
        year_format = datetime(year,1,1,0,0,0,0)
        if (year_format.year%4) !=0:
            i=365 
        else:
            i=366
        for num in range(i):
            for hour in range(24):                
                date = year_format + timedelta(days=num,hours=hour)
                data_row = {
                    "date": date.strftime('%Y-%m-%d %H:%M'),
                    "year":date.strftime('%Y'),
                    "month": int(date.strftime('%m')),
                    "month_full_name": date.strftime('%B'),
                    "month_short_name": date.strftime('%b'),
                    "day": date.strftime('%d'),
                    "hour": date.strftime('%H'),
                    "minute": date.strftime('%M'),
                    "week_number": date.strftime('%w'),
                    "week_full_name": date.strftime('%A'),
                    "week_short_name": date.strftime('%a'),
                    "quarter_number": math.ceil(date.month / 3),
                    "quarter_name": "Q"+str(math.ceil(date.month / 3))
                }
                key_list = []
                value_list = []
                for key in data_row:
                    # columns.append('{'+key+'}')
                    key_list.append(key)
                    value_list.append("'"+str(data_row[key])+"'")
                # columns = ['{'+key+'}' for key in data_row]
                # line = ','.join(columns) + '\n'
                # f.write(line.format(**data_row))

                sql = "insert into dim_date (" + ','.join(key_list) + ') values (' + ','.join(value_list) + ');' 
                cur.execute(sql)




main()






