from dotenv import load_dotenv
import os



def prepare_env():
    global spark
    from pyspark.sql import SparkSession
    import findspark
    findspark.init()
    # findspark.init('/home/ubuntu/.local/bin/pyspark')
    # findspark.init('/home/ubuntu/local/lib/python3.8/site-packages/pyspark/')
    # findspark.init('C:\Program Files\spark')
    # 你呢個位，需要啲咩drivers ，取決於你要讀寫啲咩system
    packages = [
        'com.amazonaws:aws-java-sdk-s3:1.12.79', # 寫上S3
        'org.apache.hadoop:hadoop-aws:3.2.0', # 寫上S3
        'org.apache.spark:spark-avro_2.12:2.4.4', # 要寫Avro format
        'org.postgresql:postgresql:42.2.24' # 要由postgresql application database 讀嘢
    ]

    # Spark 有大量drivers，可供使用
    spark = SparkSession.builder.appName('Read from postgresql')\
        .config('spark.jars.packages',','.join(packages)).getOrCreate()
    
    return spark


from dim_school import main_dim_school
from dim_angel_fund import main_dim_angel_fund
from dim_user_info import main_dim_user_info
from fact_user_info import main_fact_user_info
from sub_dim_user_talent import main_sub_dim_user_talent
from fact_angel_fund_participant import main_fact_angel_fund_participant
from dim_project_category import main_dim_project_category
from dim_project import main_dim_project
from fact_project import main_fact_project
from sub_dim_project_talent import main_sub_dim_project_talent
from sub_dim_project_image import main_sub_dim_project_image
from fact_project_discussion import main_fact_project_discussion
from fact_bookmark_list import main_fact_bookmark_list
from fact_participant_project import main_fact_participant_project
from fact_competition import main_fact_competition
from fact_complaint import main_fact_complaint
from fact_api_log import main_fact_api_log


#--------------------------------------------------------------------------------------------------------------
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
def main():
    prepare_env()
    main_dim_school(spark)
    main_dim_angel_fund(spark)
    main_dim_user_info(spark)
    main_fact_user_info(spark)
    main_sub_dim_user_talent(spark)
    main_fact_angel_fund_participant(spark)
    main_dim_project_category(spark)
    main_dim_project(spark)
    main_fact_project(spark)
    main_sub_dim_project_talent(spark)
    main_sub_dim_project_image(spark)
    main_fact_bookmark_list(spark)
    main_fact_project_discussion(spark)
    main_fact_participant_project(spark)
    main_fact_competition(spark)    
    main_fact_complaint(spark)
    main_fact_api_log(spark)

 # log table < airflow   
 # prefect
 # 














#--------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    main()