from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os


def read_dataframes_fact_user_info(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(SELECT user_info.*, school.code from user_info left join school on user_info.school_id = school.id) user_info")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()

def insert_fact_user_info(row):

    insert_fact_user_info_sql = "insert into fact_user_info (email_address,name,title,self_introduction,education_level,school_id,year,mode_of_study,major,faculty,is_admin,approved_at,blocked_at,date_id,created_at) values(%(email_address)s,%(name)s,%(title)s,%(self_introduction)s,%(education_level)s,(select id from dim_school where code = %(code)s limit 1),%(year)s,%(mode_of_study)s,%(major)s,%(faculty)s,%(is_admin)s,%(approved_at)s,%(blocked_at)s,(select id from dim_date where date = %(created_at)s limit 1),%(imported_at)s)"

    cur.execute(insert_fact_user_info_sql,row)
    return (True)



def main_fact_user_info(spark):
    df_fact_user_info = read_dataframes_fact_user_info(spark)
    df_fact_user_info.show()
    print("before transform - fact_user_info")
    df_fact_user_info = df_fact_user_info.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_fact_user_info = df_fact_user_info.withColumn("imported_at", current_timestamp())
    print("RDD count: ",df_fact_user_info.rdd.count())
    df_fact_user_info.show()
    print("after transform - dim_user_info")
    df_fact_user_info.rdd.foreach(insert_fact_user_info)