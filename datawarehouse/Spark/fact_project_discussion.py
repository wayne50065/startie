from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os




def read_dataframes_fact_project_discussion(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(select project_discussion.*, user_info.email_address, project.title from project_discussion left join user_info on user_info.id = project_discussion.user_id left join project on project.id = project_discussion.project_id) project_discussion")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()


def insert_fact_project_discussion(row):
    import psycopg2 as psycopg
    

    insert_dim_talent_sql = "insert into fact_project_discussion (user_id,project_id,discussion,date_id,created_at) values((select id from dim_user_info where email_address = %(email_address)s limit 1),(select id from dim_project where title = %(title)s limit 1),%(discussion)s,(select id from dim_date where date = %(created_at)s limit 1),%(imported_at)s)"

    cur.execute(insert_dim_talent_sql,row)
    return (True)



def main_fact_project_discussion(spark):

    df_fact_project_discussion = read_dataframes_fact_project_discussion(spark)
    df_fact_project_discussion.show()
    print("before transform - fact_project_discussion")
    df_fact_project_discussion = df_fact_project_discussion.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_fact_project_discussion = df_fact_project_discussion.withColumn("imported_at", current_timestamp())
    print("RDD count: ",df_fact_project_discussion.rdd.count())
    df_fact_project_discussion.show()
    print("after transform - fact_project_discussion")
    df_fact_project_discussion.rdd.foreach(insert_fact_project_discussion)
