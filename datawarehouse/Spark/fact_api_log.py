from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os



def read_dataframes_fact_api_log(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(select  project.title,a.email_address,b.email_address as approver_email_address, project_participant.approved_at, project_participant.rejected_at, project_participant.detail,  project_participant.created_at from project_participant left join user_info as a on a.id = project_participant.user_id left join project on project.id = project_participant.project_id left join user_info as b on b.id = project_participant.approver_id) participant_project")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()


def insert_fact_api_log(row):


    # insert_user_sql = "insert into dim_school (school_type,domain,code,english_name,chinese_name,email_domains,created_at) values(%(school_type)s,%(domain)s,%(code)s,%(english_name)s,%(chinese_name)s,%(email_domains)s,%(created_at)s)"

    insert_fact_competition_sql = "insert into fact_participant_project (project_id,user_id, approver_id,aprrove_at,detail,reject_at,date_id,created_at) values((select id from dim_project where title = %(title)s limit 1),(select id from dim_user_info where email_address = %(email_address)s limit 1),(select id from dim_user_info where email_address = %(approver_email_address)s limit 1),%(approved_at)s,%(detail)s,%(rejected_at)s,(select id from dim_date where date = %(created_at)s limit 1),%(imported_at)s)"
    cur.execute(insert_fact_competition_sql,row)
    return (True)






def main_fact_api_log(spark):
    df_fact_api_log = read_dataframes_fact_api_log(spark)
    df_fact_api_log.show()
    print("before transform - fact_api_log")
    df_fact_api_log = df_fact_api_log.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_fact_api_log = df_fact_api_log.withColumn("imported_at", current_timestamp())
    print("RDD count: ",df_fact_api_log.rdd.count())
    df_fact_api_log.show()
    print("RDD count: ",df_fact_api_log.rdd.count())
    print("after transform - fact_api_log")
    df_fact_api_log.rdd.foreach(insert_fact_api_log)