from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os

def read_dataframes_dim_school(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(SELECT * from school) school")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()




def insert_dim_school(row):

    insert_dim_school_sql = "insert into dim_school (school_type,domain,code,english_name,chinese_name,email_domains,school_icon,created_at,updated_at) values(%(school_type)s,%(domain)s,%(code)s,%(english_name)s,%(chinese_name)s,%(email_domains)s,%(school_icon)s,%(imported_at)s,%(imported_at)s) on conflict(domain) do update SET school_type = %(school_type)s, code = %(code)s, english_name = %(english_name)s, chinese_name = %(chinese_name)s, email_domains = %(email_domains)s, school_icon= %(school_icon)s, updated_at= %(imported_at)s"

    cur.execute(insert_dim_school_sql,row)
    return (True)


def main_dim_school(spark):
    df_dim_school = read_dataframes_dim_school(spark)
    df_dim_school.show()
    print("before transform - dim school")
    df_dim_school = df_dim_school.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_dim_school = df_dim_school.withColumn("imported_at", current_timestamp())
    df_dim_school.show()
    print("RDD count: ",df_dim_school.rdd.count())
    print("after transform - dim school")
    df_dim_school.rdd.foreach(insert_dim_school)


