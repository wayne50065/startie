from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os


def read_dataframes_fact_bookmark_list(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(select user_info.email_address, project.title, bookmark_list.liked_at, bookmark_list.unliked_at, bookmark_list.created_at from bookmark_list left join user_info on user_info.id = bookmark_list.user_id inner join project on project.id=bookmark_list.project_id) bookmark_list")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()


def insert_fact_bookmark_list(row):

    insert_fact_bookmark_list_sql = "insert into fact_bookmark_list (owner_user_id,project_id, liked_at,unliked_at ,date_id, created_at, updated_at) values((select id from dim_user_info where email_address = %(email_address)s limit 1),(select id from dim_project where title = %(title)s limit 1),%(liked_at)s,%(unliked_at)s,(select id from dim_date where date = %(liked_at)s limit 1),%(imported_at)s,%(imported_at)s)"
    cur.execute(insert_fact_bookmark_list_sql,row)
    return (True)


def main_fact_bookmark_list(spark):
    df_sub_fact_bookmark_list = read_dataframes_fact_bookmark_list(spark)
    df_sub_fact_bookmark_list.show()
    print("before transform - fact_bookmark_list")
    df_sub_fact_bookmark_list = df_sub_fact_bookmark_list.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_sub_fact_bookmark_list = df_sub_fact_bookmark_list.withColumn("liked_at", date_trunc("hour", col("liked_at")))
    df_sub_fact_bookmark_list = df_sub_fact_bookmark_list.withColumn("imported_at", current_timestamp())
    print("RDD count: ",df_sub_fact_bookmark_list.rdd.count())
    df_sub_fact_bookmark_list.show()
    print("after transform - sub_fact_bookmark_list")
    df_sub_fact_bookmark_list.rdd.foreach(insert_fact_bookmark_list)
