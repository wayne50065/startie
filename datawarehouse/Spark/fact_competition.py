from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os

def read_dataframes_fact_competition(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(select project.title as project_title, project_competition.project_id, competition.* from project_competition left join competition on project_competition.competition_id = competition.id left join project on project_competition.project_id =  project.id) project_competition")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()


def insert_fact_competition(row):

    insert_fact_competition_sql = "insert into fact_competition (project_id, title,url,start_date,cancel_date,date_id,created_at) values((select id from dim_project where title = %(project_title)s limit 1),%(title)s,%(url)s,%(start_date)s,%(cancel_date)s,(select id from dim_date where date = %(created_at)s limit 1),%(imported_at)s)"
    cur.execute(insert_fact_competition_sql,row)
    return (True)


def main_fact_competition(spark):
    df_fact_competition = read_dataframes_fact_competition(spark)
    df_fact_competition.show()
    print("before transform - fact_competition")
    df_fact_competition = df_fact_competition.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_fact_competition = df_fact_competition.withColumn("imported_at", current_timestamp())
    print("RDD count: ",df_fact_competition.rdd.count())
    df_fact_competition.show()
    print("after transform - fact_competition")
    df_fact_competition.rdd.foreach(insert_fact_competition)
