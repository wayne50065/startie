#/bin/bash
set -x

while [ true ]; do

    set -e
    # echo python date_creater_into_psql.py
    python date_creater_into_psql.py

    set +e
     python app_ETL_warehouse.py
    # echo python app_ETL_warehouse.py

    echo sleep

    # sleep 3600 # hourly
    sleep 86400 # daily


done