
from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os






def read_dataframes_dim_user_info(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(SELECT user_info.*, school.code from user_info left join school on user_info.school_id = school.id) user_info")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()

def insert_dim_user_info(row):

    insert_dim_user_info_sql = "insert into dim_user_info (email_address,name,title,self_introduction,education_level,school_id,year,mode_of_study,major,faculty,is_admin,approved_at,blocked_at,created_at,updated_at) values(%(email_address)s,%(name)s,%(title)s,%(self_introduction)s,%(education_level)s,(select id from dim_school where code = %(code)s limit 1),%(year)s,%(mode_of_study)s,%(major)s,%(faculty)s,%(is_admin)s,%(approved_at)s,%(blocked_at)s,%(imported_at)s,%(imported_at)s) on conflict(email_address) do update SET name = %(name)s, title = %(title)s, self_introduction = %(self_introduction)s, education_level = %(education_level)s, year = %(year)s, updated_at = %(imported_at)s"

    cur.execute(insert_dim_user_info_sql,row)
    return (True)


def main_dim_user_info(spark):
    df_dim_user_info = read_dataframes_dim_user_info(spark)
    df_dim_user_info.show()
    print("before transform - dim_user_info")
    df_dim_user_info = df_dim_user_info.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_dim_user_info = df_dim_user_info.withColumn("imported_at", current_timestamp())
    df_dim_user_info.show()
    print("RDD count: ",df_dim_user_info.rdd.count())
    print("after transform - dim_user_info")
    df_dim_user_info.rdd.foreach(insert_dim_user_info)