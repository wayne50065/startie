import psycopg2 as psycopg
from dotenv import load_dotenv
import os

load_dotenv()

DW_NAME = os.getenv('DW_NAME')

DW_USERNAME = os.getenv('DW_USERNAME')

DW_PASSWORD = os.getenv('DW_PASSWORD')

DW_HOST = os.getenv('DW_HOST') or 'localhost'  # 如果無DB_HOST，就load localhost


conn = psycopg.connect(dbname=DW_NAME, user=DW_USERNAME,
                       password=DW_PASSWORD, host=DW_HOST)
conn.autocommit = True
cur = conn.cursor()
