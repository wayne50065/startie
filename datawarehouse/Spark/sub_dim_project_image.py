from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os






def read_dataframes_sub_dim_project_image(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(SELECT project_image.*,project.title from project_image left join project on project.id=project_image.project_id) project_image")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()


def insert_sub_dim_project_image(row):
   
    insert_project_image_sql = "insert into sub_dim_project_image (project_id,project_image_url,created_at,updated_at) values((select id from dim_project where title = %(title)s limit 1),%(project_image_url)s,%(imported_at)s,%(imported_at)s)"
    cur.execute(insert_project_image_sql,row)
    return (True)




def main_sub_dim_project_image(spark):
    df_sub_dim_project_image = read_dataframes_sub_dim_project_image(spark)
    df_sub_dim_project_image.show()
    print("before transform - dim_project")
    df_sub_dim_project_image = df_sub_dim_project_image.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_sub_dim_project_image = df_sub_dim_project_image.withColumn("imported_at", current_timestamp())
    df_sub_dim_project_image.show()
    print("RDD count: ",df_sub_dim_project_image.rdd.count())
    print("after transform - dim_project")
    df_sub_dim_project_image.rdd.foreach(insert_sub_dim_project_image)