from loadenv import cur
from pyspark.sql.functions import col,when,substring,date_trunc,current_timestamp
import os


def read_dataframes_fact_project(spark):
    return spark.read.format('jdbc')\
         .option('url','jdbc:postgresql://localhost:5432/'+os.getenv('DB_NAME'))\
         .option('dbtable',"(select project.title, project.detail, user_info.email_address, project_category.category_name, project.chat_room_url,youtube_url,criteria, project.application_deadline, project.project_deadline, project.canceled_at, project.blocked_at, project.created_at from project left join user_info on user_info.id=project.user_id left join project_category on project_category.id=project.category_id) fact_project")\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('user',os.getenv('DB_USERNAME'))\
         .option('password',os.getenv('DB_PASSWORD'))\
         .option('driver','org.postgresql.Driver').load()


def insert_fact_project(row):

    insert_dim_project_sql = "insert into fact_project (title,detail, owner_user_id,category_id,chat_room_url,youtube_url,criteria,application_deadline,project_deadline,canceled_at,blocked_at,date_id,created_at,updated_at) values(%(title)s,%(detail)s,(select id from dim_user_info where email_address = %(email_address)s limit 1),(select id from dim_project_category where category_name = %(category_name)s limit 1),%(chat_room_url)s,%(youtube_url)s,%(criteria)s,%(application_deadline)s,%(project_deadline)s,%(canceled_at)s,%(blocked_at)s,(select id from dim_date where date = %(created_at)s limit 1),%(imported_at)s,%(imported_at)s)"
    cur.execute(insert_dim_project_sql,row)
    return (True)

def main_fact_project(spark):
    df_dim_project = read_dataframes_fact_project(spark)
    df_dim_project.show()
    print("before transform - dim_project")
    df_dim_project = df_dim_project.withColumn("created_at", date_trunc("hour", col("created_at")))
    df_dim_project = df_dim_project.withColumn("imported_at", current_timestamp())
    print("RDD count: ",df_dim_project.rdd.count())
    df_dim_project.show()
    print("after transform - fact_project")
    df_dim_project.rdd.foreach(insert_fact_project)
