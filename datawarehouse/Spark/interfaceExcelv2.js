import pkg from "pg"
const { Client } = pkg
import dotenv from "dotenv"
import Papa from "papaparse"
import fs from "fs"

dotenv.config()

export const client = new Client({
    database: process.env.DW_NAME,
    user: process.env.DW_USERNAME,
    password: process.env.DW_PASSWORD,
})
client.connect()

const readCSV = async (filePath) => {
    const csvFile = fs.readFileSync(filePath)
    const csvData = csvFile.toString()
    return new Promise((resolve) => {
        Papa.parse(csvData, {
            header: true,
            complete: (results) => {
                console.log("Complete", results.data.length, "records.")
                resolve(results.data)
            },
        })
    })
}

const test = async () => {
    let parsedData = await readCSV("./time_dimension.csv")
    console.log(parsedData)

    for (let eachuser_info of parsedData) {
        console.log(eachuser_info)
        //console.log(eachuser_info.emailaddress);

        await client.query(


            `insert into dim_date (date,year,quarter_number,quarter_name,month,month_full_name,month_short_name,week_number,week_full_name,week_short_name,day,hour,minute) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)`
    
            [



                eachuser_info.date,
                eachuser_info.year,
                eachuser_info.month,
                eachuser_info.month_full_name,
                eachuser_info.month_short_name,
                eachuser_info.day,
                eachuser_info.hour,
                eachuser_info.minute,
                eachuser_info.week_number,
                eachuser_info.week_full_name,
                eachuser_info.week_short_name,
                eachuser_info.quarter_number,
                eachuser_info.quarter_name
    
            ]
        )
    }
}

test()
