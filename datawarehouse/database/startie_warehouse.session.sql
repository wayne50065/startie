insert into dim_school (school_type,created_at) values('abc','2021-10-22 18:28:00.000000')
select * from dim_school;

----

select * from fact_api_log;
select * from fact_complaint;
select * from fact_participant_project;  --done
select * from fact_competition;  -- done
select * from fact_project_discussion;  -- done
select * from fact_bookmark_list; -- done
select * from sub_dim_project_talent; -- done 
select * from sub_dim_project_image;  -- done
select * from dim_project;    --done
select * from fact_project
select * from dim_project_category;    -- done
select * from fact_angel_fund_participant;     --done
select * from dim_angel_fund_approver_info;   --done
select * from dim_angel_fund;  ---done
select * from sub_dim_user_talent; -- done
select * from dim_user_info;  ---done
select * from fact_user_info;
select * from dim_school;   ---done
select * from dim_date;   ---done

DELETE FROM fact_api_log;
DELETE FROM fact_complaint;
DELETE FROM fact_participant_project;
DELETE FROM fact_competition;
DELETE FROM fact_project_discussion;
DELETE FROM fact_bookmark_list;
DELETE FROM sub_dim_project_talent;
DELETE FROM sub_dim_project_image;
DELETE FROM dim_project;
DELETE FROM fact_project;
DELETE FROM dim_project_category;
DELETE FROM fact_angel_fund_participant;   --done
DELETE FROM dim_angel_fund;  ---done
DELETE FROM sub_dim_user_talent;
DELETE FROM fact_user_info;
DELETE FROM dim_user_info;  ---done
DELETE FROM dim_school;   ---done


DELETE FROM dim_date;   ---done


----

DROP TABLE IF EXISTS fact_api_log;
DROP TABLE IF EXISTS fact_complaint;
DROP TABLE IF EXISTS fact_competition;
DROP TABLE IF EXISTS fact_participant_project;
DROP TABLE IF EXISTS fact_project_discussion;
DROP TABLE IF EXISTS fact_bookmark_list;
DROP TABLE IF EXISTS sub_dim_project_talent;
DROP TABLE IF EXISTS sub_dim_project_image;
DROP TABLE IF EXISTS dim_project;
DROP TABLE IF EXISTS fact_project;
DROP TABLE IF EXISTS dim_project_category;
DROP TABLE IF EXISTS fact_angel_fund_participant;   --done
DROP TABLE IF EXISTS dim_angel_fund;  ---done
DROP TABLE IF EXISTS sub_dim_user_talent;
DROP TABLE IF EXISTS fact_user_info;
DROP TABLE IF EXISTS dim_user_info;  ---done
DROP TABLE IF EXISTS dim_school;   ---done
DROP TABLE IF EXISTS dim_date;   ---done


CREATE TABLE dim_date (
    id SERIAL primary key,
    date TIMESTAMP UNIQUE NOT NULL,
    year INTEGER NOT NULL,
    quarter_number INTEGER NOT NULL,
    quarter_name Text not null,
    month INTEGER NOT NULL,
    month_full_name Text not null,
    month_short_name Text not null,
    week_number INTEGER NOT NULL,
    week_full_name Text not null,
    week_short_name Text not null,
    day INTEGER NOT NULL,
    hour INTEGER NOT NULL,
    minute INTEGER NOT NULL
);

CREATE TABLE dim_school (
    id SERIAL primary key,
    school_type TEXT,
    domain TEXT UNIQUE,
    code TEXT,
    english_name TEXT,
    chinese_name TEXT,
    email_domains TEXT,
    school_icon Text,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);


CREATE TABLE dim_user_info (
    id SERIAL primary key,
    email_address TEXT UNIQUE NOT NULL,
    name TEXT NOT NULL,
    title TEXT,
    self_introduction TEXT,
    education_level TEXT,
    school_id INTEGER,
    year TEXT,
    mode_of_study TEXT,
    major TEXT,
    faculty TEXT,
    is_admin boolean,
    approved_at TIMESTAMP,
    blocked_at TIMESTAMP,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (school_id) REFERENCES dim_school(id)
);

CREATE TABLE fact_user_info (
    id SERIAL primary key,
    email_address TEXT NOT NULL,
    name TEXT NOT NULL,
    title TEXT,
    self_introduction TEXT,
    education_level TEXT,
    school_id INTEGER,
    year TEXT,
    mode_of_study TEXT,
    major TEXT,
    faculty TEXT,
    is_admin boolean,
    approved_at TIMESTAMP,
    blocked_at TIMESTAMP,
    date_id INTEGER,
    created_at TIMESTAMP,
    FOREIGN KEY (school_id) REFERENCES dim_school(id),
    FOREIGN KEY (date_id) REFERENCES dim_date(id)
);



CREATE TABLE sub_dim_user_talent (
    id SERIAL primary key,
    user_id integer NOT NULL,
    skill TEXT NOT NULL,
    created_at TIMESTAMP,
    removed_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES dim_user_info(id),
    UNIQUE(user_id,skill)

);



CREATE TABLE dim_angel_fund (
    id SERIAL primary key,
    title TEXT NOT NULL UNIQUE,
    short_desc TEXT,
    detail TEXT,
    file_url TEXT,
    application_deadline TIMESTAMP,
    canceled_at TIMESTAMP,
    blocked_at TIMESTAMP,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);



CREATE TABLE fact_angel_fund_participant (
    id SERIAL primary key,
    user_id INTEGER NOT NULL,
    angel_fund_id INTEGER NOT NULL,
    detail Text,
    applied_at TIMESTAMP,
    quitted_at TIMESTAMP,
    approved_at TIMESTAMP,
    rejected_at TIMESTAMP,
    date_id integer not NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (date_id) REFERENCES dim_date(id),
    FOREIGN KEY (angel_fund_id) REFERENCES dim_angel_fund(id)
);


CREATE TABLE dim_project_category (
    id SERIAL primary key,
    category_name TEXT UNIQUE NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE dim_project (
    id SERIAL primary key,
    title text not null,
    detail text not null,
    owner_user_id integer not NULL,
    category_id integer not NULL,
    chat_room_url TEXT,
    youtube_url text,
    criteria text,
    application_deadline TIMESTAMP,
    project_deadline TIMESTAMP,
    canceled_at TIMESTAMP,
    blocked_at TIMESTAMP,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (owner_user_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (category_id) REFERENCES dim_project_category(id)
);


CREATE TABLE fact_project (
    id SERIAL primary key,
    title text not null,
    detail text not null,
    owner_user_id integer not NULL,
    category_id integer not NULL,
    chat_room_url TEXT,
    youtube_url text,
    criteria text,
    application_deadline TIMESTAMP,
    project_deadline TIMESTAMP,
    canceled_at TIMESTAMP,
    blocked_at TIMESTAMP,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    date_id integer not NULL,
    FOREIGN KEY (owner_user_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (category_id) REFERENCES dim_project_category(id),
    FOREIGN KEY (date_id) REFERENCES dim_date(id)
);


CREATE TABLE sub_dim_project_image (
    id SERIAL primary key,
    project_id integer not NULL,
    project_image_url Text,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (project_id) REFERENCES dim_project(id)
);

CREATE TABLE sub_dim_project_talent (
    id SERIAL primary key,
    project_id integer NOT NULL,
    skill TEXT NOT NULL,
    created_at TIMESTAMP,
    removed_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (project_id) REFERENCES dim_project(id),
    UNIQUE(project_id,skill)

);

CREATE TABLE fact_bookmark_list (
    id SERIAL primary key,
    owner_user_id integer NOT NULL,
    project_id integer NOT NULL,
    date_id integer not NULL,
    liked_at TIMESTAMP,
    unliked_at TIMESTAMP,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (owner_user_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (project_id) REFERENCES dim_project(id),
    FOREIGN KEY (date_id) REFERENCES dim_date(id)
);


CREATE TABLE fact_project_discussion (
    id SERIAL primary key,
    user_id integer NOT NULL,
    project_id integer NOT NULL,
    discussion TEXT NOT NULL,
    date_id integer not NULL,
    created_at TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (project_id) REFERENCES dim_project(id),
    FOREIGN KEY (date_id) REFERENCES dim_date(id)
);


CREATE TABLE fact_competition (
    id SERIAL primary key,
    project_id INTEGER NOT NULL,
    title TEXT NOT NULL,
    url TEXT,
    start_date TIMESTAMP,
    cancel_date TIMESTAMP,
    date_id integer not NULL,
    created_at TIMESTAMP,
    FOREIGN KEY (project_id) REFERENCES dim_project(id),
    FOREIGN KEY (date_id) REFERENCES dim_date(id)
);

CREATE TABLE fact_participant_project (
    id SERIAL primary key,
    project_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    approver_id INTEGER,
    aprrove_at TIMESTAMP,
    detail TEXT,
    reject_at TIMESTAMP,
    date_id integer not NULL,
    created_at TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (project_id) REFERENCES dim_project(id),
    FOREIGN KEY (date_id) REFERENCES dim_date(id)
);
 

CREATE TABLE fact_complaint (
    id SERIAL primary key,
    user_id INTEGER,
    project_id INTEGER,
    respondent_id INTEGER,
    discussion_detail TEXT,
    discussion_created_at TIMESTAMP,
    detail TEXT,
    reviewed_at TIMESTAMP,
    date_id integer not NULL,
    created_at TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (respondent_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (project_id) REFERENCES dim_project(id),
    FOREIGN KEY (date_id) REFERENCES dim_date(id)
);


CREATE TABLE fact_api_log (
    id SERIAL primary key,
    table_name TEXT UNIQUE NOT NULL,
    system_action TEXT UNIQUE NOT NULL,
    api_user_email_address Text,
    project_title Text,
    project_participant_email_address Text,
    approver_email_address Text,   
    discussion Text,
    competition_title Text,
    angel_fund_name Text,
    angel_fund_participant_email_address Text,   
    complaint_text Text,
    respondent_email_address Text,
    api_url Text,
    api_method Text,
    api_name Text,
    remark Text,  
    applied_at TIMESTAMP,
    approved_at TIMESTAMP,
    rejected_at TIMESTAMP,
    canceled_at TIMESTAMP,
    blocked_at TIMESTAMP,
    liked_at TIMESTAMP,
    unliked_at TIMESTAMP,
    created_at TIMESTAMP,
    
    reference_id INTEGER,
    user_id INTEGER,
    project_id INTEGER,
    project_participant_id INTEGER,
    approver_id INTEGER,
    user_talent_id INTEGER,
    project_talent_id INTEGER,    
    competition_id INTEGER,
    angel_fund_id INTEGER,
    angel_fund_participant_id INTEGER,
    complaint_id INTEGER,
    respondent_id INTEGER,
    date_id integer not NULL,
    FOREIGN KEY (date_id) REFERENCES dim_date(id),
    FOREIGN KEY (user_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (project_id) REFERENCES dim_project(id),
    FOREIGN KEY (project_participant_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (approver_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (user_talent_id) REFERENCES sub_dim_project_talent(id),
    FOREIGN KEY (project_talent_id) REFERENCES sub_dim_project_talent(id),
    FOREIGN KEY (competition_id) REFERENCES fact_competition(id),
    FOREIGN KEY (angel_fund_id) REFERENCES dim_angel_fund(id),
    FOREIGN KEY (angel_fund_participant_id) REFERENCES fact_angel_fund_participant(id),                    
    FOREIGN KEY (complaint_id) REFERENCES fact_complaint(id),
    FOREIGN KEY (respondent_id) REFERENCES dim_user_info(id),
    FOREIGN KEY (date_id) REFERENCES dim_date(id)
);

