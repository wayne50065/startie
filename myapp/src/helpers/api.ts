

export function getAPIServer() {
    let origin = window.location.origin
    if(origin==='http://localhost:3000'){
        return 'http://localhost:3100'
    }
    if(origin.startsWith('http://192')){
        return origin.replace(':3000',':3100')
    }
    return 'https://api.startiehk.com'
}


export async function get(url: string,) {
    try {
        let res = await fetch(getAPIServer() + url, {
            // method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
        })
        let json = await res.json()
        return json
    } catch (error: any) {
        return { error: error.toString() }
    }

}

export async function post(url: string, body: any) {
    try {
        let res = await fetch(getAPIServer() + url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            body: JSON.stringify(body)
        })
        let json = await res.json()
        return json
    } catch (error: any) {
        return { error: error.toString() }
    }

}

export async function patch(url: string, body: any) {
    try {
        let res = await fetch(getAPIServer() + url, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            },
            body: JSON.stringify(body)
        })
        let json = await res.json()
        return json
    } catch (error: any) {
        return { error: error.toString() }
    }

}

export function toImageUrl(url: string) {
    if (!url) {
        return url
    }
    if (url.startsWith('data:')) {
        return url
    }
    if (!url.startsWith('/')) {
        url = '/uploads/' + url
    }
    return getAPIServer() + url
}