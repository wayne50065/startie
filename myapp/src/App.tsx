import { Redirect, Route } from "react-router-dom";
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import LoginPage from "./pages/LoginPage";
import ApplicantsPage from "./pages/ApplicantsPage";
import IdeaUploadPage from "./pages/IdeaUploadPage";
import RegisterPage from "./pages/RegisterPage";
import {
  addCircleOutline,
  bulbOutline,
  peopleOutline,
  ribbonOutline,
  trophyOutline,
} from "ionicons/icons";
import IdeasListPage from "./pages/IdeasListPage";
import CompetitionsListPage from "./pages/CompetitionsListPage";
import ProfileUpdatePage from "./pages/ProfileUpdatePage";
import PeoplePage from "./pages/PeoplePage";
import MorePage from "./pages/MorePage";
import ProfilePage from "./pages/ProfilePage";
import CompetitionPage from "./pages/CompetitionPage";
import BookmarksPage from "./pages/BookmarksPage";
import IdeaEditPage from "./pages/IdeaEditPage";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import FundApplyPage from "./pages/FundApplyPage";
import FundDetailsPage from "./pages/FundDetailsPage";
import FundListPage from "./pages/FundListPage";
import IdeaDetailPage from "./pages/IdeaDetailPage";
import { RegisterSuccess } from "./pages/RegisterSuccess";
import LoginSuccess from "./pages/LoginSuccess";
import NotFoundPage from "./pages/NotFoundPage";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { checkTokenThunk } from "./redux/auth/thunk";


/*

/tab/ideas           -> /idea/:id   -> /idea/:id/edit or /idea/:id/applicants
/tab/competitions
/tab/people
/tab/funds
/tab/more

 */

const App: React.FC = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(checkTokenThunk());
  }, []);
  const homepage = "/idea"

  return <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route exact={true} path='/register/:OTP'>
            <RegisterSuccess />
          </Route>
          <Route exact={true} path="/register">
            <RegisterPage />
          </Route>
          <Route exact={true} path='/login/:OTP'>
            <LoginSuccess />
          </Route>
          <Route exact={true} path="/login">
            <LoginPage />
          </Route>
          <Route exact={true} path="/profile/:id">
            <ProfilePage />
          </Route>
          <Route exact path="/people">
            <PeoplePage />
          </Route>



          <Route exact path="/idea-create" component={IdeaUploadPage} />
          <Route path="/idea-applicant/:idea_id" component={ApplicantsPage} />
          <Route path="/idea-edit/:idea_id" component={IdeaEditPage} />
          <Route path="/idea/:idea_id">
            <IdeaDetailPage />
          </Route>
          <Route exact path="/idea">
            <IdeasListPage />
          </Route>
          <Route path="/competitionsList">
            <CompetitionsListPage />
          </Route>
          <Route path="/competition/:competition_id">
            <CompetitionPage />
          </Route>

          <Route path="/angel-fund" component={FundListPage} />
          <Route exact path="/fund-details/:fund_id">
            <FundDetailsPage />
          </Route>
          <Route exact path="/fund-application/:fund_id">
            <FundApplyPage />
          </Route>

          <Route path="/more">
            <MorePage />
          </Route>
          <Route exact={true} path="/profile-update/:id">
            <ProfileUpdatePage />
          </Route>
          <Route exact path="/bookmarks">
            <BookmarksPage />
          </Route>
          <Route exact path="/">
            <Redirect to='/idea' />
          </Route>
          <Route path="/index.html" >
            <Redirect to='/'></Redirect>
          </Route>
          <Route>
            <NotFoundPage />
          </Route>
        </IonRouterOutlet>

        <IonTabBar slot="bottom">
          <IonTabButton tab="tab-ideas" href={homepage}>
            <IonIcon icon={bulbOutline} />
            <IonLabel>Projects</IonLabel>
          </IonTabButton>

          <IonTabButton tab="tab-competitions" href="/competitionsList">
            <IonIcon icon={trophyOutline} />
            <IonLabel>Competitions</IonLabel>
          </IonTabButton>

          <IonTabButton tab="tab-create" href="/people">
            <IonIcon icon={peopleOutline} />
            <IonLabel>People</IonLabel>
          </IonTabButton>

          <IonTabButton tab="tab-bookmarks" href="/angel-fund">
            <IonIcon icon={ribbonOutline} />
            <IonLabel>Funds</IonLabel>
          </IonTabButton>

          <IonTabButton tab="tab-more" href="/more">
            <IonIcon icon={addCircleOutline} />
            <IonLabel>More</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
};

export default App;
