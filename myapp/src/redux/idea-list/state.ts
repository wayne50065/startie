export type IdeaListState = {
    ideaItemList: IdeaItem[]
    error?: string | undefined
    
}

export type IdeaItem = {
    id: number
    title: string
    projectOwner: string
    category: string
    detail: string
    project_deadline: Date
    talentNeeded: string[]
    criteria: string
    youtube_url: string
    image: string
    chat_room_url: string
    competition_name: string
    competition_url: string
    competition_date: number | string | Date
    created_at: number | string | Date
}

export const initialState: IdeaListState = {
    ideaItemList: [
        // {
        //     id: 1, 
        //     title: "Start up",
        //     projectOwner: "kate",
        //     category: "IT",
        //     detail: "Looking for a creative member.",
        //     project_deadline: "1/11/2020",
        //     talentNeeded: ["IT","Marketing", "Fintech"],
        //     criteria: "HSU only",
        //     youtube_url: "www.youtube.com",
        //     image: "",
        //     chat_room_url: "www.youtube.com",
        //     competition_name: "IT Game",
        //     competition_url: "www.youtube.com",
        //     competition_date: "1/11/2020"
        // },
        // {
        //     id: 2, 
        //     title: "Create a Youtube channel",
        //     projectOwner: "Mark",
        //     category: "IT",
        //     detail: "This is content, without any paragraph or header tags, within an ion-cardContent element.",
        //     project_deadline: "1/11/2020",
        //     talentNeeded: ["IT"],
        //     criteria: "PolyU only",
        //     youtube_url: "www.youtube.com",
        //     image: "",
        //     chat_room_url: "www.youtube.com",
        //     competition_name: "IT Game",
        //     competition_url: "www.youtube.com",
        //     competition_date: "1/11/2020"
        // },
    ],
}
