import { IdeaItem } from "./state";

export function addIdeaItem(item: IdeaItem){
    return {
        type: 'addIdeaItem' as const,
        item,
    }
}

export function loadIdeaListSuccess(ideaList: IdeaItem[]){
    return{
        type: 'loadIdeaListSuccess' as const,
        ideaList,
    }
}

export function applyIdea(id: number){
    return{
        type:'applyIdea' as const,
        id,
    }
}


export function setIdeaListError(reason: string){
    return{
        type:'setIdeaListError' as const,
        reason,
    }
}


export type IdeaListAction = 
    | ReturnType<typeof addIdeaItem>
    | ReturnType<typeof loadIdeaListSuccess>
    | ReturnType<typeof applyIdea>
    | ReturnType<typeof setIdeaListError>