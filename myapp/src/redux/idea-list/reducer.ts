import { IdeaListAction } from "./action";
import { initialState, IdeaListState } from "./state";


export const ideaListReducer = (
    state: IdeaListState = initialState,
    action: IdeaListAction,
): IdeaListState => {
    switch (action.type) {
        case 'addIdeaItem':{
            let newIdeaItemList = [action.item, ...state.ideaItemList]
            return{
                ideaItemList: newIdeaItemList,
            }
        }
        case 'loadIdeaListSuccess':
            return {
                ideaItemList: action.ideaList
            }
        case 'applyIdea':
            return {
                ideaItemList: state.ideaItemList
            }
        case 'setIdeaListError':
            return{
             ...state,
             error: action.reason,
            }   
        default:
            return state
    }
}