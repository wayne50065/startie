import { getAPIServer } from "../../helpers/api"
import { errorToString } from "../../helpers/format"
import { RootDispatch } from "../dispatch"
import { RootState } from "../state"
import { addIdeaItem, applyIdea, loadIdeaListSuccess, setIdeaListError } from "./action"
import { IdeaItem } from "./state"


export function addIdeaItemThunk(){
    return async(dispatch:RootDispatch, getState: ()=> RootState) => {
        
        // let token = getState().auth.token

        // if(!token){
        //     dispatch(setIdeaListError('Please login.'))
        //     return
        // }

        let origin 
        try {
            origin = getAPIServer()
        } catch (error) {
            dispatch(setIdeaListError(errorToString(error)))
            return
        }
        
        let json: any
        try {
            let res = await fetch(`${origin}/idea/form`,{
                method: 'POST',
                headers: {
                    // Authorization: 'Bearer ' + token,
                    'content-type': 'application/json',
                },
                body: JSON.stringify({})
            })
            json = await res.json()
        }catch(error) {
            console.error('Failed to Post /idea/form')
            dispatch(setIdeaListError(errorToString(error)))
            return
        }
        
        if (json.error) {
            console.error('Post /idea/form response error', json.error)
            dispatch(setIdeaListError(json.error))
            return
        }

        let id: number = json.id


        
    }
    
}



export function applyIdeaThunk(){
    return async(dispatch: RootDispatch) => {
                
        // let token = getState().auth.token

        // if(!token){
        //     dispatch(setIdeaListError('Please login.'))
        //     return
        // }
                
        let origin
        try {
            origin = getAPIServer()
        } catch (error) {
            dispatch(setIdeaListError(errorToString(error)))
            return
        }

        let json: any
        try {
            let res = await fetch(`${origin}/idea/form`,{
                method: 'POST',
                headers: {
                    // Authorization: 'Bearer ' + token,
                    'content-type': 'application/json',
                },
                body: JSON.stringify({})
            })
            json = await res.json()
        } catch (error) {
            console.error('Failed to POST /idea/', error)
            dispatch(setIdeaListError(errorToString(error)))
            return
        }
                
        if (json.error) {
            console.error('Post /idea/ response error', json.error)
            dispatch(setIdeaListError(json.error))
            return
        }

    }
}