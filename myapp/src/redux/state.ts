import {AuthState} from './auth/state'
import {IdeaListState} from './idea-list/state'
import { RouterState } from 'connected-react-router'
import { FundListState } from './fund/state'
import { UserState } from './user/state'


export type RootState = {
    user: UserState
    auth: AuthState
    ideaList: IdeaListState
    fundList: FundListState
    router: RouterState
}