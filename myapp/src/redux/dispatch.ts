import {RootState} from './state'
import {RootAction} from './action'
import {ThunkDispatch} from 'redux-thunk'


export type RootDispatch=
    ThunkDispatch<RootState, {}, RootAction>