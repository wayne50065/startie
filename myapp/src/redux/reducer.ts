import { connectRouter } from "connected-react-router";
import { combineReducers } from "redux";
import { RootAction } from "./action";
import { ideaListReducer } from "./idea-list/reducer";
import { authReducer } from "./auth/reducer";
import { RootState } from "./state";
import history from './history'
import { fundListReducer } from "./fund/reducer";
import { userReducer } from "./user/reducer";


export const rootReducer: (
    state: RootState | undefined,
    action: RootAction,
) => RootState = combineReducers({
    user: userReducer,
    auth: authReducer,
    ideaList: ideaListReducer,
    fundList: fundListReducer,
    router: connectRouter(history),
})