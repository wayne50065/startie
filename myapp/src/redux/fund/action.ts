import { FundItem } from "./state";


export function loadFundList(fundList: FundItem[]){
    return{
        type: 'loadFundList' as const,
        fundList,
    }
}

export function setFundListError(reason: string){
    return{
        type:'setFundListError' as const,
        reason,
    }
}

export type FundListAction =
    | ReturnType<typeof loadFundList>
    | ReturnType<typeof setFundListError>