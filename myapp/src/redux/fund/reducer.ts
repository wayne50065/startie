import { initialState } from "../fund/state";
import { FundListAction } from "./action";
import { FundListState } from "./state";


export const fundListReducer = (
    state: FundListState = initialState,
    action: FundListAction,
) : FundListState => {
    switch (action.type) {
        case 'loadFundList':
            return {
            fundItemList: action.fundList
        }
        case 'setFundListError':
            return{
             ...state,
             error: action.reason,
            }   
        default:
            return state
    }
}