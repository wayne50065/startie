export type FundListState = {
    fundItemList: FundItem[]
    error?: string | undefined
}

export type FundItem = {
    id: number
    title: string
    fundImg: string
    shortDesc:string
    detail:string
    applicationDeadline: number | string | Date
    // cancelDate: number
    // blockDate: number
}

export const initialState: FundListState ={
    fundItemList:[]
}

// {id: 1,
//     title: "fund",
//     shortDesc: "Start to apply",
//     detail:"The Angel Investment Network connects business entrepreneurs in Hong Kong and South East Asia with Angel Investors located globally.",
//     applicationDeadline: '1/1/2020',
//     },
//     {id: 4,
//         title: "GOOD fund",
//         shortDesc: "Apply now",
//         detail:"details of fund",
//         applicationDeadline: '1/1/2020',
//     },