import { getAPIServer } from "../../helpers/api";
import { errorToString } from "../../helpers/format";
import { RootDispatch } from "../dispatch";
import { RootState } from "../state";
import { loadFundList, setFundListError } from "./action";
import { FundItem } from "./state";


export function loadFundListThunk(){
    return async(dispatch: RootDispatch) => {
        
        let origin
        try {
            origin = getAPIServer()
        } catch (error) {
            dispatch(setFundListError(errorToString(error)))
            return
        }

        let json: any
        try {
            let res = await fetch(`${origin}/fund/all`)
            json = await res.json()
        } catch (error) {
            console.error('Failed to GET /fund/all', error)
            dispatch(setFundListError(errorToString(error)))
            return
        }

        if (json.error) {
            console.error('GET /fund/all response error', json.error)
            dispatch(setFundListError(json.error))
            return
        }

        let fundList: FundItem[] = json.fundList
        dispatch(loadFundList(fundList))
    }
}