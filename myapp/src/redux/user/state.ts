

export interface UserState {
  userInfo: UserInfo;
}
export interface UserInfo {
  id: number,
  email_address: string,
  name: string,
  is_admin: boolean,
  title: string,
  personal_image_url: string,
  student_card_url: string,
  self_introduction: string,
  education_level: string,
  year: string,
  mode_of_study: string,
  school_name: string,
  major: string,
  faculty: string,
  approved_at: string,
  blocked_at: string,
  skill: string[]
}

export const initialState: UserState = {
  userInfo: {
    id: 0,
    email_address: "default@default.com",
    name: "default",
    is_admin: false,
    title: "N/a",
    personal_image_url: "N/a",
    student_card_url: "N/a",
    self_introduction: "N/a",
    education_level: "N/a",
    year: "0",
    mode_of_study: "part-Time",
    school_name: "HiHi University",
    major: "N/a",
    faculty: "N/a",
    approved_at: "N/a",
    blocked_at: "N/a",
    skill:["none","none","none"],
  },
};
