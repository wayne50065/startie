import { UserInfo} from "./state";

export function saveUserInfo(userInfo: UserInfo) {
  return {
    type: "/saveUserInfo" as const,
    userInfo,
  };
}


export type UserAction =
  | ReturnType<typeof saveUserInfo>;
