import { UserAction } from "./action";
import { initialState, UserState } from "./state";

export const userReducer = (
  state: UserState = initialState,
  action: UserAction
): UserState => {
  switch (action.type) {
    case "/saveUserInfo":
      return {userInfo:action.userInfo}
      default:
        return state
  }
};
