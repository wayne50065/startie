import { getAPIServer } from "../../helpers/api";
import { RootDispatch } from "../dispatch";
import { saveUserInfo } from "./action";

export async function loadUserInfo() {
  return async (dispatch: RootDispatch) => {
    let origin = getAPIServer();
    let token = localStorage.getItem("token");
    if (!token) {
      console.log("cannot get token");
      return;
    }
    try {
      const res = await fetch(`${origin}/getCurrentUser`, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      });
      if (res.status === 200) {
        console.log("Successfully get user info from DB");
        const resultSQL = await res.json();

        dispatch(saveUserInfo(resultSQL.userInfos));
        console.log(resultSQL.userInfos);
        return;
      }
    } catch (e) {
      console.log("Fail to fetch /getUserInfo @userThunk");
      return;
    }
  };
}
