import { FundListAction } from './fund/action'
import {IdeaListAction} from './idea-list/action'
import { AuthAction } from './auth/action'
import { UserAction } from './user/action'
export * from './idea-list/action'

export type RootAction = AuthAction | IdeaListAction | FundListAction | UserAction