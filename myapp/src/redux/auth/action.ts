import { JWTpayload } from "../../../../server/utils/models";


export function loginSuccess(payload:JWTpayload, token:string) {
  return {
    type: "/loginSuccess" as const,
    payload,
    token,
  };
}

export function loginFailed(reason: string) {
  return {
    type: "/loginFailed" as const,
    reason,
  };
}

export function logout() {
  return {
    type: "/logout" as const,
  };
}
export type AuthAction =
  | ReturnType<typeof loginSuccess>
  | ReturnType<typeof loginFailed>
  | ReturnType<typeof logout>;
