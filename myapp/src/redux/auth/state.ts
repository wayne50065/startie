import {JWTpayload} from '../../../../server/utils/models'

export interface AuthState{
    payload?:JWTpayload,
    error?: string
    token?: string
}

export const initialState:AuthState={payload:undefined}
