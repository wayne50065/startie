import { JWTpayload } from "../../../../server/utils/models";
import { RootDispatch } from "../dispatch";
import { loginFailed, loginSuccess, logout } from "./action";
import jwtDecode from "jwt-decode";
import { getAPIServer } from "../../helpers/api";
import { errorToString } from "../../helpers/format";

export function checkTokenThunk() {
  return (dispatch: RootDispatch) => {
    let token = localStorage.getItem("token");
    console.log("token:  ",token)
    if (!token) {
      dispatch(logout());
      return;
    }
    let payload: JWTpayload = jwtDecode(token);
    dispatch(loginSuccess(payload, token));
    return
  };
}

export function handleTokenThunk(token: string) {
  return (dispatch: RootDispatch) => {
    console.log("putting token into local storage")//==============dev testing
    localStorage.setItem("token", token);
    try {
      console.log("trying to decode token to get payload")//==============dev testing
      const payload: JWTpayload = jwtDecode(token);
      console.log("pushing payload and token into state @dispatching loginSuccess")//==============dev testing
      dispatch(loginSuccess(payload, token));
      return;
    } catch (e) {
      dispatch(loginFailed((e as Error).toString()));
      return;
    }
  };
}
