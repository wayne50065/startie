import { AuthAction } from "./action";
import {initialState,AuthState} from "./state"

export const authReducer=(state:AuthState=initialState, action:AuthAction):AuthState=>{
    switch (action.type){
        case "/loginFailed":
        return {error:action.reason}
        case '/loginSuccess':
        return {payload:action.payload, token:action.token}
        case '/logout':
        return {}
        default:
        return state
    }
}