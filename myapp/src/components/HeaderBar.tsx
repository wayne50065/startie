import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCol,
  IonGrid,
  IonIcon,
  IonInput,
  IonItem,
  IonRow,
  IonSearchbar,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { useState } from "react";
import {
  arrowBackSharp,
  closeSharp,
  search,
  searchOutline,
} from "ionicons/icons";
import style from "../theme/clientTheme.module.scss";
import "./HeaderBar.css";
import {} from "@capacitor/core";


type SearchBarProps = {
  close: () => void;
  onSearch: (text: string) => void;
};
function SearchBarMD(props: SearchBarProps) {
  return (
    <>
      <IonToolbar>
        <IonButtons slot="start">
          <IonButton onClick={props.close} color="medium">
            <IonIcon src={arrowBackSharp}></IonIcon>
          </IonButton>
        </IonButtons>
        <IonInput placeholder="Search"></IonInput>
        <IonButtons slot="end">
          <IonButton color="medium">
            <IonIcon src={closeSharp}></IonIcon>
          </IonButton>
        </IonButtons>
      </IonToolbar>
    </>
  );
}
function SearchBarIOS(props: SearchBarProps) {
  return (
    <>
      <IonItem>
        <IonSearchbar color="light" className="ion-no-padding" />
        <IonButtons slot="end">
          <IonButton onClick={props.close}>Cancel</IonButton>
        </IonButtons>
      </IonItem>
    </>
  );
}

function SearchBar(props: SearchBarProps) {
  if (navigator.userAgent.includes("iPhone")) {
    return <SearchBarIOS {...props} />;
  }
  return <SearchBarMD {...props} />;
}

interface HeaderBarProps {
  backBtn?: boolean;
  title: string;
  backLocation?: string;
  search?: boolean;
  afterTitle?:JSX.Element
}
export const HeaderBar = (props: HeaderBarProps) => {
  const [searchOn, setSearchOn] = useState(false);
  function toggleSearch() {
    setSearchOn(!searchOn);
  }
  if ("dev") {
    if (searchOn) {
      return <SearchBar onSearch={() => {}} close={toggleSearch} />;
    }
    return (
      <>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref={props.backLocation} />
          </IonButtons>
          <IonTitle>{props.title}</IonTitle>
          {props.afterTitle}
          {props.search ? (
            <IonButtons slot="end">
              <IonButton onClick={toggleSearch}>
                <IonIcon src={searchOutline}></IonIcon>
              </IonButton>
            </IonButtons>
          ) : null}
        </IonToolbar>
      </>
    );
  }
  return (
    <header className="headerBar">
      <IonGrid>
        <IonRow>
          <IonCol size="2">
            {props.backBtn ? (
              <IonButtons slot="">
                <IonBackButton
                  text=""
                  className="back-btn"
                  defaultHref={props.backLocation}
                />
              </IonButtons>
            ) : (
              <></>
            )}
          </IonCol>
          <IonCol size="8" className="titleBox">
            <div>{props.title}</div>
          </IonCol>
          <IonCol size="2" className="colRight">
            {props.search ? (
              <IonButtons
                onClick={() => {
                  setSearchOn(!searchOn);
                  console.log(searchOn);
                }}
              >
                <IonIcon icon={search} slot="start"></IonIcon>
              </IonButtons>
            ) : (
              <></>
            )}
          </IonCol>
        </IonRow>
      </IonGrid>
      {searchOn ? (
        <IonSearchbar className={style.whiteTheme}></IonSearchbar>
      ) : (
        <></>
      )}
    </header>
  );
};

export default HeaderBar;
