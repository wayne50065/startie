import {
  IonBadge,
  IonCard,
  IonCol,
  IonImg,
  IonList,
  IonRow,
} from "@ionic/react";
import { getAPIServer } from "../helpers/api";
import default_pic from "../temp_local_img/default_project_pic.jpg";

import "./ProfileCards.css";
interface Props {
  details: ProjectsCards[];
}
export interface ProjectsCards {
  id: number;
  title: string;
  images: string[];
  talents: string[];
  criteria: string;
}
export const ProjectCards = (props: Props) => {
  return (
    <>
      {props.details.map((project) => {
        return (
          <IonCard
            className="projectCard ion-text-start"
            routerLink={"/idea/" + project.id}
          >
            {project.criteria ? (
              <div className="underLine">
                <IonBadge color="danger" className="ion-margin-bottom">
                  {project.criteria}
                </IonBadge>
              </div>
            ) : null}
            <IonRow>
              <IonCol size="8">
                <h4>{project.title}</h4>
              </IonCol>

              <IonCol size="4">
                <div className="projectImg">
                  <IonImg
                    src={
                      project.images[0] === null
                        ? `${default_pic}`
                        : `${getAPIServer()}/uploads/${project.images[0]}`
                    }
                  />
                </div>
              </IonCol>
            </IonRow>
            <IonList>
              {project.talents.map((talent) => (
                <IonBadge className="margin-bottom-5px">{talent}</IonBadge>
              ))}
              {/* <p className="projectContent">{project.detail}</p> */}
            </IonList>
          </IonCard>
        );
      })}
    </>
  );
};
