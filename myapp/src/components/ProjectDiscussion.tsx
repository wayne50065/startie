import {
  IonAvatar,
  IonButton,
  IonCard,
  IonCol,
  IonGrid,
  IonIcon,
  IonImg,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonRow,
  IonText,
} from "@ionic/react";
import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { getAPIServer } from "../helpers/api";

import icon from "../temp_local_img/sj.png";

export type discussion = {
  userName: string
  // userImage: File
  discussion: string
  created_at: Date
}

export const ProjectDiscussionContent: React.FC = () => {
  const [ideaDiscussion, setIdeaDiscussion] = useState<discussion>()
  const [error, setError] = useState('')

  const params = useParams<{ idea_id: string }>()
  const idea_id = parseInt(params.idea_id)

  async function loadMessage(){
    try {
      let res = await fetch(`${getAPIServer()}/idea/${idea_id}/message`)
      let json = await res.json()
      if (json.error) {
        setError(json.error)
      } else {
        setIdeaDiscussion(json.ideaMessage)
        setError('')
      }
    } catch (error) {
      setError((error as Error).toString())
    }
  }

  useEffect(() => {
    loadMessage()
  }, [idea_id])

  if (!ideaDiscussion) {
    return (
      <div>
        <p>404: Discussion not found!</p>
      </div>
    )
  }

  return (
    <>
      <IonCard>
                <IonGrid>
                  <IonRow>
                    <IonCol size="2">
                      <IonAvatar className="ion-align-items-start">
                        <img src={icon} />
                      </IonAvatar>
                    </IonCol>
                    <IonCol size="10">
                      <p>{ideaDiscussion.discussion}</p>
                      {/* <p>Want to join.</p> */}
                    </IonCol>
                  </IonRow>
                  <IonLabel className="ion-Label-end">{ideaDiscussion.created_at}</IonLabel>
                </IonGrid>
                <IonText>{ideaDiscussion.created_at}</IonText>
              </IonCard>
    </>

  );
};


// {
// <IonCard>
// <IonGrid>
//   <IonRow>
//     <IonCol size="2">
//       <IonAvatar className="ion-align-items-start">
//         <img src={icon} />
//       </IonAvatar>
//     </IonCol>
//     <IonCol size="10">
//       <p>
//         Ionic Framework is an open source UI toolkit for building
//         performant, high-quality mobile and desktop apps using web
//         technologies.
//       </p>
//     </IonCol>
//   </IonRow>
//   <IonLabel className="ion-Label-end">posted at: time</IonLabel>
// </IonGrid>
// </IonCard>

// <IonList>
// <IonItem>
//   <IonAvatar slot="start">
//     <IonImg src={icon} />
//   </IonAvatar>

//   {/* <IonLabel className="ion-Label-start">
//     <h2>Steve</h2>

//   </IonLabel> */}
//   <p>I'm interested in marketing.</p>
//   {/* <IonLabel className="ion-Label-end">posted at: time</IonLabel> */}
// </IonItem>
// </IonList>

// <IonItem className="ion-margin-vertical">
// <IonInput value={message} onIonChange={e => setMessage(e.detail.value || '')} placeholder="Message"></IonInput>
// <IonButton slot="end" size="default" color="primary" onClick={onTexted}>
//   Send
// </IonButton>
// </IonItem>}