import React, { useState } from 'react';
import { IonToast, IonContent, IonButton } from '@ionic/react';

interface Toast{
    message:string,
    position?:"bottom" | "middle" | "top",
    duration?:number,
    translucent?:boolean|false,
    isOpen:boolean,
    color?:"primary"| "secondary"| "tertiary"| "success"| "warning"| "danger"| "light"| "medium"| "dark"
}

export const Toast= (props: Toast) => {


  return (
    <IonContent>
      <IonToast
        isOpen={props.isOpen}
        message={props.message}
        position={props.position}
        duration={props.duration}
        color={props.color}
        buttons={[
          {
            text: 'Done',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');

            }
          }
        ]}
      />
    </IonContent>
  );
};