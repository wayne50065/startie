import {
  IonCard,
  IonCardContent,
  IonText,
} from "@ionic/react";

type Props = {
  message: string | undefined;
};

export default function ErrorMessage(props: Props) {
  const { message } = props;

  if (!message) {
    return <></>;
  }

  return (
    <IonCard>
      <IonCardContent>
        <IonText color="danger">{message}</IonText>
      </IonCardContent>
    </IonCard>
  );
}
