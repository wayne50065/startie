import { useState } from "react";
import "./ProjectAbout.css";

export type Idea = {
  id: number
  title: string
  projectOwner: string
  category: string
  detail: string
  project_deadline: Date
  talentNeeded: string[]
  criteria: string
  youtube_url: string
  image: string
  chat_room_url: string
  competition_name: string
  competition_url: string
  competition_date: number | string | Date
  created_at: number | string | Date
}


export function ProjectAbout() {
  const [ideaDetail, setIdeaDetail] = useState<Idea[]>([])
  const [error, setError] = useState('')
  const [going, setGoing] = useState(false)


  function onApplied() {
    setGoing(true)
  }



  return (
    <></>
  )
};

export default ProjectAbout;
