import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol, IonContent, IonHeader, IonIcon, IonImg, IonItem, IonLabel, IonPage, IonRow } from "@ionic/react";
import { get, getAPIServer } from '../helpers/api';
import { calendarOutline, navigateOutline } from "ionicons/icons";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import HeaderBar from "../components/HeaderBar";
import "./CompetitionPage.css";

export type Competition = {
  id: number
  title: string
  detail: string
  image_url: string
  date: Date
  url: string
}

export const Competition: React.FC = () => {
  const [competitionDetail, setCompetitionDetail] = useState<Competition>();
  const [error, setError] = useState('')

  const params = useParams<{ competition_id: string }>()
  const competition_id = parseInt(params.competition_id)

  async function loadCompetitionDetail() {
    try {
      let json = await get(`/competition/${competition_id}`)
      if (json.error) {
        setError(json.error)
      } else {
        setCompetitionDetail(json.competitionDetail)
        setError('')
      }
    } catch (error) {
      setError((error as Error).toString())
    }
  }

  useEffect(() => {
    loadCompetitionDetail()
  }, [])

  return (
    <IonPage>
      <IonHeader>
        <HeaderBar
          title="Competitions"
          backBtn={true}
          backLocation="/competitionsList"
        />
      </IonHeader>

      <IonContent>
        {!competitionDetail ?
          <p>404: Competition not found!</p>
          :
          <IonCard>
            <IonImg src={`${getAPIServer()}/uploads/${competitionDetail.image_url}`}/>
            <IonCardHeader>
              <IonCardTitle className="">{competitionDetail.title}</IonCardTitle>
            </IonCardHeader>
            <IonCardContent>
              <IonCardSubtitle>Description</IonCardSubtitle>
              <p className="ion-padding-bottom">{competitionDetail.detail}</p>

              <IonRow>
                <IonCol size="4">
                  <IonItem className="ion-no-padding" lines="none">
                    <IonIcon
                      className="com-detail-icon"
                      icon={calendarOutline}
                      slot="start"
                    ></IonIcon>
                    <IonLabel className="ion-no-margin">Date</IonLabel>
                  </IonItem>
                </IonCol>
                <IonCol size="7">
                  <IonItem className="ion-no-padding" lines="none">
                    {new Date(competitionDetail.date).toLocaleDateString('en-GB', { day: 'numeric', month: 'short', year: 'numeric' })}
                  </IonItem>
                </IonCol>
              </IonRow>
              <IonRow>
                <IonCol size="4">
                  <IonItem className="ion-no-padding" lines="none">
                    <IonIcon
                      className="com-detail-icon"
                      icon={navigateOutline}
                      slot="start"
                    ></IonIcon>
                    <IonLabel>Url</IonLabel>
                  </IonItem>
                </IonCol>
                <IonCol size="7">
                  <IonItem className="ion-no-padding" lines="none">
                    <a className="com-detail-link" href={competitionDetail.url}>
                      {competitionDetail.url}
                    </a>
                  </IonItem>
                </IonCol>
              </IonRow>
            </IonCardContent>
          </IonCard>
        }
      </IonContent>
    </IonPage>
  );
};

export default Competition;
