import React, { useEffect, useState } from 'react';
import { IonContent, IonHeader, IonPage, IonCard, IonIcon, IonSearchbar, IonRow, IonCol, IonBadge, IonImg, IonFab, IonFabButton, IonInfiniteScroll, IonInfiniteScrollContent, useIonViewWillEnter, IonList, IonItem } from '@ionic/react';
import { createOutline } from 'ionicons/icons';
import HeaderBar from "../components/HeaderBar";
import eventPic from "../temp_local_img/ideas_temp.png";
import './IdeasListPage.css'
import { get, getAPIServer } from '../helpers/api';
import default_pic from '../temp_local_img/default_project_pic.jpg'
import { useParams } from 'react-router';

export type Idea = {
  id: number
  title: string
  talents: string[]
  criteria: string
  images:string[]
  created_at:string
}

export function IdeasList() {
  const params = useParams<string>();
  const [searchText, setSearchText] = useState('');
  const [ideaList, setIdeaList] = useState<Idea[]>([])
  const [error, setError] = useState('')
  const token=localStorage.getItem("token")

  async function loadIdeaList() {
    try {
      let json = await get(`/idea/all`)
      if (json.error) {
        setError(json.error)
      } else {
        // setIdeaList(json.ideaList)
        setError('')
        const max = ideaList.length + 5;
        const min = max - 5;
        const newIdeaList: Idea[] = [];
        if(max>json.ideaList.length){
          return
        }
        for (let i = min; i < max; i++) {
          if(i>json.ideaList.length){
            break
          }
          newIdeaList.push(json.ideaList[i]);
          setIdeaList([...ideaList, ...newIdeaList]);
        }
      }
    } catch (error) {
      setError((error as Error).toString())
    }
  }


  const loadData = (ev: any) => {
    setTimeout(() => {
      loadIdeaList()
      ev.target.complete();
    }, 500);
  };

  useIonViewWillEnter(() => {
    loadIdeaList()

  });
  useEffect(()=>{
    loadIdeaList()
  },[params])
  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="Projects" />
        {/* <IonSearchbar value={searchText} onIonChange={e => setSearchText(e.detail.value!)}></IonSearchbar> */}
      </IonHeader>

      <IonContent>
        <IonImg src={eventPic} />
        {!ideaList ?
            <p>404: Fund not found!</p>
        //   : searchText?
        // ideaList.filter(idea => idea.title = searchText  )
        :   <div>
            {ideaList.map(item =>
              <IonCard className="projectCard" routerLink={"/idea/" + item.id} key={item.id}>
                {item.criteria?<div className="underLine">
                <IonBadge color="danger" className="ion-margin-bottom">{item.criteria}</IonBadge>
                </div>:null}
                
                <IonRow  className="ion-margin-bottom">
                  <IonCol size='8'>
                    <h4>{item.title}</h4>
                    
                  </IonCol>
                  <IonCol size='4'>
                    <div className="project-image">
                      <IonImg className="image"src={item.images[0]===null?`${default_pic}`:`${getAPIServer()}/uploads/${item.images[0]}`}/>
                    </div>
                  </IonCol>
                </IonRow>
                <IonList>
                {item.talents.map(talent =>
                      <IonBadge className="margin-bottom-5px">{talent}</IonBadge>)}
                </IonList>
              </IonCard>
            )}
          </div>
        }

        <IonFab vertical="bottom" horizontal="end" slot="fixed">
          <IonFabButton color="secondary" routerLink={token?`/idea-create`:`/login`}>
            <IonIcon icon={createOutline} />
          </IonFabButton>
        </IonFab>
        <IonInfiniteScroll onIonInfinite={loadData} threshold="100px">
          <IonInfiniteScrollContent
            loadingSpinner="bubbles"
            loadingText="Loading more data..."
          ></IonInfiniteScrollContent>
        </IonInfiniteScroll>
      </IonContent>
    </IonPage>
  );
};


export default IdeasList;