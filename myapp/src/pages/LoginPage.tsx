import {
  IonButton,
  IonContent,
  IonFooter,
  IonHeader,
  IonImg,
  IonInput,
  IonPage,
  IonText,
  useIonToast,
} from "@ionic/react";
import { checkTokenThunk } from "../redux/auth/thunk";
import { useEffect, useState } from "react";
import logo from "../temp_local_img/logo.png";
import HeaderBar from "../components/HeaderBar";
import "./LoginPage.css";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { getAPIServer } from "../helpers/api";

const LoginPage: React.FC = () => {
  let origin = window.location.origin;
  const [loginEmail, setLoginEmail] = useState("");
  const [OTP, setOTP] = useState("");
  const [present, dismiss] = useIonToast();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(checkTokenThunk());
  }, []);

  async function Login(loginEmail: string) {
    const origin = getAPIServer();
    try {
      let res = await fetch(`${origin}/login/getOTP`, {
        method: "POST",
        headers: {
          "content-Type": "application/json",
        },
        body: JSON.stringify({ loginEmail: loginEmail, origin }),
      });
      const result = await res.json();
      if (result.error) {
        present({
          buttons: [{ text: "close" }],
          position: "bottom",
          duration: 1000,
          color: "danger",
          message: result.error,
        });
      } else {
        present({
          buttons: [{ text: "close" }],
          position: "bottom",
          duration: 1000,
          color: "dark",
          message: "OTP has sent!",
        });
      }
    } catch (e) {
      console.log((e as Error).toString());
      return;
    }
  }
  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="Sign in" backBtn={true} backLocation="/more" />
      </IonHeader>
      <IonContent>
        <div className="login-container ion-justify-content-center ion-padding">
          <IonImg src={logo} alt="client Project Name and Logo" />
          <form action="login" className={`loginPage-Form`}>
            <IonInput
              className="ion-margin-vertical"
              type="email"
              placeholder="Email*"
              onIonChange={(e) => {
                setLoginEmail(e.detail.value!);
              }}
            ></IonInput>

            <IonButton
              type="button"
              className={`sdOTP-Btn`}
              onClick={() => {
                dismiss();
                Login(loginEmail);
              }}
            >
              Send Authorization Link
            </IonButton>
            <IonInput
              className="ion-margin-vertical"
              placeholder="OTP"
              onIonChange={(e: any) => {
                setOTP(e.detail.value);
              }}
            ></IonInput>
            <IonButton
              href={
                loginEmail === "demo.master.key"
                  ? `/login/LoGinASmAstER`
                  : `/login/${OTP}`
              }
            >
              {loginEmail === "demo.master.key"
                ? "Login as Master"
                : "Login with OTP"}
            </IonButton>
            <IonButton href="/register">Register</IonButton>
          </form>
          <IonText className="ion-text-center">
            By proceeding you also agree to the Terms of Service and Privacy
            Policy
          </IonText>
        </div>
      </IonContent>
      <IonFooter></IonFooter>
    </IonPage>
  );
};

export default LoginPage;
