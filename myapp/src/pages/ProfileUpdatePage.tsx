import React, { useEffect, useState } from "react";
import {
  IonFooter,
  IonButton,
  IonContent,
  IonHeader,
  IonPage,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonSelect,
  IonSelectOption,
  IonBadge,
} from "@ionic/react";
import HeaderBar from "../components/HeaderBar";
import { useDispatch } from "react-redux";
import { checkTokenThunk } from "../redux/auth/thunk";

import { getAPIServer } from "../helpers/api";
import { saveUserInfo } from "../redux/user/action";
import { UserInfo } from "../redux/user/state";
import { useParams } from "react-router";

export const ProfileUpdateForm: React.FC = () => {
  let origin = getAPIServer();
  let token = localStorage.getItem("token");
  const params = useParams<{ id: string }>();
  const user_id = parseInt(params.id);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(checkTokenThunk());
    loadUserInfo();
  }, [params]);
  // const userInfos=useSelector((state:RootState)=>state.user.userInfo)
  const [title, setTitle] = useState<string>();

  const [userInfos, setUserInfos] = useState<UserInfo>({ skill: [] } as any);
  const skillSet=new Set<string>()
  for(let skill of userInfos.skill){
      skillSet.add(skill)
  }

  async function loadUserInfo() {

    if (!token) {
      console.log("cannot get token");
      return;
    }
    try {
      console.log("trying to get user data")
      const res = await fetch(`${origin}/getUser/${user_id}`, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      });
      if (res.status === 200) {
        console.log("Successfully get user info from DB");
        const resultSQL = await res.json();
        dispatch(saveUserInfo(resultSQL.userInfos));

        setUserInfos(resultSQL.userInfos);
        console.log("userInfos :", resultSQL.userInfos);
        return;
      }
    } catch (e) {
      console.log("Fail to fetch /getUserInfo @userThunk");
      return;
    }
  }
  async function updateUserProfile(){
    let formData = new FormData()

        // formData.append('info',info)
        // if(file){
        //     formData.append('file',file,file.name)
        // }
  }

  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="Profile" backBtn={true} backLocation={`/profile/${user_id}`} />
      </IonHeader>
      <IonContent>
        <IonList>
          <IonItem>
            <IonLabel>Title</IonLabel>
            <IonSelect
              value={userInfos.title}
              placeholder="Select"
              onIonChange={(e) => setTitle(e.detail.value)}
            >
              <IonSelectOption value="Mr">Mr</IonSelectOption>
              <IonSelectOption value="Mrs">Mrs</IonSelectOption>
              <IonSelectOption value="Ms">Ms</IonSelectOption>
            </IonSelect>
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Name</IonLabel>
            <IonInput value={userInfos.name}></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel position="stacked">Personal image</IonLabel>
            <input type="file" value=""></input>
          </IonItem>
          <IonItem>
            <IonLabel position="stacked">Skill</IonLabel>
            {userInfos.skill.map((skill)=>{return (<IonBadge className="margin-bottom-5px">{skill}</IonBadge>)})}
            <IonInput
              value=""
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Self introduction</IonLabel>
            <IonInput value={userInfos.self_introduction}></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel>Education level</IonLabel>
            <IonSelect value={userInfos.education_level} placeholder="Select">
              <IonSelectOption value="Bachelor">Bachelor</IonSelectOption>
              <IonSelectOption value="Master">Master</IonSelectOption>
              <IonSelectOption value="Doctor">Doctor</IonSelectOption>
            </IonSelect>
          </IonItem>
          <IonItem>
            <IonLabel>Study Mode</IonLabel>
            <IonSelect value={userInfos.mode_of_study} placeholder="Select">
              <IonSelectOption value="Full-time">Full time</IonSelectOption>
              <IonSelectOption value="Part-time">Part time</IonSelectOption>
            </IonSelect>
          </IonItem>
          <IonItem>
            <IonLabel>Year</IonLabel>
            <IonSelect value={userInfos.year} placeholder="Select">
              <IonSelectOption value="1">1</IonSelectOption>
              <IonSelectOption value="2">2</IonSelectOption>
              <IonSelectOption value="3">3</IonSelectOption>
              <IonSelectOption value="4">4</IonSelectOption>
              <IonSelectOption value="5">5</IonSelectOption>
              <IonSelectOption value="6">6</IonSelectOption>
              <IonSelectOption value="graduated">Graduated</IonSelectOption>
            </IonSelect>
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Faculty</IonLabel>
            <IonInput
              type="text"
              value={userInfos.faculty ? userInfos.faculty : "N/A"}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Major</IonLabel>
            <IonInput type="text" value={userInfos.major}></IonInput>
          </IonItem>
        </IonList>
      </IonContent>
      <IonFooter>
        <IonButton expand="block" color="medium">
          Submit
        </IonButton>
      </IonFooter>
    </IonPage>
  );
};

export default ProfileUpdateForm;
