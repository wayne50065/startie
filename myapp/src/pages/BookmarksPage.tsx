import React, { useState } from "react";
import { IonButton, IonPage, IonHeader, IonAvatar, IonContent, IonCard, IonRow, IonCol, IonCardSubtitle, IonIcon, IonGrid, IonButtons, IonCardTitle, IonItem, useIonAlert } from "@ionic/react";
import HeaderBar from "../components/HeaderBar";
import { trash } from "ionicons/icons";
import icon from "../temp_local_img/sj.png";
import { get, getAPIServer, patch } from "../helpers/api";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { useLocation } from "react-router";

export type bookmark = {
  id: number
  ideaName: string
  project_id: number
}

export function Bookmarks() {
  const [bookmark, setBookmark] = useState<bookmark[]>([])
  const [error, setError] = useState('')

  // let user_id = useSelector((state: RootState) => state.auth.payload?.id)

  async function loadBookmark() {
    try {
      let json = await get(`/bookmark`)
      if (json.error) {
        setError(json.error)
      } else {
        setBookmark(json.bookmarkList)
        setError('')
      }
    } catch (error) {
      setError((error as Error).toString())
    }
  }

  async function onRemoved(id: number) {
    let json = await patch(`/bookmark/${id}`, {})
    if (json.error) {
      setError(json.error)
    } else {
      setError('')
      // setBookmark(bookmark.filter(list => list.id !== id))
      loadBookmark()
    }
  }


  const location = useLocation()

  useEffect(() => {
    if(location.pathname==='/bookmarks') {
      loadBookmark()
    }
  }, [location.pathname])

  if (!bookmark) {
    return (
      <div>
        <p>404: Bookmark not found!</p>
      </div>
    )
  }

  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="Bookmarks" backBtn={true} backLocation="/more" />
      </IonHeader>

      <IonContent>


        {bookmark.map(mark =>
          <IonCard key={mark.id} className="bookmarkCard">
            <IonGrid>
              <IonRow>
                {/* <IonCol size="3">
                <IonCardSubtitle>NOV 1</IonCardSubtitle>
                <IonCardSubtitle>Fri</IonCardSubtitle>
              </IonCol> */}
                <IonItem routerLink={"/idea/" + mark.project_id}>
                  <IonCol size="12">
                    <h3>{mark.ideaName}</h3>
                    {/* <p>
                  This is content, without any paragraph or header tags, within an
                  ion-cardContent element.
                </p> */}
                    {/* <IonAvatar>
                  <img src={icon} />
                </IonAvatar> */}
                  </IonCol>
                </IonItem>
                <IonCol size="12">
                  <div className="ion-text-end">
                    {/* <IonButtons> */}
                    <IonButton size="small" color="danger" onClick={() => onRemoved(mark.id)}>
                      <IonIcon icon={trash} />
                    </IonButton>
                    {/* </IonButtons> */}
                  </div>
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonCard>
        )}
      </IonContent>
    </IonPage>
  )
};

export default Bookmarks;
