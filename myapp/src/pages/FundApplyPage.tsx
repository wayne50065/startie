import React, { useState } from 'react';
import { IonFooter, IonButton,IonContent, IonHeader, IonPage, IonItem, IonLabel, IonList, IonTextarea, IonText, useIonToast } from '@ionic/react';
import HeaderBar from "../components/HeaderBar";
import { useSelector } from 'react-redux';
import { RootState } from '../redux/state';
import { getAPIServer } from '../helpers/api';
import { useParams } from 'react-router';


export const FundApplyForm: React.FC = () => {
    const origin = getAPIServer();
    const [info, setInfo] = useState<string>('');
    const [file, setFile] = useState<File>();
  const [present, dismiss] = useIonToast();

    // <File>
    
    const [submitSuccess, setSubmitSuccess] = useState(false)
    const [error, setError] = useState('')

    const params = useParams<{ fund_id: string }>()
    const fund_id = parseInt(params.fund_id)
    

    let token = useSelector((state: RootState) => state.auth.token)

    async function onSubmit() {
        let formData = new FormData()

        formData.append('info',info)
        if(file){
            formData.append('file',file,file.name)
        }
        console.log(fund_id)
        let res = await fetch(`${origin}/fund/${fund_id}/apply`, {
            method: "POST",
            headers: {
                'Authorization': 'Bearer ' + token
            },
            body: formData,
        })


        let json = await res.json()
        if (json.error){
            setError(json.error)
        } else {
            setError('')
            setSubmitSuccess(true)
            present({
                buttons: [{ text: 'close', handler: () => {window.location.href=`/fund-details/${fund_id}`} }],
                position:"middle",
                duration:2000,
                color:"dark",
                message: 'Upload Success!  Admin will contact you soon.',
              })
              setTimeout(()=>{window.location.href=`/fund-details/${fund_id}`},2000)
        }

    }

    return (
        <IonPage>
            <IonHeader>
                <HeaderBar title="Fund Application" backBtn={true} backLocation={`/fund-details/${fund_id}`}/>
            </IonHeader>
            <IonContent>
                <IonList>
                    <IonItem>
                        <IonLabel position="floating" >Apply message</IonLabel>
                        <IonTextarea value={info} onIonChange={(e) => setInfo(e.detail.value!)}></IonTextarea>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">File(.pdf)</IonLabel>
                        <input type='file' onChange={e => e.target.files?setFile(e.target.files[0]):null}></input>
                    </IonItem>
                        <IonButton expand="block" className="ion-margin-top"onClick={async ()=>{await onSubmit()}}>
                            Submit
                        </IonButton>
                        
                </IonList>

            </IonContent>

            <IonFooter>

            </IonFooter>
        </IonPage>
    );
};

export default FundApplyForm;