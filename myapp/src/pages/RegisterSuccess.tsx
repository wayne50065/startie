import React, { useEffect, useState } from "react";
import { IonFooter, IonContent, IonHeader, IonPage } from "@ionic/react";
import HeaderBar from "../components/HeaderBar";
import { getAPIServer } from "../helpers/api";
import { useParams } from "react-router";
import { useDispatch, useSelector} from "react-redux";
import { loginFailed} from "../redux/auth/action";

import { checkTokenThunk, handleTokenThunk } from "../redux/auth/thunk";
import { RootState } from "../redux/state";

export const RegisterSuccess: React.FC = () => {
  const params = useParams<{ OTP: string }>();
  const emailOTP = params.OTP;
  const dispatch = useDispatch();
  let origin = getAPIServer();
  const authState = useSelector((state: RootState) => state.auth);
  let payload = authState.payload;

  useEffect(() => {
    RegisterOTP();
    dispatch(checkTokenThunk());
  }, []);

  async function RegisterOTP() {
    console.log(`OTP => ${emailOTP}`);
    try {
      let res = await fetch(`${origin}/register/${emailOTP}`);
      if (res.status === 200) {
        let token = (await res.json()).token;
        dispatch(handleTokenThunk(token));
        setTimeout(() => {
          window.location.href = "/";
        }, 3000);
        return;
      } else {
        dispatch(loginFailed(await res.json()));
        window.location.href = "/";
        return;
      }
    } catch (e) {
      throw new Error("cannot load requested page.");
    }
  }

  return (
    <IonPage>
      <IonHeader>
        <HeaderBar
          title="RegisterSuccess"
          backBtn={true}
          backLocation="/register"
        />
      </IonHeader>
      <IonContent>
        <p>{ payload? "Email activated! Redirecting..." : "Unauthorized!"}</p>
      </IonContent>
      <IonFooter></IonFooter>
    </IonPage>
  );
};

export default RegisterSuccess;
