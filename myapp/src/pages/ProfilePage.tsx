import {
  IonAvatar,
  IonBadge,
  IonButton,
  IonCard,
  IonContent,
  IonFooter,
  IonHeader,
  IonImg,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonSegment,
  IonSegmentButton,
  IonText,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import HeaderBar from "../components/HeaderBar";
import "./ProfilePage.css";
import { useDispatch, useSelector } from "react-redux";
import { checkTokenThunk } from "../redux/auth/thunk";
import { getAPIServer } from "../helpers/api";
import { UserInfo } from "../redux/user/state";
import { saveUserInfo } from "../redux/user/action";
import { ProjectCards, ProjectsCards } from "../components/ProfileCards";
import { useParams } from "react-router";
import { RootState } from "../redux/state";

const ProfilePage: React.FC = () => {
  const origin = getAPIServer();
  const token = localStorage.getItem("token");
  const authStateItems = useSelector((state: RootState) => state.auth);
  let payload = authStateItems.payload;
  const params = useParams<{ id: string }>();
  const user_id = parseInt(params.id);
  const dispatch = useDispatch();
  useEffect(() => {
    if(!user_id) return
    dispatch(checkTokenThunk());
    loadUserInfo();
    getCreatedProjectDetails();
    getJoinedProjectDetails();
    getGoingProjectDetails();
  }, [params]);
  const [projectType, setProjectType] = useState("joined");
  const [userInfos, setUserInfos] = useState<UserInfo>();
  const [created, setCreated] = useState<ProjectsCards[]>([]);
  const [joined, setJoined] = useState<ProjectsCards[]>([]);
  const [going, setGoing] = useState<ProjectsCards[]>([]);

  async function loadUserInfo() {
    if (!token) {
      console.log("cannot get token");
      return;
    }
    try {
      console.log("Getting user ID : ", user_id, "@FrontEnd.");
      const res = await fetch(`${origin}/getUser/${user_id}`, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      });
      if (res.status === 200) {
        console.log("Successfully get user info from DB");
        const resultSQL = await res.json();
        dispatch(saveUserInfo(resultSQL.userInfos));

        setUserInfos(resultSQL.userInfos);
        console.log("userInfos :", resultSQL.userInfos);
        return;
      }
    } catch (e) {
      console.log("Fail to fetch /getUserInfo @userThunk");
      return;
    }
  }
  async function getCreatedProjectDetails() {
    if (!origin) {
      console.log("cannot get origin");
      return;
    }
    if (!token) {
      console.log("cannot get token");
      return;
    }

    try {
      const res = await fetch(`${origin}/profile/created/${user_id}`, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      });
      console.log("calling getCreatedProjectDetails @react");
      if (res.status === 200) {
        const result = await res.json();
        setCreated(result);
        return;
      }
    } catch (error) {
      console.log(`fail to fetch @getCreatedProjectDetails ${error}`);
      return;
    }
  }
  async function getJoinedProjectDetails() {
    if (!origin) {
      console.log("cannot get origin");
      return;
    }
    if (!token) {
      console.log("cannot get token");
      return;
    }

    try {
      const res = await fetch(`${origin}/profile/joined/${user_id}`, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      });
      console.log("calling getJoinedProjectDetails @react");
      if (res.status === 200) {
        const result = await res.json();
        setJoined(result);
        return;
      }
    } catch (error) {
      console.log(`fail to fetch @getJoinedProjectDetails ${error}`);
      return;
    }
  }
  async function getGoingProjectDetails() {
    if (!origin) {
      console.log("cannot get origin");
      return;
    }
    if (!token) {
      console.log("cannot get token");
      return;
    }

    try {
      const res = await fetch(`${origin}/profile/going/${user_id}`, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      });
      console.log("calling getGoingProjectDetails @react");
      if (res.status === 200) {
        const result = await res.json();
        setGoing(result);
        return;
      }
    } catch (error) {
      console.log(`fail to fetch @getGoingProjectDetails ${error}`);
      return;
    }
  }

  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="Profile" backBtn={true} backLocation="/more" />
      </IonHeader>
      <IonContent className="profile_detail_container">
      
        <IonCard className="userInfoContainer">
          <IonItem lines="none" className="ion-align-items-start icon-container" color="light">
              <IonAvatar className="ion-margin-vertical">
              <IonImg
                src={`${getAPIServer()}/uploads/${
                  userInfos?.personal_image_url
                }`}
              />
              </IonAvatar>
            
            
            {(user_id == payload?.id)? (
              <IonButton slot="end" className="ion-padding-vertical" href={`/profile-update/${payload?.id}`}>
                edit profile
              </IonButton>
            ) : null}
            <IonLabel color="dark" slot="end">{userInfos?.name}</IonLabel>
          </IonItem>
          <IonItem lines="none">
            <IonBadge color="danger" slot="end">
              {userInfos?.is_admin === true ? "Admin" : null}
            </IonBadge>
          </IonItem>
            
          
          <IonItem lines="none">
          {/* <IonText>{userInfos?.self_introduction}</IonText> */}
          <IonText className="detail-box">Clickable items have a few visual differences that indicate they can be interacted with. For example, a clickable item receives the ripple effect upon activation in md mode, has a highlight when activated in ios mode</IonText>
          </IonItem >

          <IonList className="ion-padding">
            {userInfos?.skill.map((talent)=>{return(<IonBadge className="margin-bottom-5px">{talent}</IonBadge>)})}
          </IonList>
          
        </IonCard>

        <h3 className="projects-title ion-text-center">Projects</h3>
        <IonSegment
          value={projectType}
          onIonChange={(e) => {
            if (e.detail.value) {
              setProjectType(e.detail.value.toString());
            }
          }}
        >
          <IonSegmentButton value="joined">
            <IonLabel>Joined</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="created">
            <IonLabel>Created</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="going">
            <IonLabel>Applied</IonLabel>
          </IonSegmentButton>
        </IonSegment>

        <div className="ion-text-center">
          {projectType === "created" ? (
            created.length > 0 ? (
              <ProjectCards details={created} />
            ) : (
              "No created project."
            )
          ) : projectType === "joined" ? (
            joined.length > 0 ? (
              <ProjectCards details={joined} />
            ) : (
              "No joined project."
            )
          ) : going.length > 0 ? (
            <ProjectCards details={going} />
          ) : (
            "No applied project."
          )}
        </div>
      </IonContent>
      <IonFooter></IonFooter>
    </IonPage>
  );
};

export default ProfilePage;
