import {
  IonAvatar,
  IonBadge,
  IonButton,
  IonCard,
  IonContent,
  IonHeader,
  IonImg,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonText,
  useIonToast,
  useIonViewWillEnter,
} from "@ionic/react";
import React, { useState } from "react";

import HeaderBar from "../components/HeaderBar";
import { getAPIServer } from "../helpers/api";
import './PeoplePage.css'

export type PeopleCards = {
  id: number;
  name: string;
  personal_image_url: string;
  self_introduction: string;
  skill: string[];
};
export const People: React.FC = () => {
  let origin = getAPIServer();
  const [cards, setCards] = useState<PeopleCards[]>([]);
  const [present, dismiss] = useIonToast();

  const token=localStorage.getItem("token")
  async function loadCVs() {
    let token = localStorage.getItem("token");
    if (!token) {
      console.log("cannot get token");
      return;
    }
    try {
      const res = await fetch(`${origin}/getAllUserInfo`, {
        method: "GET",
        headers: {
          "content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      });
      if (res.status === 200) {
        const result = await res.json();
        const max = cards.length + 10;
        const min = max - 10;
        const newCards: PeopleCards[] = [];
        if(max>result.length){
          return
        }
        for (let i = min; i < max; i++) {
          if(i>result.length){
            break
          }
          newCards.push(result[i]);
          setCards([...cards, ...newCards]);
        }
      }
    } catch (error) {
      console.log(`Failed to fetch => /loadCVs ${error}`);
    }
  }
  const messageBox=()=>{
   if(!token){
     return (present({
      buttons: [{ text: 'close', handler: () => {window.location.href="/login"} }],
      position:"middle",
      color:"danger",
      duration:500,
      message: `Login to check People`,
      animated:true,
    }))
   }
      
  }
  const loadData = (ev: any) => {
    setTimeout(() => {
      loadCVs();
      ev.target.complete();
    }, 500);
  };
  
  useIonViewWillEnter(() => {
    loadCVs();
    messageBox();
    
  });
  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="People" search={false} />
      </IonHeader>
      <IonContent fullscreen>
        <IonList>
          
          {cards.map((card) => {
            return (
              <IonCard
                key={card.id}
                button={true}
                className="peopleCards-container"
                routerLink={"/profile/" + card.id}
              >
                <IonItem lines="none" color="light">
                  <IonAvatar slot="start"  className="ion-margin-top">
                    <IonImg
                      src={`${origin}/uploads/${card.personal_image_url}`}
                    />
                  </IonAvatar>
                  <IonLabel className="userName" color="dark">
                    <h2>{card.name}</h2>
                  </IonLabel>
                </IonItem>
                
                {/* <IonItem lines="none" className="ion-margin-top">{card.self_introduction.length>80?card.self_introduction.slice(0,80)+" ...":card.self_introduction}</IonItem> */}
                <IonList className="ion-padding">

                  {card.skill.map((skill) => {
                    return <IonBadge className="talent-tag margin-bottom-5px">{skill}</IonBadge>;
                  })}
                </IonList>
              </IonCard>
            );
          })}
        </IonList>


        <IonInfiniteScroll onIonInfinite={loadData} threshold="100px">
          <IonInfiniteScrollContent
            loadingSpinner="bubbles"
            loadingText="Loading more data..."
          ></IonInfiniteScrollContent>
        </IonInfiniteScroll>
      </IonContent>
    </IonPage>
  );
};

export default People;
