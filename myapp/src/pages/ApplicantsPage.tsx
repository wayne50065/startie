import React, { useEffect, useState } from "react";
import {
  IonButton,
  IonPage,
  IonHeader,
  IonItem,
  IonLabel,
  IonAvatar,
  IonContent,
  IonCard,
  IonRow,
  IonCol,
  IonGrid,

  IonBadge,
} from "@ionic/react";
import HeaderBar from "../components/HeaderBar";
import { useParams } from "react-router";
import { get, getAPIServer, patch } from "../helpers/api";

export type applicant = {
  id: number;
  userId: number;
  userName: string;
  userPic: string;
  detail: string;
  applied_at: Date;
  has_approved: boolean;
  has_rejected: boolean;
};

export function ApplicantList() {
  const [applicant, setApplicant] = useState<applicant[]>([]);
  const [error, setError] = useState("");
  // const [accept, setAccept] = useState(false)
  // const [reject, setReject] = useState(false)
  // const hasApproved = applicant?.map(ap => ap.has_approved)
  // const hasRejected = applicant?.map(ap => ap.has_rejected)
  const params = useParams<{ idea_id: string }>();
  const idea_id = parseInt(params.idea_id);
  let numbers = 0;

  async function loadApplicant() {
    try {
      let json = await get(`/idea/${idea_id}/apply`);
      if (json.error) {
        setError(json.error);
      } else {
        setApplicant(json.ideaParticipant);
        setError("");
      }
    } catch (error) {
      setError((error as Error).toString());
    }
  }

  async function onAccepted(id: number) {
    let json = await patch(`/idea/approveApply/${id}`, {});
    if (json.error) {
      setError(json.error);
    } else {
      setError("");
      loadApplicant();
    }
  }

  async function onRejected(id: number) {
    let json = await patch(`/idea/rejectApply/${id}`, {});
    if (json.error) {
      setError(json.error);
    } else {
      setError("");
      loadApplicant();
    }
  }

  useEffect(() => {
    loadApplicant();
  }, [idea_id]);

  if (!applicant) {
    return (
      <div>
        <p>404: Applicant not found!</p>
      </div>
    );
  }

  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="Applicants" />
      </IonHeader>

      <IonContent>
        {applicant.length > 0 ? null : (
          <p className="ion-text-center">No applicants</p>
        )}
        {applicant.map((a) => (
          <IonCard key={a.id} className="projectCard">
            <IonGrid>
              <IonRow>
                {/* <IonCol size="3">
                  <IonCardSubtitle>
                    {new Date(a.applied_at).toLocaleDateString('en-GB', { day: 'numeric', month: 'short', year: 'numeric' })}
                  </IonCardSubtitle>
                </IonCol> */}
                <IonCol>
                  <IonItem
                    className="ion-no-padding"
                    routerLink={"/profile/" + a.userId}
                  >
                    <IonAvatar slot="start">
                      <img src={`${getAPIServer()}/uploads/${a.userPic}`} />
                    </IonAvatar>
                    <IonLabel>{a.userName}</IonLabel>
                  </IonItem>
                  <p>{a.detail}</p>
                  {a.has_approved ? (
                    <IonBadge>Approved</IonBadge>
                  ) : a.has_rejected ? (
                    <IonBadge>Rejected</IonBadge>
                  ) : (
                    <div className="ion-text-end">
                      <IonButton
                        size="small"
                        color="success"
                        onClick={() => onAccepted(a.id)}
                      >
                        Accept
                      </IonButton>
                      <IonButton
                        size="small"
                        color="danger"
                        onClick={() => onRejected(a.id)}
                      >
                        Reject
                      </IonButton>
                    </div>
                  )}
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonCard>
        ))}
      </IonContent>
    </IonPage>
  );
}

export default ApplicantList;
