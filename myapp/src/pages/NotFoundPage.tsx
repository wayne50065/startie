import { useLocation } from "react-router"

const NotFoundPage = () => {
    const location = useLocation()
    const pathname = location.pathname
    return (
        <div>
            <h2>Page Not Found(<code>{pathname}</code></h2>
        </div>
    )
}

export default NotFoundPage