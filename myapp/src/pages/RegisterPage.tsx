import React, { useState } from "react";
import {
  IonFooter,
  IonButton,
  IonContent,
  IonHeader,
  IonPage,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  useIonToast,
} from "@ionic/react";
import HeaderBar from "../components/HeaderBar";
import { getAPIServer } from "../helpers/api";

export const RegisterForm: React.FC = () => {
  const [userEmail, setUserEmail] = useState("");
  const [userName, setUserName] = useState("");
  const [present, dismiss] = useIonToast();




  async function register(registerEmail: string, userName: string) {
      const origin = getAPIServer()
      let ip=window.location.origin
    try{
      const res=await fetch(`${origin}/register/getOTP`,{
        method:"POST",
        headers:{
          'content-type':'application/json'
        },
        body:JSON.stringify({registerEmail,userName,ip})
      })
      const result = await res.json();
      if (res.status===200){
        present({
          buttons: [{ text: 'close' }],
          position:"bottom",
          duration:1000,
          color:"dark",
          message: 'OTP has sent!',
        })
      }else {
        present({
          buttons: [{ text: 'close' }],
          position:"bottom",
          duration:1000,
          color:"danger",
          message: `${result.error}`,
        })

      }
      console.log(result.error)
    }catch(e){
      console.log((e as Error).toString());
      return 
    }
  }



  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="Register" backBtn={true} backLocation="/login" />
      </IonHeader>
      <IonContent>
        <IonList>
          <IonItem>
            <IonLabel position="floating">Email address</IonLabel>
            <IonInput
              type="email"
              required
              onIonChange={(e) => {
                setUserEmail(e.detail.value!);
              }}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Name</IonLabel>
            <IonInput
              type="text"
              required
              onIonChange={(e) => {
                setUserName(e.detail.value!);
              }}
            ></IonInput>
          </IonItem>
        </IonList>
      </IonContent>
      <IonFooter>
        <IonButton
          expand="block"
          color="medium"
          onClick={async() => {
            await register(userEmail, userName);
            
          }}
        >
          Submit
        </IonButton>
      </IonFooter>
    </IonPage>
  );
};

export default RegisterForm;
