import React, { useEffect, useState } from 'react';
import { IonFooter, IonButton, IonDatetime, IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonInput, IonItem, IonLabel, IonList, IonItemDivider, IonIcon, IonButtons, IonThumbnail, IonAvatar, IonImg } from '@ionic/react';
import HeaderBar from "../components/HeaderBar";
import { imageSharp, imagesSharp, trashBinOutline, trashOutline } from 'ionicons/icons'
import './IdeaUploadPage.css'
import { compressMobilePhoto, dataURItoBlob, dataURItoMimeType } from '@beenotung/tslib/image'
import { selectImage, } from '@beenotung/tslib/file'
import { get, getAPIServer } from '../helpers/api';
import { useParams } from 'react-router';
import { useInput } from '../hooks/use-input';



export type Idea = {
    title: string
    category: string
    detail: string
    project_deadline: Date
    talentNeeded: string[]
    criteria: string
    youtube_url: string
    image: string
    chat_room_url: string
    competition_name: string
    competition_url: string
    competition_date: number | string | Date
  }


export function EditIdea() {
    const [state, setState, { input, textarea, photos, datetime, upload }] = useInput()
    const [error, setError] = useState('')

    const params = useParams<{ idea_id: string }>()
    const idea_id = parseInt(params.idea_id)

    async function getIdea() {
        try {
            let json = await get(`/idea/${idea_id}`)
            console.log('get idea:', json)
            if (json.error) {
                setError(json.error)
            } else {
                setState(json.ideaDetail)
                setError('')
            }
        } catch (error) {
            setError((error as Error).toString())
        }
    }

    useEffect(() => {
        getIdea()
    }, [idea_id])

    const editIdea = () => upload('PATCH', `/idea/${idea_id}`)

    console.log('edit idea render:', state)

    return (
        <IonPage className='IdeaUploadPage'>
            <IonHeader>
                <HeaderBar title="Update Your Ideas" />
            </IonHeader>
            <IonContent>
                <IonList>
                {input('title')}
                {input('category')}
                {textarea('detail')}
                {datetime('project_deadline')}
                {input('talent_needed')}
                {input('criteria (Optional)')}
                {input('youtube_url')}
                {photos('images (Optional)')}
                {input('chat_room_url')}
                {input('Competition_name (If any)')}
                {input('Competition_url (If any)')}
                {datetime('Competition_date (If any)')}
                    <IonButton expand='block' color="medium" onClick={editIdea}>
                        Submit
                    </IonButton>
                </IonList>
            </IonContent>
        </IonPage>
    )
}


export default EditIdea;
