import React, { useState } from 'react';
import { IonButton, IonContent, IonHeader, IonPage,IonItem,IonList,IonIcon, IonButtons,IonListHeader,IonChip, useIonToast, useIonRouter } from '@ionic/react';
import HeaderBar from "../components/HeaderBar";
import { addOutline, closeOutline} from 'ionicons/icons'
import './IdeaUploadPage.css'
import { useInput } from '../hooks/use-input';
import { useGet } from '../hooks/use-get';
import ErrorMessage from '../components/ErrorMessage';
import { useArray } from '../hooks/use-array';

interface Talent { id: number, skill: string }
interface Category { id: number, category_name: string }
interface SearchTalentListResult {
    error?: string
    talent_list?: Talent[]
}
interface SearchCategoryListResult {
    error?: string
    category_list?: Category[]
}

export function IdeaUploadForm() {
    const [present, dismiss] = useIonToast();
    const [state, setState, { input, textarea, photos, datetime, upload }] = useInput({
        talent_needed_id_list: [],
        talent_needed: '',
        category_id_list: [],
        category: ''
    })

    const [selectedTalentList, selectedTalentListControl] = useArray<Talent>([])
    const [newTalentList, newTalentListControl] = useArray<string>([])


    const searchTalentParams = new URLSearchParams()
    searchTalentParams.set('q', state.talent_needed)
    const [searchTalentResult] = useGet<SearchTalentListResult>('/talent/search?' + searchTalentParams, {})

    const searchCategoryParams = new URLSearchParams()
    searchCategoryParams.set('q', state.category)
    const [searchCategoryResult] = useGet<SearchCategoryListResult>('/category/search?' + searchCategoryParams, {})

    const router = useIonRouter()
    const uploadIdea = async () => {
        await upload('POST', '/idea', {
            talent_needed_id_list: selectedTalentList.map(row => row.id),
            new_talent_needed_list: newTalentList
        })
        setState({
            talent_needed_id_list: [],
            talent_needed: '',
            category_id_list: [],
            category: ''
        })
        selectedTalentListControl.reset()
        newTalentListControl.reset()
        router.push('/')
    }

    return <IonPage className='IdeaUploadPage'>
        <IonHeader>
            <HeaderBar title="Upload Your Ideas" />
        </IonHeader>
        <IonContent>

            <IonList>
                {input('title')}

                {input('category')}
                <IonListHeader hidden={!state.category}>
                    Category Suggestion
                </IonListHeader>
                <IonList hidden={!state.category} >
                    <ErrorMessage message={searchCategoryResult.error} />
                    {searchCategoryResult.category_list
                        ?.map(cat =>
                            <IonItem key={cat.id} onClick={() =>
                                setState({
                                    ...state,
                                    category: cat.category_name,
                                })}>
                                {cat.category_name}
                            </IonItem>
                        )}
                </IonList>

                {textarea('detail')}
                {datetime('project_deadline')}


                {input('talent_needed', <IonButtons slot='end' >
                    <IonButton hidden={!state.talent_needed} onClick={() => {
                        newTalentListControl.push(state.talent_needed)
                        setState({ ...state, talent_needed: '' })
                    }}>
                        <IonIcon icon={addOutline}></IonIcon>
                    </IonButton>
                </IonButtons>)}
                <IonListHeader >
                    Selected Talent
                </IonListHeader>
                {selectedTalentList.map(talent =>
                    <IonChip key={talent.id} onClick={() => {
                        selectedTalentListControl.remove(talent)
                    }}>
                        {talent.skill}
                        <IonIcon icon={closeOutline}>
                        </IonIcon>
                    </IonChip>)}
                {newTalentList.map(skill =>
                    <IonChip key={skill} onClick={() => {
                        newTalentListControl.remove(skill)
                    }}>
                        {skill}
                        <IonIcon icon={closeOutline}>
                        </IonIcon>
                    </IonChip>)}
                <IonListHeader hidden={!state.talent_needed}>
                    Talent Suggestion
                </IonListHeader>
                <IonList hidden={!state.talent_needed} >
                    <ErrorMessage message={searchTalentResult.error} />
                    {searchTalentResult.talent_list
                        ?.filter(talent => !selectedTalentList.find(x => x.id === talent.id))
                        .map(talent =>
                            <IonItem key={talent.id} onClick={() => {
                                selectedTalentListControl.push(talent)
                                setState({ ...state, talent_needed: '' })
                            }}>
                                {talent.skill}
                            </IonItem>
                        )}
                </IonList>


                {input('criteria (Optional)')}
                {input('youtube_url')}
                {photos('images (Optional)')}
                {input('chat_room_url')}
                {input('competition_name (If any)')}
                {input('competition_url (If any)')}
                {datetime('competition_date (If any)')}

                <IonButton expand='block' color="dark" onClick={async () => {
                    await uploadIdea()
                    present('Idea submitted.', 2000)
                }}>
                    Submit
                </IonButton>
            </IonList>
        </IonContent>
    </IonPage>
}


export default IdeaUploadForm;

