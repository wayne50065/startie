import React, { useEffect } from "react";
import {
  IonPage,
  IonHeader,
  IonList,
  IonItem,
  IonLabel,
  IonAvatar,
  IonContent,
  IonImg,
  IonButton,
  IonIcon,
  IonButtons,
  IonSegment,
  IonSegmentButton,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
  useIonViewWillEnter,
} from "@ionic/react";
import HeaderBar from "../components/HeaderBar";
import { useState } from "react";
import { get, getAPIServer } from "../helpers/api";
import { refreshOutline } from "ionicons/icons";
import competitionPic from "../temp_local_img/Angel_fund.jpeg";
import ErrorMessage from "../components/ErrorMessage";

export type Fund = {
  id: number;
  title: string;
  file_url: string;
  short_desc: string;
};

export function AngelFund() {
  const [currentFundList, setCurrentFundList] = useState<Fund[]>([]);
  const [pastFundList, setPastFundList] = useState<Fund[]>([]);
  const [fundPage, setFundPage] = useState("current");

  const [error, setError] = useState("");

  async function loadCurrentFundList() {
    try {
      let json = await get(`/fund/list/current`);
      if (json.error) {
        setError(json.error);
      } else {
        // setCurrentFundList(json.currentFundList);
        setError("");
        const max = currentFundList.length + 15;
        const min = max - 15;
        const newCurrentFundList: Fund[] = [];
        if (max > json.currentFundList.length) {
          return;
        }
        for (let i = min; i < max; i++) {
          if (i > json.currentFundList.length) {
            break;
          }
          newCurrentFundList.push(json.currentFundList[i]);
          setCurrentFundList([...currentFundList, ...newCurrentFundList]);
        }
      }
    } catch (error) {
      setError((error as Error).toString());
    }
  }

  async function loadPastFundList() {
    try {
      let json = await get(`/fund/list/past`);
      if (json.error) {
        setError(json.error);
      } else {
        // setPastFundList(json.pastFundList);
        setError("");
        const max = pastFundList.length + 10;
        const min = max - 10;
        const newPastFundList: Fund[] = [];
        if (max > json.pastFundList.length) {
          return;
        }
        for (let i = min; i < max; i++) {
          if (i > json.pastFundList.length) {
            break;
          }
          newPastFundList.push(json.pastFundList[i]);
          setPastFundList([...pastFundList, ...newPastFundList]);
        }
      }
    } catch (error) {
      setError((error as Error).toString());
    }
  }

  // useEffect(() => {
  //   loadCurrentFundList()
  //   loadPastFundList()
  // }, []);
  const loadData = (ev: any) => {
    setTimeout(() => {
      loadCurrentFundList();
      loadPastFundList();
      ev.target.complete();
    }, 500);
  };

  useIonViewWillEnter(() => {
    loadCurrentFundList();
    loadPastFundList();
  });
  // console.log({ fundItemList })
  return (
    <IonPage>
      <IonHeader>
        <HeaderBar
          title="Angel Fund"
          // afterTitle={
          //   <IonButtons slot="end">
          //     <IonButton onClick={loadCurrentFundList}>
          //       <IonIcon icon={refreshOutline}></IonIcon>
          //     </IonButton>
          //   </IonButtons>
          // }
        />
      </IonHeader>
      <IonContent>
      <IonImg src={competitionPic} />
        <IonSegment
          value={fundPage}
          onIonChange={(e) => {
            if (e.detail.value) setFundPage(e.detail.value.toString());
          }}
        >
          <IonSegmentButton value="current">Current</IonSegmentButton>
          <IonSegmentButton value="past">Past</IonSegmentButton>
        </IonSegment>

        <ErrorMessage message={error} />
        {fundPage === "current" ? (
          <IonList>
            {currentFundList.map((fund) => (
              <IonItem key={fund.id} routerLink={"/fund-details/" + fund.id}>
                <IonAvatar slot="start">
                  <IonImg src={`${getAPIServer()}/uploads/${fund.file_url}`} />
                </IonAvatar>
                <IonLabel>
                  <h2>{fund.title}</h2>
                  <p>{fund.short_desc}</p>
                </IonLabel>
              </IonItem>
            ))}
          </IonList>
        ) : (
          <IonList>
            {pastFundList.map((fund) => (
              <IonItem key={fund.id} routerLink={"/fund-details/" + fund.id}>
                <IonAvatar slot="start">
                  <IonImg src={`${getAPIServer()}/uploads/${fund.file_url}`} />
                </IonAvatar>
                <IonLabel>
                  <h2>{fund.title}</h2>
                  <p>{fund.short_desc}</p>
                </IonLabel>
              </IonItem>
            ))}
          </IonList>
        )}
        <IonInfiniteScroll onIonInfinite={loadData} threshold="100px">
          <IonInfiniteScrollContent
            loadingSpinner="bubbles"
            loadingText="Loading more data..."
          ></IonInfiniteScrollContent>
        </IonInfiniteScroll>
      </IonContent>
    </IonPage>
  );
}

export default AngelFund;
