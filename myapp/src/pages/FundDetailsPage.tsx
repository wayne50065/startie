import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonImg,
  IonItem,
  IonPage,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import HeaderBar from "../components/HeaderBar";
import { get, getAPIServer } from "../helpers/api";

export type Fund = {
  id: number;
  title: string;
  file_url: string;
  detail: string;
  application_deadline: Date;
  isValid: boolean;
};

export const FundDetails: React.FC = () => {
  let origin= getAPIServer()
  const [fundDetail, setFundDetail] = useState<Fund>();
  const [error, setError] = useState("");
  const [applied, setApplied]= useState<boolean>(false);
  const token = localStorage.getItem("token");
  const params = useParams<{ fund_id: string }>();
  const fund_id = parseInt(params.fund_id);
  console.log({fund_id})
  const today = new Date(Date.now());

  async function loadFundDetail() {
    try {
      let json = await get(`/fund/${fund_id}`);
      if (json.error) {
        setError(json.error);
      } else {
        setFundDetail(json.fundDetail);
        setError("");
      }
    } catch (error) {
      setError((error as Error).toString());
    }
    try{
      let res = await fetch(`${origin}/fundApplied/${fund_id}`,{
        method: "GET",
            headers: {
                'Authorization': 'Bearer ' + token
            },
      });
      const result=await res.json()
      
      
      if(res){
        setApplied(result)
      }
      

    }catch(error){
      setError((error as Error).toString());
    }
  }

  useEffect(() => {
    loadFundDetail();
  }, [fund_id]);

  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="Fund" backBtn={true} backLocation="/angel-fund" />
      </IonHeader>

      <IonContent>
        {!fundDetail ? (
          <p>404: Fund not found!</p>
        ) : (
          <IonCard>
            {/* {JSON.stringify(fundDetail)} */}
            <IonImg src={`${getAPIServer()}/uploads/${fundDetail.file_url}`} />
            <IonCardHeader>
              <IonCardTitle>{fundDetail.title}</IonCardTitle>
            </IonCardHeader>
            <IonCardContent>
              <p>{fundDetail.detail}</p>
              <hr />
              <p>Application Deadline:</p>
              {new Date(fundDetail.application_deadline).toLocaleDateString(
                "en-GB",
                { day: "numeric", month: "short", year: "numeric" }
              )}
              {fundDetail.isValid ? (
                <IonItem lines="none">
                  <IonButton
                    disabled={applied}
                    slot="end"
                    size="default"
                    color="secondary"
                    routerLink={token ? "/fund-application/" + fund_id :"/login"}
                  >
                    {applied?"You have applied":"Apply"}
                    
                  </IonButton>
                </IonItem>
              ) : null}
            </IonCardContent>
          </IonCard>
        )}
        <p>
        {applied}
        </p>
      </IonContent>
    </IonPage>
  );
};

console.log('test fund apply')

export default FundDetails;
