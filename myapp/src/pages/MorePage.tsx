import React, { useEffect } from "react";
import {
  IonPage,
  IonHeader,
  IonList,
  IonItem,
  IonLabel,
  IonContent,
  IonIcon,
} from "@ionic/react";
import HeaderBar from "../components/HeaderBar";
import {
  book,
  bookmarks,
  documents,
  logIn,
  personCircle,
  reader,
} from "ionicons/icons";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { checkTokenThunk } from "../redux/auth/thunk";

export const More: React.FC = () => {
  const dispatch = useDispatch();
  const authStateItems = useSelector((state: RootState) => state.auth);
  let token = authStateItems.token;
  let payload = authStateItems.payload;

  function Logout(){
    localStorage.removeItem("token")
    dispatch(checkTokenThunk());
  }
  useEffect(() => {
    dispatch(checkTokenThunk());
  }, []);
  // const userInfo = useSelector((state: RootState) => state.user.userInfo);
  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="More" />
      </IonHeader>

      <IonContent>
        <IonList>

          {token? <IonItem routerLink={`/profile/${payload?.id}`}>
            <IonIcon icon={personCircle} slot="start"></IonIcon>
            <IonLabel>Profile</IonLabel>
          </IonItem>:null}
          

          {token?<IonItem routerLink="/bookmarks">
            <IonIcon icon={bookmarks} slot="start"></IonIcon>
            <IonLabel>Bookmarks</IonLabel>
          </IonItem>:null}
          
          <IonItem>
            <IonIcon icon={documents} slot="start"></IonIcon>
            <IonLabel>Privacy policy</IonLabel>
          </IonItem>

          <IonItem>
            <IonIcon icon={reader} slot="start"></IonIcon>
            <IonLabel>Term of usage</IonLabel>
          </IonItem>

          <IonItem>
            <IonIcon icon={book} slot="start"></IonIcon>
            <IonLabel>About us</IonLabel>
          </IonItem>
          <IonItem
            routerLink={token? "/more" : "/login" }
            button onClick={Logout}
          >
            <IonIcon icon={logIn} slot="start"></IonIcon>
            <IonLabel>{token ? "Logout" : "Login"}</IonLabel>
          </IonItem>
          {/* <p>
            ==========Dev mode Data=================
          </p>
          <h2>{payload?.emailAddress}</h2>
          <h2>{payload?.userName}</h2>
          <p>
            id: {payload?.id} <br/>
            Email: {payload?.emailAddress} <br/>
            UserName: {payload?.userName} <br/>
            Admin: {payload?.admin.toString()} <br/>
          </p>
          <p>Token: <br/>{token? token : "token not found"}</p>
          <p>
            ==========Dev mode Data=================
          </p> */}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default More;
