import React, {useEffect} from "react";
import {
  IonFooter,
  IonContent,
  IonHeader,
  IonPage,
  useIonToast,

} from "@ionic/react";
import HeaderBar from "../components/HeaderBar";
import { getAPIServer } from "../helpers/api";
import { useParams } from "react-router";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { loginFailed} from "../redux/auth/action";
import { checkTokenThunk, handleTokenThunk } from "../redux/auth/thunk";



export const LoginSuccess:React.FC=()=>{
  const params=useParams<{OTP:string}>()
  const emailOTP=params.OTP
  const dispatch = useDispatch();
  const [present, dismiss] = useIonToast();
  const [error,setError]=useState("")
    let origin= getAPIServer()

    useEffect(()=>{
      LoginCheck()
      dispatch(checkTokenThunk())
      
    },[])

    async function LoginCheck(){
      console.log("Login Checking")
      let res =await fetch(`${origin}/login/${emailOTP}`)
        if (res.status===200){
          const token=(await res.json()).token
          console.log("Token get! ", token)
          console.log("preparing to run handleTokenThunk")
          dispatch(handleTokenThunk(token))
          present({
            buttons: [{ text: 'close', handler: () => {window.location.href="/"} }],
            position:"middle",
            duration:2000,
            color:"dark",
            message: 'Login Success!  Redirecting',
            // onDidDismiss: () => console.log('dismissed'),
            // onWillDismiss: () => console.log('will dismiss'),
          })
          setTimeout(()=>{window.location.href="/"},2000)
          return
        }else{
          const failMessage=await res.json()
          dispatch(loginFailed(failMessage.toString()))
          setError(failMessage)
          present({
            buttons: [{ text: 'close', handler: () => {window.location.href="/login"} }],
            position:"middle",
            color:"danger",
            message: `Login Failed!      ${failMessage}`,
            animated:true,
            // onDidDismiss: () => console.log('dismissed'),
            // onWillDismiss: () => console.log('will dismiss'),
          })
          return
        }
    }

    return(
    <IonPage>
      <IonHeader>
        <HeaderBar title="" />
      </IonHeader>
      <IonContent>
          
      </IonContent>
      <IonFooter>
      </IonFooter>
    </IonPage>
    )
}

export default LoginSuccess