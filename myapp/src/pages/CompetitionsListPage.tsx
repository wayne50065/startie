import React, { useEffect, useState } from "react";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonCard,
  IonRow,
  IonCol,
  IonImg,
  IonSegment,
  IonSegmentButton,
  useIonViewWillEnter,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
} from "@ionic/react";
import HeaderBar from "../components/HeaderBar";
import competitionPic from "../temp_local_img/competitionPic.png";
import "./CompetitionsListPage.css";
import { get, getAPIServer } from "../helpers/api";

export type Competition = {
  id: number
  title: string
  short_desc: string
  image_url: string
}

export const CompetitionsList: React.FC = () => {
  const [error, setError] = useState('')
  const [currentCompetitionList, setCurrentCompetitionList] = useState<Competition[]>([])
  const [pastCompetitionList, setPastCompetitionList] = useState<Competition[]>([])
  const [competitionPage, setCompetitionPage] = useState('current')

  async function loadCurrentCompetitionList() {
    try {
      let json = await get(`/competition/current`)
      if (json.error) {
        setError(json.error)
      } else {
        // setCurrentCompetitionList(json.currentCompetitionList)
        setError('')
        const max = currentCompetitionList.length + 7;
        const min = max - 7;
        const newCurrentCompetitionList: Competition[] = [];
        if(max>json.currentCompetitionList.length){
          return
        }
        for (let i = min; i < max; i++) {
          if(i>json.currentCompetitionList.length){
            break
          }
          newCurrentCompetitionList.push(json.currentCompetitionList[i]);
          setCurrentCompetitionList([...currentCompetitionList, ...newCurrentCompetitionList]);
        }
      }
    } catch (error) {
      setError((error as Error).toString())
    }
  }

  async function loadPastCompetitionList() {
    try {
      let json = await get(`/competition/past`)
      if (json.error) {
        setError(json.error)
      } else {
        // setPastCompetitionList(json.pastCompetitionList)
        setError('')
        const max = pastCompetitionList.length + 7;
        const min = max - 7;
        const newPastCompetition: Competition[] = [];
        if(max>json.pastCompetitionList.length){
          return
        }
        for (let i = min; i < max; i++) {
          if(i>json.pastCompetitionList.length){
            break
          }
          newPastCompetition.push(json.pastCompetitionList[i]);
          setPastCompetitionList([...pastCompetitionList, ...newPastCompetition]);
        }
      }
    } catch (error) {
      setError((error as Error).toString())
    }
  }const loadData = (ev: any) => {
    setTimeout(() => {
      loadCurrentCompetitionList()
      loadPastCompetitionList()
      ev.target.complete();
    }, 500);
  };
  // useEffect(() => {
  //   loadCurrentCompetitionList()
  //   loadPastCompetitionList()
  // }, []);
  useIonViewWillEnter(() => {
    loadCurrentCompetitionList()
    loadPastCompetitionList()
  });

  if (!currentCompetitionList || !pastCompetitionList) {
    return (
      <div>
        <p>404: Fund not found!</p>
      </div>
    )
  }

  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="Competitions" search={false} />
        {/* <IonSearchbar value={searchText} onIonChange={e => setSearchText(e.detail.value!)}></IonSearchbar> */}
      </IonHeader>

      <IonContent fullscreen>
        <IonImg src={competitionPic} />
        <IonSegment value={competitionPage} onIonChange={e => {
          if (e.detail.value) setCompetitionPage(e.detail.value.toString())
        }}
        >
          <IonSegmentButton value='current'>
            Current
          </IonSegmentButton>
          <IonSegmentButton value='past'>
            Past
          </IonSegmentButton>
        </IonSegment>

        {competitionPage === 'current' ?
          currentCompetitionList.map(current =>
            <IonCard className="projectCard" routerLink={"/competition/" + current.id}>
              <IonRow>
                <IonCol size="8">
                  <h4>{current.title}</h4>
                  <p className="projectContent">{current.short_desc}</p>
                </IonCol>
                <IonCol size="4">
                  <div className="projectImg">
                    <IonImg src={`${getAPIServer()}/uploads/${current.image_url}`} />
                  </div>
                </IonCol>
              </IonRow>
            </IonCard>
          )
          :
          pastCompetitionList.map(past =>
            <IonCard className="projectCard" routerLink={"/competition/" + past.id}>
              <IonRow>
                <IonCol size="8">
                  <h4>{past.title}</h4>
                  <p className="projectContent">{past.short_desc}</p>
                </IonCol>
                <IonCol size="4">
                  <div className="projectImg">
                    <IonImg src={`${getAPIServer()}/uploads/${past.image_url}`} />
                  </div>
                </IonCol>
              </IonRow>
            </IonCard>
          )
        }
         <IonInfiniteScroll onIonInfinite={loadData} threshold="100px">
          <IonInfiniteScrollContent
            loadingSpinner="bubbles"
            loadingText="Loading more data..."
          ></IonInfiniteScrollContent>
        </IonInfiniteScroll>
      </IonContent>
    </IonPage>
  );
};

export default CompetitionsList;
