import { IonContent, IonHeader, IonPage, IonFooter, IonItem, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent, IonSegment, IonSegmentButton, IonLabel, IonBadge, IonButton, IonImg, IonButtons, IonTabButton, IonIcon, IonList, IonGrid, IonRow, IonCol, IonInput, IonAvatar, IonText, IonTextarea, useIonAlert } from "@ionic/react";
import HeaderBar from "../components/HeaderBar";
import "./IdeaDetailPage.css";
import ProjectAbout from "../components/ProjectAbout";
// import { ProjectDiscussionContent} from "../components/ProjectDiscussion";
import React, { useEffect, useState } from "react";
import { Redirect, useLocation, useParams } from "react-router";
import { get, getAPIServer, patch, post } from "../helpers/api";
import { constructSharp, heart, heartOutline, logoWhatsapp, logoYoutube, shareOutline, walk } from "ionicons/icons";
import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import default_pic from '../temp_local_img/default_project_pic.jpg'


export type Idea = {
  id: number
  title: string
  user_id: number
  projectOwner: string
  category: string
  detail: string
  project_deadline: Date
  talentNeeded: string[]
  criteria: string
  youtube_url: string
  images: string
  chat_room_url: string
  competition_name: string
  competition_url: string
  competition_date: number | string | Date
  created_at: number | string | Date
  has_applied: boolean
  has_quitted: boolean
  has_approved: boolean
  has_liked: boolean
  has_unliked: boolean
}

export type discussion = {
  userName: string
  userPic: string
  discussion: string
  created_at: Date
}

export function NewsFeed() {
  const [ideaDetail, setIdeaDetail] = useState<Idea>()
  const [error, setError] = useState('')
  const [projectAction, SetProjectAction] = useState("about");
  const token = localStorage.getItem("token")
  const going = ideaDetail?.has_applied
  const quit = ideaDetail?.has_quitted
  const approve = ideaDetail?.has_approved
  // const bookmark = ideaDetail?.has_liked
  // const unBookmark = ideaDetail?.has_unliked
  const hasBookmark = ideaDetail?.has_liked && !ideaDetail.has_unliked
  const [about, setAbout] = useState('')
  const [message, setMessage] = useState('')
  // const [bookmark, setBookmark] = useState(false)
  const [ideaDiscussion, setIdeaDiscussion] = useState<discussion[]>([])
  const [present] = useIonAlert();



  const params = useParams<{ idea_id: string }>()
  const idea_id = parseInt(params.idea_id)

  async function loadIdeaDetail() {
    try {
      let json = await get(`/idea/${idea_id}`)
      if (json.error) {
        setError(json.error)
      } else {
        setIdeaDetail(json.ideaDetail)
        setError('')
      }
    } catch (error) {
      setError((error as Error).toString())
    }
  }

  async function loadMessage() {
    try {
      let json = await get(`/idea/${idea_id}/message`)
      if (json.error) {
        setError(json.error)
      } else {
        setIdeaDiscussion(json.ideaMessage)
        setError('')
      }
    } catch (error) {
      setError((error as Error).toString())
    }
  }

  async function onBookmark() {
    let json = await post(`/bookmark/idea/${idea_id}`, {})
    if (json.error) {
      setError(json.error)
    } else {
      setError('')
      loadIdeaDetail()
    }
  }

  async function onUnBookmark() {
    let json = await patch(`/bookmark/idea/${idea_id}`, {})
    if (json.error) {
      setError(json.error)
    } else {
      setError('')
      loadIdeaDetail()
    }
  }

  async function onApplied() {
    if (!token) {
      window.location.href = "login"
    }
    let json = await post(`/idea/${idea_id}/apply`, { about })
    if (json.error) {
      setError(json.error)
    } else {
      setError('')
      // setGoing(true)
      setAbout('')
      loadIdeaDetail()
    }
  }

  async function onQuitted() {
    let json = await patch(`/idea/${idea_id}/apply`, {})
    if (json.error) {
      setError(json.error)
    } else {
      setError('')
      loadIdeaDetail()
    }
  }

  async function onTexted() {
    if (!token) {
      window.location.href = "/login"
    }
    if (message == "") {
      return
    }
    let json = await post(`/idea/${idea_id}/message`, { message })
    // console.log(json)
    if (json.error) {
      setError(json.error)
    } else {
      setError('')
      setMessage('')
      loadMessage()
    }
  }

  async function onDelete() {
    let json = await patch(`/idea/cancel/${idea_id}`, {})
    if (json.error) {
      setError(json.error)
    } else {
      setError('')
      window.location.href = '/'
    }
  }

  useEffect(() => {
    loadIdeaDetail()
    loadMessage()
  }, [idea_id])


  let isOwner = useSelector((state: RootState) => state.auth.payload?.id === ideaDetail?.user_id)

  if (!ideaDetail) {
    return (
      <div>
        <p>404: Idea not found!</p>
      </div>
    )
  }

  if (!ideaDiscussion) {
    return (
      <div>
        <p>404: Discussion not found!</p>
      </div>
    )
  }

  let date = ideaDetail.project_deadline
  let comUrl = ideaDetail.competition_url

  console.log(ideaDetail.has_liked)
  console.log(ideaDetail.has_unliked)

  return (
    <IonPage>
      <IonHeader>
        <HeaderBar title="Idea" backBtn={true} backLocation="/idea" />
      </IonHeader>
      <IonContent>
        <IonCard className="project-card">
          <IonCardHeader>
            <IonCardTitle className="ion-text-wrap">{ideaDetail.title}</IonCardTitle>
            <IonBadge color="warning">{ideaDetail.criteria}</IonBadge>

            <IonCardSubtitle>Project owner: {ideaDetail.projectOwner}</IonCardSubtitle>
            <IonCardSubtitle>Created at: {new Date(ideaDetail.created_at).toLocaleDateString('en-GB', { day: 'numeric', month: 'short', year: 'numeric' })}</IonCardSubtitle>

            <IonSegment
              value={projectAction}
              onIonChange={(e) => {
                if (e.detail.value) {
                  SetProjectAction(e.detail.value.toString());
                }
              }}
            >
              <IonSegmentButton value="about">
                <IonLabel>About</IonLabel>
              </IonSegmentButton>
              <IonSegmentButton value="discussion">
                <IonLabel>Discussion</IonLabel>
              </IonSegmentButton>
            </IonSegment>
          </IonCardHeader>
          <IonCardContent>
            {projectAction === "about" ? (
              <>
                <IonImg className="ion-margin-vertical"
                  src={ideaDetail.images == "" ? `${default_pic}` : `${getAPIServer()}/uploads/${ideaDetail.images[0]}`} alt="EventPhoto"


                // src={`${getAPIServer()}/uploads/${ideaDetail.images}`.map(img => img)} alt="EventPage"
                />

                <IonButtons slot="secondary" >
                  {hasBookmark ? (
                    <IonTabButton tab="bookmarked" className="ion-margin-horizontal" onClick={onUnBookmark}>
                      <IonIcon icon={heart} color="danger" />
                      <IonLabel color="danger" >Bookmarked</IonLabel>
                    </IonTabButton>
                  ) : (
                    <IonTabButton tab="bookmark" className="ion-margin-horizontal" onClick={onBookmark}>
                      <IonIcon icon={heartOutline} color="danger" />
                      <IonLabel color="danger" >Bookmark</IonLabel>
                    </IonTabButton>
                  )}

                  <IonTabButton tab="share" className="ion-margin-horizontal">
                    <IonIcon icon={shareOutline} />
                    <IonLabel>Share</IonLabel>
                  </IonTabButton>
                  {approve ? (
                    <IonTabButton className="ion-margin-horizontal">
                      <IonIcon icon={walk} color='primary' />
                      <IonLabel>Going</IonLabel>
                    </IonTabButton>
                  ) : null}
                </IonButtons>

                <IonList>
                  <IonItem>
                    <p>{ideaDetail.detail}</p>
                  </IonItem>
                  <IonItem className="display-block" >
                    <IonLabel>
                      <h2>Category</h2>
                      <p>{ideaDetail.category}</p></IonLabel>
                  </IonItem>

                  <IonItem >
                    <IonLabel>
                      <h2>Talent needed</h2>
                      <p>{ideaDetail.talentNeeded.map(talent => talent)}</p>
                    </IonLabel>
                  </IonItem>
                  <IonItem>
                    <IonLabel>
                      <h2>Project deadline</h2>
                      <p>{new Date(date).toLocaleDateString('en-GB', { day: 'numeric', month: 'short', year: 'numeric' })}</p></IonLabel>
                  </IonItem>
                  {ideaDetail.competition_name.length > 0 ? <IonItem>
                    <IonLabel>
                      <h2>Competition details</h2>
                      <IonGrid>
                        <IonRow>
                          <IonCol size='3'>
                            Name
                          </IonCol>
                          <IonCol size='9'>
                            {ideaDetail.competition_name}
                          </IonCol>
                        </IonRow>
                        <IonRow>
                          <IonCol size='3'>
                            Url
                          </IonCol>
                          <IonCol size='9'>
                            <a className='com-detail-link' href={comUrl}>{comUrl}</a>
                          </IonCol>
                        </IonRow>
                        <IonRow>
                          <IonCol size='3'>
                            Date
                          </IonCol>
                          <IonCol size='9'>
                            {new Date(ideaDetail.competition_date).toLocaleDateString('en-GB', { day: 'numeric', month: 'short', year: 'numeric' })}
                          </IonCol>
                        </IonRow>
                      </IonGrid>

                    </IonLabel>
                  </IonItem> : null}

                  <IonItem lines='none'>
                    <IonButtons>
                      {ideaDetail.youtube_url ?
                        <IonButton href={'http://' + ideaDetail.youtube_url}>
                          <IonIcon color='danger' slot="icon-only" src={logoYoutube} />
                        </IonButton> : null}
                    </IonButtons>
                    {approve ? (
                      <IonButton>
                        <IonIcon color='success' slot="icon-only" icon={logoWhatsapp} />
                      </IonButton>
                    ) : null
                    }
                  </IonItem>

                </IonList>
              </>
            ) : (

              <>
                {ideaDiscussion.map(item =>
                  <IonCard>
                    <IonGrid>
                      <IonRow>
                        <IonCol size="3">
                          <IonAvatar className="ion-align-items-start">
                            <img src={`${getAPIServer()}/uploads/${item.userPic}`} />
                          </IonAvatar>
                        </IonCol>
                        <IonCol size="9">
                          <h4>{item.userName}</h4>
                          <p>{item.discussion}</p>
                        </IonCol>
                      </IonRow>
                      <IonLabel className="ion-Label-end">
                        {new Date(item.created_at).toLocaleDateString('en-GB', { day: 'numeric', month: 'short', year: 'numeric', hour: 'numeric', minute: 'numeric' })}
                      </IonLabel>
                    </IonGrid>
                  </IonCard>
                )}
              </>
            )
            }
          </IonCardContent>
        </IonCard>
      </IonContent>


      <IonFooter>
        {projectAction === "discussion" ? (
          // discussion segment
          <IonItem className="ion-margin-vertical">
            <IonTextarea value={message} onIonChange={e => setMessage(e.detail.value || '')} placeholder="Message"></IonTextarea>
            <IonButton slot="end" size="default" color="primary" onClick={onTexted}>
              Send
            </IonButton>
          </IonItem>
        ) : (
          // About Segment
          isOwner ? (
            <>
              <IonItem lines="none" >
                <IonButtons className="button-bar-container">
                  <IonButton size='small' color=''
                    onClick={() =>
                      present({
                        cssClass: 'my-css',
                        header: 'Delete the idea?',
                        message: '',
                        buttons: [
                          'Cancel',
                          { text: 'Confirm', handler: (d) => onDelete() },
                        ],
                        onDidDismiss: (e) => console.log('did dismiss'),
                      })
                    }
                  >
                    Delete
                  </IonButton>

                  {/* <IonButton size='small' routerLink={"/idea-edit/:idea_id"}>
                <IonLabel>Edit</IonLabel>
              </IonButton> */}
                  <IonButton size='small' routerLink={"/idea-applicant/" + idea_id}>
                    Applicants
                  </IonButton>
                </IonButtons>

              </IonItem>

            </>
          ) : (
            quit ?
              <IonItem color='warning'>
                <IonLabel className="ion-text-wrap">
                  You have already quitted this project.
                </IonLabel>
              </IonItem>
              : going ?
                // just applied
                <>
                  <IonItem color='medium'>
                    <p className='ion-text-wrap'>
                      Application submitted.<br /> The project owner will email you shortly.
                    </p>
                    <IonButton size='default' color='danger' className='margin-auto'
                      slot='end'
                      onClick={() =>
                        present({
                          cssClass: 'my-css',
                          header: 'Are you sure to quit this project?',
                          message: 'You can no longer join again, unless the project owner consent.',
                          buttons: [
                            'Cancel',
                            { text: 'Confirm', handler: (d) => onQuitted() },
                          ],
                          onDidDismiss: (e) => console.log('did dismiss'),
                        })
                      }
                    >
                      Quit
                    </IonButton>
                  </IonItem>
                </> :
                // not yet applied
                <IonItem className="ion-margin-vertical">
                  <IonTextarea value={about} onIonChange={e => setAbout(e.detail.value || '')} placeholder="About You"></IonTextarea>
                  <IonButton slot="end" size="default" color="primary" onClick={onApplied} >
                    Apply
                  </IonButton>
                </IonItem>
          )
        )
        }
      </IonFooter >
    </IonPage >
  );
};

export default NewsFeed;
