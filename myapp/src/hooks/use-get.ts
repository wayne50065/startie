import { useEffect, useState } from "react";
import { get } from "../helpers/api";

export function useGet<T>(url: string, initialValue: T) {
    const [json, setJson] = useState<T & { error?: string }>(initialValue)
    useEffect(() => {
        get(url).then(setJson)
    }, [url])
    return [json, setJson] as const
}