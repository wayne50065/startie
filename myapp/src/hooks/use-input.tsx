import { selectImage } from "@beenotung/tslib/file";
import { compressMobilePhoto, dataURItoBlob, dataURItoMimeType } from "@beenotung/tslib/image";
import { IonItem, IonLabel, IonInput, IonDatetime, IonButtons, IonButton, IonIcon, IonTextarea } from "@ionic/react";
import { trashOutline, imageSharp } from "ionicons/icons";
import { useState } from "react";
import { useSelector } from "react-redux";
import { getAPIServer, toImageUrl } from "../helpers/api";
import { RootState } from "../redux/state";

export type Photo = {
    url: string
    file: File
}

export function useInput<T extends object>(initialValue: T = {} as T) {
    const [state, setState] = useState<T>(initialValue)

    function item<T>(field: string, renderItem: (value: T, setValue: (value: T) => void) => JSX.Element, extra?: JSX.Element) {
        let key = field.split(' ')[0]
        let label = field.replace(/_/g, ' ')
        let value = (state as any)[key]
        function setValue(value: T) {
            setState(state => ({ ...state, [key]: value }))
        }
        return <IonItem key={key}>
            <IonLabel position="stacked">{label}</IonLabel>
            {renderItem(value, setValue)}
            {extra}
        </IonItem>
    }

    function input(field: string, extra?: JSX.Element) {
        return item<string>(field, (value, setValue) =>
            <IonInput value={value} onIonChange={e => setValue(e.detail.value || '')}></IonInput>, extra
        )
    }

    function textarea(field: string) {
        return item<string>(field, (value, setValue) =>
            <IonTextarea value={value} onIonChange={e => setValue(e.detail.value || '')}></IonTextarea>
        )
    }

    function datetime(field: string) {
        return item<string>(field, (value, setValue) =>
            <IonDatetime min="2021" max="2030" value={value} onIonChange={e => setValue(e.detail.value || '')} placeholder='Select Date'></IonDatetime>
        )
    }

    function photos(field: string) {
        return item<Photo[]>(field, (value, setValue) => {
            const photos = value || []
            function remove(photo: Photo) {
                setValue(photos.filter(x => x !== photo))
            }
            async function selectImages() {
                let files = await selectImage({ multiple: true, })
                for (let file of files) {
                    let url = await compressMobilePhoto({ image: file })
                    let blob = dataURItoBlob(url)
                    let type = dataURItoMimeType(url)
                    file = new File([blob], file.name, { type, lastModified: file.lastModified })
                    photos.push({ file, url })
                }
                setValue([...photos])
            }
            return <div>
                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                    {photos.map((photo: Photo) => <div style={{ display: 'flex' }}>
                        <img src={toImageUrl(photo.url)}
                            style={{
                                maxWidth: '20vw',
                                maxHeight: '30vh',
                            }}
                        ></img>
                        <IonButtons>
                            <IonButton onClick={() => remove(photo)}>
                                <IonIcon src={trashOutline} color='danger'>
                                </IonIcon>
                            </IonButton>
                        </IonButtons>
                    </div>)}
                </div>

                <IonButtons>
                    <IonButton onClick={selectImages}>
                        <IonIcon src={imageSharp}></IonIcon>
                    </IonButton>
                </IonButtons>
            </div>
        }
        )
    }

    let token = useSelector((state: RootState) => state.auth.token)

    function upload(method: 'POST' | 'PATCH', url: string, extra={}) {
        let formData = new FormData()
        let newState = {...state, ...extra}
        for (let key in newState) {
            let value = newState[key] as any
            if (Array.isArray(value)) {
                for (let item of value) {
                    if (item.file) {
                        item = item.file
                    }
                    formData.append(key, item)
                }
            } else {
                formData.append(key, value)
            }

        }

        return fetch(getAPIServer() + url, {
            method,
            headers: {
                'Authorization': 'Bearer ' + token
            },
            body: formData
        })
    }

    return [state, setState, { item, input, datetime, photos, textarea, upload }] as const
}