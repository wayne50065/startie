import { useState } from "react";

export function useArray<T>(initialValue: T[]) {
    const [array, setArray] = useState(initialValue)
    function push(item: T) {
        setArray([...array, item])
    }
    function remove(item: T) {
        setArray(array.filter(x => x !== item))
    }
    function filter(fn: (item: T) => boolean) {
        setArray(array.filter(fn))
    }
    function reset() {
        setArray(initialValue)
    }
    return [array, { push, reset, remove, filter, set: setArray }] as const
}