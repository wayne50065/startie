export function IdeaUploadForm1(props: Props) {
    const [projectTitle, setProjectTitle] = useState<string>();
    const [category, setCategory] = useState<string>();
    const [details, setDetails] = useState<string>();
    const [projectDeadline, setProjectDeadline] = useState<string>();
    const [talentNeeded, setTalentNeeded] = useState<string>();
    const [talentNeededRemark, setTalentNeededRemark] = useState<string>();
    const [criteria, setCriteria] = useState<string>();
    const [youtubeVideoLink, setYoutubeVideoLink] = useState<string>();
    const [photo, setPhoto] = useState<string>();
    const [teamChannelLink, setTeamChannelLink] = useState<string>();
    const [competitionName, setCompetitionName] = useState<string>();
    const [competitionUrl, setCompetitionUrl] = useState<string>();
    const [competitionDate, setCompetitionDate] = useState<string>();

    const dispatch = useDispatch()

    function onSubmitIdea() {
        dispatch(addIdeaItem(props.ideaItemList))
    }

    return (
        <IonPage>
            <IonHeader>
                <HeaderBar title="Upload Your Ideas" />
            </IonHeader>
            <IonContent>
                <IonList>
                    <IonItem>
                        <IonLabel position="stacked">Project title</IonLabel>
                        <IonInput value={projectTitle} onIonChange={e => setProjectTitle(e.detail.value!)}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Category</IonLabel>
                        <IonInput value={category} onIonChange={e => setCategory(e.detail.value!)}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Details</IonLabel>
                        <IonInput value={details} onIonChange={e => setDetails(e.detail.value!)}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Project deadline</IonLabel>
                        <IonDatetime placeholder="Select Date" value={projectDeadline} onIonChange={e => setProjectDeadline(e.detail.value!)}></IonDatetime>
                    </IonItem>

                    <IonItem>
                        <IonLabel position="stacked">Talent needed</IonLabel>
                        <IonInput value={talentNeeded} onIonChange={e => setTalentNeeded(e.detail.value!)}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Talent needed remark (Optional)</IonLabel>
                        <IonInput value={talentNeededRemark} onIonChange={e => setTalentNeededRemark(e.detail.value!)}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Criteria (Optional)</IonLabel>
                        <IonInput value={criteria} onIonChange={e => setCriteria(e.detail.value!)} placeholder="e.g. HSU Limited"></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Youtube video link (Optional)</IonLabel>
                        <IonInput value={youtubeVideoLink} onIonChange={e => setYoutubeVideoLink(e.detail.value!)} ></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Photo (Optional)</IonLabel>
                        <input type='file' value={photo} onChange={e => setPhoto(e.target.value!)}></input>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Team channel link</IonLabel>
                        <IonInput value={teamChannelLink} onIonChange={e => setTeamChannelLink(e.detail.value!)} ></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Competition name (If any)</IonLabel>
                        <IonInput value={competitionName} onIonChange={e => setCompetitionName(e.detail.value!)}></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Competition url (If any)</IonLabel>
                        <IonInput value={competitionUrl} onIonChange={e => setCompetitionUrl(e.detail.value!)} ></IonInput>
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Competition date (If any)</IonLabel>
                        <IonDatetime value={competitionDate} onIonChange={e => setCompetitionDate(e.detail.value!)} placeholder="Select Date"></IonDatetime>
                    </IonItem>
                </IonList>


            </IonContent>
            <IonFooter>
                <IonButton expand='block' color="medium" onClick={onSubmitIdea}>
                    Submit
                </IonButton>
            </IonFooter>
        </IonPage>
    );
};
