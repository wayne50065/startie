import { Knex } from "knex";
import { hashPassword } from "../utils/bcrypt";

export async function seed(knex: Knex): Promise<void> {
  // delete table
  await knex("user_talent").del();
  await knex("project_talent").del();
  await knex("talent").del();
  await knex("interest_list").del();
  await knex("project_discussion").del();
  await knex("project_participant").del();
  await knex("project_image").del();
  await knex("project").del();
  await knex("project_category").del();
  await knex("user_info").del();
  await knex("api_log").del();

  // variable list

  const user_list = [
    {
      email_address: "mark@tecky.io",
      name: "Mark Lam",
      title: "Male",
      is_admin: false,
      password_hash: await hashPassword("mark"),
    },
    {
      email_address: "kate@tecky.io",
      name: "Kate Cheng",
      title: "Female",
      is_admin: false,
      password_hash: await hashPassword("kate"),
    },
    {
      email_address: "wayne@tecky.io",
      name: "Wayne Sze",
      title: "Male",
      is_admin: false,
      password_hash: await hashPassword("wayne"),
    },
    {
      email_address: "mark@teckyadmin.io",
      name: "Mark Lam",
      title: "Male",
      is_admin: true,
      password_hash: await hashPassword("mark"),
    },
    {
      email_address: "kate@teckyadmin.io",
      name: "Kate Cheng",
      title: "Female",
      is_admin: true,
      password_hash: await hashPassword("kate"),
    },
    {
      email_address: "wayne@teckyadmin.io",
      name: "Wayne Sze",
      title: "Male",
      is_admin: true,
      password_hash: await hashPassword("wayne"),
    },
  ];

  const project_category_list = [
    { category_name: "Academic" },
    { category_name: "Business" },
    { category_name: "Education" },
    { category_name: "Environmental" },
    { category_name: "Health" },
    { category_name: "IT" },
    { category_name: "Language" },
    { category_name: "Marketing" },
    { category_name: "Mathematics" },
    { category_name: "Science" },
    { category_name: "Transportation" },
    { category_name: "Sport" },
  ];

  const talent_list = [
    { skill: "Art" },
    { skill: "Business negotiation" },
    { skill: "Coding" },
    { skill: "Design" },
    { skill: "English" },
    { skill: "Football" },
    { skill: "Health" },
    { skill: "Japanese" },
    { skill: "Korean" },
    { skill: "Marketing" },
    { skill: "Mathematics" },
    { skill: "Science" },
    { skill: "Spanish" },
  ];

  const project_list = [
    {
      title: "Checking physical capability - no competition event 1",
      detail: "Please input details",
      user_email: "mark@tecky.io",
      category_id: "Sport",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
    {
      title: "Checking physical capability - no competition event 2",
      detail: "Please input details",
      user_email: "kate@tecky.io",
      category_id: "Sport",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
    {
      title: "Checking physical capability - no competition event 3",
      detail: "Please input details",
      user_email: "wayne@tecky.io",
      category_id: "Sport",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
    {
      title: "Checking physical capability - competition event 4",
      detail: "Please input details",
      user_email: "mark@tecky.io",
      category_id: "Sport",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
    {
      title: "Checking physical capability - competition event 5",
      detail: "Please input details",
      user_email: "kate@tecky.io",
      category_id: "Sport",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
    {
      title: "Checking physical capability - competition event 6",
      detail: "Please input details",
      user_email: "wayne@tecky.io",
      category_id: "Sport",
      application_deadline: "2021-11-04 03:14:07",
      project_deadline: "2021-11-04 03:14:07",
    },
  ];

  const project_image = [
    {
      title: "Checking physical capability - no competition event 1",
      project_image_url: "Sport1.jpg",
    },
    {
      title: "Checking physical capability - no competition event 2",
      project_image_url: "Sport2.jpg",
    },
    {
      title: "Checking physical capability - no competition event 3",
      project_image_url: "Sport3.jpg",
    },
    {
      title: "Checking physical capability - competition event 4",
      project_image_url: "Sport4.jpg",
    },
    {
      title: "Checking physical capability - competition event 5",
      project_image_url: "Sport5.jpg",
    },
    {
      title: "Checking physical capability - competition event 6",
      project_image_url: "Sport6.jpg",
    },
  ];

  const interest_list = [
    {
      user_email: "mark@tecky.io",
      title: "Checking physical capability - competition event 6",
    },

    {
      user_email: "kate@tecky.io",
      title: "Checking physical capability - competition event 4",
    },

    {
      user_email: "wayne@tecky.io",
      title: "Checking physical capability - competition event 5",
    },
  ];

  // const project_discussion = [
  //   {
  //     user_email: "mark@tecky.io",
  //     title: "Checking physical capability - competition event 5",
  //     discussion: "i wanna join competition",
  //   },
  //   {
  //     user_email: "kate@tecky.io",
  //     title: "Checking physical capability - competition event 6",
  //     discussion: "i wanna join competition",
  //   },
  //   {
  //     user_email: "wayne@tecky.io",
  //     title: "Checking physical capability - competition event 4",
  //     discussion: "i wanna join competition",
  //   },
  //   {
  //     user_email: "mark@tecky.io",
  //     title: "Checking physical capability - no competition event 3",
  //     discussion: "i wanna join",
  //   },
  //   {
  //     user_email: "kate@tecky.io",
  //     title: "Checking physical capability - no competition event 1",
  //     discussion: "i wanna join",
  //   },
  //   {
  //     user_email: "wayne@tecky.io",
  //     title: "Checking physical capability - no competition event 2",
  //     discussion: "i wanna join",
  //   },
  // ];

  // const project_participant = [
  //   {
  //     title: "Checking physical capability - no competition event 1",
  //     user_email: "kate@tecky.io",
  //     approver_email: "mark@tecky.io",
  //     approve_date: "2021-10-19 03:14:07",
  //     detail: "abc",
  //   },
  //   {
  //     title: "Checking physical capability - no competition event 2",
  //     user_email: "wayne@tecky.io",
  //     approver_email: "kate@tecky.io",
  //     approve_date: "2021-10-19 03:14:07",
  //     detail: "abc",
  //   },
  //   {
  //     title: "Checking physical capability - no competition event 3",
  //     user_email: "mark@tecky.io",
  //     approver_email: "wayne@tecky.io",
  //     approve_date: "2021-10-19 03:14:07",
  //     detail: "abc",
  //   },
  //   {
  //     title: "Checking physical capability - competition event 4",
  //     user_email: "wayne@tecky.io",
  //   },
  //   {
  //     title: "Checking physical capability - competition event 5",
  //     user_email: "mark@tecky.io",
  //   },
  //   {
  //     title: "Checking physical capability - competition event 6",
  //     user_email: "kate@tecky.io",
  //   },
  // ];

  // const project_talent = [
  //   {
  //     title: "Checking physical capability - no competition event 1",
  //     skill: "Art",
  //   },
  //   {
  //     title: "Checking physical capability - no competition event 1",
  //     skill: "Design",
  //   },
  //   {
  //     title: "Checking physical capability - no competition event 2",
  //     skill: "Mathematics",
  //   },
  //   {
  //     title: "Checking physical capability - no competition event 2",
  //     skill: "Science",
  //   },
  //   {
  //     title: "Checking physical capability - no competition event 3",
  //     skill: "Coding",
  //   },
  //   {
  //     title: "Checking physical capability - no competition event 3",
  //     skill: "Mathematics",
  //   },
  //   {
  //     title: "Checking physical capability - competition event 4",
  //     skill: "English",
  //   },
  //   {
  //     title: "Checking physical capability - competition event 5",
  //     skill: "Korean",
  //   },
  //   {
  //     title: "Checking physical capability - competition event 6",
  //     skill: "Japanese",
  //   },
  // ];

  // const user_talent = [
  //   {
  //     user_email: "wayne@tecky.io",
  //     skill: "Art",
  //   },
  //   {
  //     user_email: "wayne@tecky.io",
  //     skill: "Design",
  //   },
  //   {
  //     user_email: "mark@tecky.io",
  //     skill: "Mathematics",
  //   },
  //   {
  //     user_email: "mark@tecky.io",
  //     skill: "Science",
  //   },
  //   {
  //     user_email: "kate@tecky.io",
  //     skill: "Coding",
  //   },
  //   {
  //     user_email: "kate@tecky.io",
  //     skill: "Mathematics",
  //   },
  //   {
  //     user_email: "wayne@tecky.io",
  //     skill: "English",
  //   },
  //   {
  //     user_email: "kate@tecky.io",
  //     skill: "Korean",
  //   },
  //   {
  //     user_email: "mark@tecky.io",
  //     skill: "Japanese",
  //   },
  // ];

  // const api_log = [
  //   {
  //     table: "project",
  //     table_pk: 3,
  //     action: "select",
  //     user_id: 2,
  //     project_id: '2021-11-04 03:14:07',
  //     reference_id: '2021-11-04 03:14:07',
  //     approver_id: null,
  //     talent_id: null,
  //     discussion_id: null,
  //     api_url: null,
  //     api_method: null,
  //     api_name: null,
  //     approve_at: null,
  //   },

  // ]

  // const competition_list = [
  //   {
  //     competition_name: "Checking physical capability - competition 4a",
  //     competition_year: "2021",
  //     competition_url: "http://www.google.com",
  //     competition_start_date: "2021-10-22 03:14:07",
  //     competition_cancel_date: null,
  //   },

  //   {
  //     competition_name: "Checking physical capability - competition 4b",
  //     competition_year: "2021",
  //     competition_url: "http://www.google.com",
  //     competition_start_date: "2021-10-24 03:14:07",
  //     competition_cancel_date: null,
  //   },

  //   {
  //     competition_name: "Checking physical capability - competition",
  //     competition_year: "2022",
  //     competition_url: "http://www.google.com",
  //     competition_start_date: "2021-10-28 03:14:07",
  //     competition_cancel_date: null,
  //   },

  //   {
  //     competition_name: "Checking physical capability - competition",
  //     competition_year: "2022",
  //     competition_url: "http://www.google.com",
  //     competition_start_date: "2021-10-19 03:14:07",
  //     competition_cancel_date: null,
  //   },
  // ];

  // const project_competition = [
  //   {
  //     title: "Checking physical capability - competition event 4",
  //     competition_name: "Checking physical capability - competition 4a",
  //   },
  //   {
  //     title: "Checking physical capability - competition event 4",
  //     competition_name: "Checking physical capability - competition 4b",
  //   },
  //   {
  //     title: "Checking physical capability - competition event 5",
  //     competition_name: "Checking physical capability - competition",
  //   },
  //   {
  //     title: "Checking physical capability - competition event 6",
  //     competition_name: "Checking physical capability - competition",
  //   },
  // ];

  // insert data into table
  await knex.insert(user_list).into("user_info").returning("*");
  await knex
    .insert(project_category_list)
    .into("project_category")
    .returning("*");
  await knex.insert(talent_list).into("talent").returning("*");
  
  
  // for (let i = 0; i < project_list.length; i++) {
  //   let var_title = project_list[i]["title"];
  //   let var_detail = project_list[i]["detail"];
  //   let var_user_id = await knex
  //     .select("id")
  //     .from("user_info")
  //     .where("email_address", project_list[i]["user_email"]);
  //   let var_category_id = await knex
  //     .select("id")
  //     .from("project_category")
  //     .where("category_name", project_list[i]["category_id"]);
  //   let var_application_deadline = project_list[i]["application_deadline"];
  //   let var_project_deadline = project_list[i]["project_deadline"];

  //   await knex
  //     .insert({
  //       title: var_title,
  //       detail: var_detail,
  //       user_id: var_user_id[0]["id"],
  //       category_id: var_category_id[0]["id"],
  //       application_deadline: var_application_deadline,
  //       project_deadline: var_project_deadline,
  //     })
  //     .into("project")
  //     .returning("*");
  // }

  // for (let i = 0; i < project_image.length; i++) {
  //   let var_title = await knex
  //     .select("id")
  //     .from("project")
  //     .where("title", project_image[i]["title"]);
  //   let var_project_image_url = project_image[i]["project_image_url"];
  //   await knex
  //     .insert({
  //       project_id: var_title[0]["id"],
  //       project_image_url: var_project_image_url,
  //     })
  //     .into("project_image")
  //     .returning("*");
  // }

  // for (let i = 0; i < interest_list.length; i++) {
  //   let var_title = await knex
  //     .select("id")
  //     .from("project")
  //     .where("title", interest_list[i]["title"]);
  //   let var_user_id = await knex
  //     .select("id")
  //     .from("user_info")
  //     .where("email_address", interest_list[i]["user_email"]);
  //   await knex
  //     .insert({
  //       user_id: var_user_id[0]["id"],
  //       project_id: var_title[0]["id"],
  //     })
  //     .into("interest_list")
  //     .returning("*");
  // }

  // for (let i = 0; i < project_participant.length; i++) {
  //   let var_title = await knex
  //     .select("id")
  //     .from("project")
  //     .where("title", project_participant[i]["title"]);
  //   let var_user_id = await knex
  //     .select("id")
  //     .from("user_info")
  //     .where("email_address", project_participant[i]["user_email"]);
  //   let var_approver_email = project_participant[i]["approver_email"];
  //   let var_approve_date = project_participant[i]["approve_date"];
  //   let var_detail = project_participant[i]["detail"];

  //   await knex
  //     .insert({
  //       user_id: var_user_id[0]["id"],
  //       project_id: var_title[0]["id"],
  //       approve_date: var_approve_date,
  //       detail: var_detail,
  //     })
  //     .into("project_participant")
  //     .returning("*");
  // }

  // for (let i = 0; i < project_discussion.length; i++) {
  //   let var_title = await knex
  //     .select("id")
  //     .from("project")
  //     .where("title", project_discussion[i]["title"]);
  //   let var_user_id = await knex
  //     .select("id")
  //     .from("user_info")
  //     .where("email_address", project_discussion[i]["user_email"]);
  //   let var_discussion = project_discussion[i]["discussion"];
  //   await knex
  //     .insert({
  //       user_id: var_user_id[0]["id"],
  //       project_id: var_title[0]["id"],
  //       discussion: var_discussion,
  //     })
  //     .into("project_discussion")
  //     .returning("*");
  // }

  // for (let i = 0; i < project_talent.length; i++) {
  //   let var_title = await knex
  //     .select("id")
  //     .from("project")
  //     .where("title", project_talent[i]["title"]);
  //   let var_talent_id = await knex
  //     .select("id")
  //     .from("talent")
  //     .where("skill", project_talent[i]["skill"]);
  //   await knex
  //     .insert({
  //       talent_id: var_talent_id[0]["id"],
  //       project_id: var_title[0]["id"],
  //     })
  //     .into("project_talent")
  //     .returning("*");
  // }

  // for (let i = 0; i < user_talent.length; i++) {
  //   let var_user_id = await knex
  //     .select("id")
  //     .from("user_info")
  //     .where("email_address", user_talent[i]["user_email"]);
  //   let var_talent_id = await knex
  //     .select("id")
  //     .from("talent")
  //     .where("skill", user_talent[i]["skill"]);
  //   await knex
  //     .insert({
  //       talent_id: var_talent_id[0]["id"],
  //       user_id: var_user_id[0]["id"],
  //     })
  //     .into("user_talent")
  //     .returning("*");
  // }

  // for (let i = 0; i < competition_list.length; i++) {
  //   let var_competition_name = competition_list[i]["competition_name"];
  //   let var_competition_year = competition_list[i]["competition_year"];
  //   let var_competition_url = competition_list[i]["competition_url"];
  //   let var_competition_start_date =
  //     competition_list[i]["competition_start_date"];
  //   let var_competition_cancel_date =
  //     competition_list[i]["competition_cancel_date"];

  //   await knex
  //     .insert({
  //       competition_name: var_competition_name,
  //       competition_year: var_competition_year,
  //       competition_url: var_competition_url,
  //       competition_start_date: var_competition_start_date,
  //       competition_cancel_date: null,
  //     })
  //     .into("competition")
  //     .returning("*");
  // }

  // for (let i = 0; i < project_competition.length; i++) {
  //   let var_title = await knex
  //     .select("id")
  //     .from("project")
  //     .where("title", project_competition[i]["title"]);
  //   let var_competition_id = await knex
  //     .select("id")
  //     .from("competition")
  //     .where("competition_name", project_competition[i]["competition_name"]);
  //   await knex
  //     .insert({
  //       project_id: var_title[0]["id"],
  //       competition_id: var_competition_id[0]["id"],
  //     })
  //     .into("project_competition")
  //     .returning("*");
  // }

  // for (let i = 0; i < competition_list.length; i++) {
  //   let var_competition_name = competition_list[i]["competition_name"];
  //   let var_competition_year = competition_list[i]["competition_year"];
  //   let var_competition_url = competition_list[i]["competition_url"];
  //   let var_competition_start_date = competition_list[i]["competition_start_date"];
  //   let var_competition_cancel_date = competition_list[i]["competition_cancel_date"];

  //   await knex
  //     .insert({

  //       competition_name: var_competition_name,
  //       competition_year:var_competition_year,
  //       competition_url: var_competition_url,
  //       competition_start_date:var_competition_start_date,
  //       competition_cancel_date: null,
  //     })
  //     .into("angel_fund")
  //     .returning("*");
  // }

}
